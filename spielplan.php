<?php 
header('Content-type: text/html; charset=utf-8');
session_start();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Spielplan 2014/15</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body style="padding-top: 20px">

	<?PHP
	include "admin/lib.php";
	//Admin-Objekt erstellen
	$adm = new IVKAdmin();

	?>
	<div class="container">
		<div class="jumbotron">
			<h1>Spielplan IVK Wintermeisterschaft</h1>
			<h1>2014/2015</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h1>Spielplan durchsuchen</h1>
				<?php
				//*******  Explorer ************
				$data = $_GET['data'];
				$show = isset($data);

				$x = new c_explorer($adm);
				echo $x->selection("spielplan.php", $data);

				?>
				<br/>
				<?php
				
				//Suchergebnis anzeigen
				if ($show) {
  				echo $x->show($data);
				}
				?>
			</div>
		</div>
	</div>
</body>
</html>

