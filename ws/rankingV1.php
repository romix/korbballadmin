<?php
include "../admin/lib.php";

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET');

$adm = new IVKAdmin();

$gruppe = $_GET['group'];
$rangliste = new c_rangliste($adm);
$rangliste->setGroup($gruppe);
$tabelle = new c_tabelle($adm);

$data = new ArrayObject();
$ranglisteIDs = $rangliste->getRangliste();
$arrIndex = 0;
foreach ($ranglisteIDs as $ranking => $teamIDs) {
	foreach ($teamIDs as $teamID) {
		$team = $tabelle->arrTeamCode($teamID);
		if (isset($team['team'])) {
			$teamName = $team['team'];
		} else {
			$teamName = $team['txt'];
		}
		$points = $rangliste->punkte($teamID);
		$games = getGames('', -1, -1, $teamID, 'time', $adm);
		$played = $rangliste->anzahlGespielteSpiele($teamID);
		$won = $rangliste->gamesWon($teamID);
		$lost = $rangliste->gamesLost($teamID);
		$tie = $rangliste->gamesTie($teamID);
		$teamItem['rate'] = $rangliste->korbVerhaeltnis($teamID);
		$teamItem['teamId'] = $teamID;
		$teamItem['team'] = $teamName;
		$teamItem['points'] = $points;
		$teamItem['games'] = count($games);
		$teamItem['played'] = $played;
		$teamItem['won'] = $won;
		$teamItem['lost'] = $lost;
		$teamItem['tie'] = $tie;
		$teamItem['rank'] = $ranking + 1;
		$data[++$arrIndex] = $teamItem;
	}
}

print (json_encode($data));
?>