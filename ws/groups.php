<?php
include "../admin/lib.php";

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET');

$adm = new IVKAdmin();

$sql = "SELECT gruppe.ID as ID, gruppe from gruppe order by gruppe;";

$stmt = $adm->pdodb->query($sql);

$data = array();
while ($row = $stmt->fetch()) {
	$gruppe = $row['gruppe'];
	$data[$row['ID']] = $gruppe;
}
print (json_encode($data));
?>