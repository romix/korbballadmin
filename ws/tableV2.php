<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET');
include "../admin/lib.php";

$adm = new IVKAdmin();

$gruppe = $_GET['group'];
$tabelle = new c_tabelle($adm);
$rangliste = new c_rangliste($adm);
$rangliste->setGroup($gruppe);

$data = array();
$origTable = $tabelle->getGroupTable($gruppe);
$table['groupName'] = $origTable['groupName'];

if ($origTable['rounds'] != null) {
	foreach ($origTable['rounds'] as $roundKey => $roundName) {
		$round['id'] = $roundKey;
		$round['name'] = $roundName;
		$table['rounds'][] = $round;
	}
}

foreach ($origTable['teams'] as $teamKey => $teamName) {
	$team['id'] = $teamKey;
	$team['name'] = $teamName;
	$table['teams'][] = $team;
}

if ($origTable['games'] != null) {
	foreach ($origTable['games'] as $roundKey => $roundGames) {
		$games = array();
		$games['roundId'] = $roundKey;
		foreach ($roundGames as $teamOneKey => $teamTwoGames) {
			$teamOne = array();
			$teamOne['teamOneId'] = $teamOneKey;
			foreach ($teamTwoGames as $teamTwoKey => $result) {
				$teamTwo = array();
				$teamTwo['teamTwoId'] = $teamTwoKey;
				$teamTwo['result'] = $result;
				$teamOne['teamTwos'][] = $teamTwo;
			}
			$games['teamOnes'][] = $teamOne;
		}
		$table['games'][] = $games;
	}
}

$data['table'] = $table;

foreach ($rangliste->getRangliste() as $rank => $teamKeys) {
	$data['rankings'][] = array('rank' => $rank + 1, 'teamIds' => $teamKeys);
}

foreach($rangliste->getAllPoints() as $teamKey => $points) {
	$data['points'][] = array('teamId' => $teamKey, 'points' => $points);
}

foreach ($rangliste->getAllGoalRates() as $teamKey => $rate) {
	$data['rates'][] = array('teamId' => $teamKey, 'own' => $rate['own'], 'others' => $rate['others']);
}

print (json_encode(array('composedModel' => $data)));
?>