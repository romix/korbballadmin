<?php
include "../admin/lib.php";

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET');


$adm = new IVKAdmin();

$sql = "SELECT gruppe.ID as ID, gruppe, typ from gruppe order by gruppe;";

$stmt = $adm->pdodb->query($sql);

$data = array();
while ($row = $stmt->fetch()) {
	$gruppe = $row['gruppe'];
	$grp['ID'] = $row['ID'];
	$grp['name'] = $gruppe;
	$grp['typ'] = $row['typ'];
	$data[]['group'] = $grp;
}
print (json_encode($data));
?>