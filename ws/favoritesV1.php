<?php
include "../admin/lib.php";

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET');

$adm = new IVKAdmin();

$data = json_decode(file_get_contents("php://input"), true);

$fixedFavorites = [];
$stmt = $adm->prepareStatement("SELECT mannschaft.ID, teamgruppe.gruppeID, gruppe.gruppe FROM mannschaft left join teamgruppe ON teamgruppe.teamID = mannschaft.ID left join gruppe on teamgruppe.gruppeID = gruppe.ID where mannschaft.mannschaft LIKE :team_name;");
foreach($data["favorites"] as $favorite) {
    $stmt->execute(array("team_name" => $favorite));
    while($row = $stmt->fetch()) {
        $fixedFavorites[] = array("id" => (int)$row["ID"], "groupId" => (int)$row["gruppeID"], "gruppe" => $row["gruppe"], "team_name" => $favorite);
    }
}

print (json_encode($fixedFavorites));
?>