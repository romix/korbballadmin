<?php
include "../admin/lib.php";

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET');

$adm = new IVKAdmin();

$data = array("season" => $adm->saison);

print (json_encode($data));
?>