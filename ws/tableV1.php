<?php
include "../admin/lib.php";

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET');

$adm = new IVKAdmin();

$gruppe = $_GET['group'];
$tabelle = new c_tabelle($adm);
$rangliste = new c_rangliste($adm);
$rangliste->setGroup($gruppe);

$data = array();
$data['table'] = $tabelle->getGroupTable($gruppe);
$data['ranking'] = $rangliste->getRangliste();
$data['points'] = $rangliste->getAllPoints();
$data['rates'] = $rangliste->getAllGoalRates();

print (json_encode($data));
?>