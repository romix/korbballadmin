<?php
include "../admin/lib.php";

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET');

$adm = new IVKAdmin();

$gruppe = $_GET['group'];
$rangliste = new c_rangliste($adm);
$tabelle = new c_tabelle($adm);
$rangliste->setGroup($gruppe);
$ranglisteIDs = $rangliste->getRangliste();
$annotations = $rangliste->getAllAnnotations();

$data = array();

foreach ($ranglisteIDs as $ranking => $teamIDs) {
	foreach ($teamIDs as $teamID) {
		$team = $tabelle->arrTeamCode($teamID);
		if ($annotations[$teamID]) {
			if ($team['team']) {
				$teamName = $team['team'];
			} else {
				$teamName = $team['txt'];
			}
			foreach ($annotations[$teamID] as $anno) {
				$data[] = $teamName . ': ' . $anno;
			}
		}
	}
}

print (json_encode($data));
?>