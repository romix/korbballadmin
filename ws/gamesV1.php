<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET');

include "../admin/lib.php";

$adm = new IVKAdmin();

$team = isset($_GET['team']) ? $_GET['team'] : -1;
$group = isset($_GET['group']) ? $_GET['group'] : -1;

$games = getGames('', $group, -1, $team, 'time', $adm);

$data = array();
// encoding for json UTF-8
foreach ($games as $game) {
	$dat = array();
	$dat['tag'] = $game['tag'];
	$dat['zeit'] = $game['zeit'];
	$dat['halle'] = $game['halle'];
	$dat['gruppe'] = $game['gruppe'];
	$dat['runde'] = $game['runde'];
	$dat['txtTeamA'] = $game['txtTeamA'];
	$dat['txtTeamB'] = $game['txtTeamB'];
	$dat['resultatA'] = $game['resultatA'];
	$dat['resultatB'] = $game['resultatB'];
	$dat['punkteA'] = $game['punkteA'];
	$dat['punkteB'] = $game['punkteB'];
	$data[] = $dat;
}

print (json_encode($data));
?>