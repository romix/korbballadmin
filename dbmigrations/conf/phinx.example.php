<?php
$config = array();

$config['paths']['migrations'] = '%%PHINX_CONFIG_DIR%%/../migrations';

$config['environments']['default_migration_table'] = 'phinxlog';
$config['environments']['default_database'] = 'production';

$config['environments']['production']['adapter'] = 'mysql';
$config['environments']['production']['host'] = 'localhost';
$config['environments']['production']['name'] = 'turnver_korbball';
$config['environments']['production']['user'] = 'korbball';
$config['environments']['production']['pass'] = '********';
$config['environments']['production']['port'] = '3306';
$config['environments']['production']['charset'] = 'utf8';
$config['environments']['production']['mysql_attr_init_command'] = "SET CHARSET 'utf8', NAMES 'utf8', sql_mode = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';";

return $config;