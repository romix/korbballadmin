<?php

use Phinx\Migration\AbstractMigration;

class AddTableGruppeExtrapunkte extends AbstractMigration {

	public function up() {
		$this->execute("CREATE TABLE `gruppe_extrapunkte` (
				`ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`gruppeID` int(10) unsigned NOT NULL DEFAULT '0',
				`teamID` char(10) NOT NULL,
				`extrapunkte` tinyint(4) NOT NULL DEFAULT '0',
				`grund` varchar(254) NOT NULL DEFAULT '',
				PRIMARY KEY (`ID`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");
	}

	public function down() {
	}
}