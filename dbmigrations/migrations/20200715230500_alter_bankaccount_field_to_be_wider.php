<?php

use Phinx\Migration\AbstractMigration;

class AlterBankaccountFieldToBeWider extends AbstractMigration
{

  public function up()
  {
  	$this->execute("SET SESSION sql_mode='ALLOW_INVALID_DATES'");
    $tableAnmeldung = $this->table('anmeldung');
    $tableAnmeldung->changeColumn('bank', 'string', ['limit' => '400'])
      ->save();
  }

  public function down()
  {
  }
}