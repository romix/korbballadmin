<?php

use Phinx\Migration\AbstractMigration;

/**
 * Initially create all tables.
 * @author roman
 *
 */
class InitialMigration extends AbstractMigration {
	
	public function up() {
		$this->execute("CREATE TABLE `anmeldung` (
				`ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`mannschaftID` int(10) DEFAULT '0',
				`datum` date DEFAULT NULL,
				`saison` varchar(20) NOT NULL DEFAULT '',
				`minGebDat` date DEFAULT NULL,
				`maxGebDat` date DEFAULT NULL,
				`kategorie` varchar(8) DEFAULT NULL,
				`malter` varchar(8) DEFAULT NULL,
				`verein` varchar(45) NOT NULL,
				`verband` varchar(45) NOT NULL,
				`mNummer` tinyint(4) DEFAULT NULL,
				`liga` varchar(1) DEFAULT NULL,
				`tenueShirt` varchar(45) DEFAULT NULL,
				`tenueHose` varchar(45) DEFAULT NULL,
				`ersatzShirt` varchar(45) DEFAULT NULL,
				`ersatzHose` varchar(45) DEFAULT NULL,
				`vName` varchar(45) NOT NULL,
				`vAdresse` varchar(45) NOT NULL,
				`vPLZOrt` varchar(45) NOT NULL,
				`vTel` varchar(45) NOT NULL,
				`vMail` varchar(45) NOT NULL,
				`sName` varchar(45) DEFAULT NULL,
				`sAdresse` varchar(45) DEFAULT NULL,
				`sPLZOrt` varchar(45) DEFAULT NULL,
				`sTel` varchar(45) DEFAULT NULL,
				`sMail` varchar(45) DEFAULT NULL,
				`bank` varchar(45) DEFAULT NULL,
				`konflikt` blob,
				`mName` varchar(45) DEFAULT NULL,
				`mAdresse` varchar(45) DEFAULT NULL,
				`mPLZOrt` varchar(45) DEFAULT NULL,
				`mTel` varchar(45) DEFAULT NULL,
				`mMail` varchar(45) DEFAULT NULL,
				`jName` varchar(45) DEFAULT NULL,
				`jAdresse` varchar(45) DEFAULT NULL,
				`jPLZOrt` varchar(45) DEFAULT NULL,
				`jTel` varchar(45) DEFAULT NULL,
				`jMail` varchar(45) DEFAULT NULL,
				PRIMARY KEY (`ID`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

		$this->execute("CREATE TABLE `gruppe` (
				`ID` int(11) NOT NULL AUTO_INCREMENT,
				`ligaID` int(11) NOT NULL DEFAULT '0',
				`gruppe` char(30) NOT NULL DEFAULT '',
				`final` enum('1','0') NOT NULL DEFAULT '0',
				`name` char(30) DEFAULT NULL,
				PRIMARY KEY (`ID`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

		$this->execute("CREATE TABLE `halle` (
				`ID` int(11) NOT NULL AUTO_INCREMENT,
				`halle` blob NOT NULL,
				PRIMARY KEY (`ID`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");


		$this->execute("CREATE TABLE `liga` (
				`ID` int(11) NOT NULL AUTO_INCREMENT,
				`liga` char(30) DEFAULT NULL,
				PRIMARY KEY (`ID`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

		$this->execute("CREATE TABLE `mannschaft` (
				`ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`vereinID` int(10) unsigned NOT NULL DEFAULT '0',
				`mannschaft` varchar(35) NOT NULL DEFAULT '',
				`ligaID` int(10) unsigned DEFAULT NULL,
				`gruppeID` int(10) unsigned DEFAULT NULL,
				`tenue` varchar(20) NOT NULL DEFAULT '',
				`hosen` varchar(20) NOT NULL DEFAULT '',
				`ersatz_tenue` varchar(20) NOT NULL DEFAULT '',
				`ersatz_hosen` varchar(20) NOT NULL DEFAULT '',
				`b_anrede` varchar(10) DEFAULT NULL,
				`b_name` varchar(30) DEFAULT NULL,
				`b_vorname` varchar(30) DEFAULT NULL,
				`b_strasse` varchar(50) DEFAULT NULL,
				`b_plz` varchar(5) DEFAULT NULL,
				`b_ort` varchar(30) DEFAULT NULL,
				`b_tel` varchar(20) DEFAULT NULL,
				`b_email` varchar(50) DEFAULT '',
				`bankverbindung` mediumtext COLLATE utf8_unicode_ci,
				`s_name` varchar(30) DEFAULT '',
				`s_strasse` varchar(30) DEFAULT '',
				`s_plz` varchar(5) DEFAULT '',
				`s_ort` varchar(30) DEFAULT '',
				PRIMARY KEY (`ID`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

		$this->execute("CREATE TABLE `runde` (
				`ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`gruppeID` int(10) unsigned NOT NULL DEFAULT '0',
				`runde` char(30) NOT NULL DEFAULT '',
				`system` enum('ko','all') NOT NULL DEFAULT 'all',
				PRIMARY KEY (`ID`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

		$this->execute("CREATE TABLE `spiel` (
				`ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`rundeID` int(10) unsigned NOT NULL DEFAULT '0',
				`gruppeID` int(10) unsigned NOT NULL DEFAULT '0',
				`halleID` int(10) unsigned NOT NULL DEFAULT '0',
				`tag` date DEFAULT NULL,
				`zeit` time NOT NULL DEFAULT '00:00:00',
				`teamA` char(10) NOT NULL DEFAULT '',
				`teamB` char(10) NOT NULL DEFAULT '',
				`resultatA` tinyint(4) DEFAULT NULL,
				`resultatB` tinyint(4) DEFAULT NULL,
				`punkteA` tinyint(4) DEFAULT NULL,
				`punkteB` tinyint(4) DEFAULT NULL,
				PRIMARY KEY (`ID`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

		$this->execute("CREATE TABLE `teamgruppe` (
				`ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`gruppeID` int(10) unsigned NOT NULL DEFAULT '0',
				`teamID` int(10) unsigned NOT NULL DEFAULT '0',
				`teamCode` char(10) DEFAULT NULL,
				PRIMARY KEY (`ID`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

		$this->execute("CREATE TABLE `temp_matrix` (
				`ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`templateID` int(10) unsigned NOT NULL DEFAULT '0',
				`zeile` tinyint(3) unsigned NOT NULL DEFAULT '0',
				`halle` tinyint(3) unsigned NOT NULL DEFAULT '0',
				`spieltag` tinyint(3) unsigned NOT NULL DEFAULT '0',
				`teamA` char(10) NOT NULL DEFAULT '',
				`teamB` char(10) NOT NULL DEFAULT '',
				PRIMARY KEY (`ID`),
				KEY `halle` (`halle`),
				KEY `teamA` (`teamA`),
				KEY `teamB` (`teamB`),
				KEY `templateID` (`templateID`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

		$this->execute("CREATE TABLE `user` (
				`ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
				`anrede` char(10) DEFAULT NULL,
				`name` char(30) DEFAULT NULL,
				`vorname` char(30) DEFAULT NULL,
				`strasse` char(60) DEFAULT NULL,
				`plz` char(5) DEFAULT NULL,
				`ort` char(30) DEFAULT NULL,
				`email` char(40) DEFAULT NULL,
				`passwort` char(10) DEFAULT NULL,
				`rights` enum('adm') DEFAULT NULL,
				PRIMARY KEY (`ID`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

		$this->execute("CREATE TABLE `verein` (
				`ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`verein` varchar(30) NOT NULL DEFAULT '',
				`verband` varchar(30) DEFAULT NULL,
				`vereinort` varchar(30) NOT NULL DEFAULT '',
				`owner` int(11) DEFAULT NULL,
				`bank` varchar(4096) DEFAULT NULL,
				PRIMARY KEY (`ID`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

	}

	public function down() {
		// no way back...
	}
}