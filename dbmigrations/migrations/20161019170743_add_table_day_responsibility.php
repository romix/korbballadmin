<?php

use Phinx\Migration\AbstractMigration;

class AddTableDayResponsibility extends AbstractMigration {

	public function up() {
		$this->execute("CREATE TABLE `day_responsibility` (
				`ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`day` date NOT NULL,
				`caption` varchar(254) NOT NULL DEFAULT '',
				`subcaption` varchar(254) NOT NULL DEFAULT '',
				`picname` varchar(254) NOT NULL DEFAULT '',
				PRIMARY KEY (`ID`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");
	}

	public function down() {
	}
}