<?php

use Phinx\Migration\AbstractMigration;

class RemoveColumnGroupName extends AbstractMigration
{

  public function up()
  {
    $tableGruppe = $this->table('gruppe');
    $tableGruppe->removeColumn('name')
      ->save();
  }

  public function down()
  {
  }
}