<?php

use Phinx\Migration\AbstractMigration;

class ChangeFinalDataType extends AbstractMigration
{

  public function up()
  {
    $this->table('gruppe')->addColumn('typ', 'enum', ['values' => ['liga', 'final', 'cup']])->save();

    $this->execute("UPDATE gruppe SET typ = 'liga' WHERE final LIKE 0");
    $this->execute("UPDATE gruppe SET typ = 'final' WHERE final LIKE 1");

    $this->table('gruppe')->removeColumn('final')->save();
  }

  public function down()
  {
  }
}