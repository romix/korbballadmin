<?php

use Phinx\Migration\AbstractMigration;

class AddBemerkungenToAnmeldung extends AbstractMigration {

	public function up() {
		$this->execute("SET SESSION sql_mode='ALLOW_INVALID_DATES';
				ALTER TABLE `anmeldung` ADD COLUMN `bemerkungen` TEXT NULL AFTER `konflikt`");
	}

	public function down() {
	}
}