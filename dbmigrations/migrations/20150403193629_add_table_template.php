<?php

use Phinx\Migration\AbstractMigration;

class AddTableTemplate extends AbstractMigration {

	public function up() {
		$this->execute("CREATE TABLE `template` (
				`ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`template` char(40) NOT NULL DEFAULT '',
				`hallen` tinyint(3) unsigned NOT NULL DEFAULT '1',
				`spieltage` tinyint(3) unsigned NOT NULL DEFAULT '1',
				`teams` tinyint(3) unsigned NOT NULL DEFAULT '0',
				`zeilen` tinyint(3) unsigned NOT NULL DEFAULT '1',
				`system` enum('all','ko') NOT NULL DEFAULT 'all',
				PRIMARY KEY (`ID`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");
	}

	public function down() {
	}
}