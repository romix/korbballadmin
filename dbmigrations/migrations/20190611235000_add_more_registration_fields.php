<?php

use Phinx\Migration\AbstractMigration;

class AddMoreRegistrationFields extends AbstractMigration
{

  public function up()
  {
  	$this->execute("SET SESSION sql_mode='ALLOW_INVALID_DATES'");
    $tableAnmeldung = $this->table('anmeldung');
    $tableAnmeldung->addColumn('kaName', 'string', ['after' => 'jMail', 'limit' => 45, 'null' => true])
    ->addColumn('kaAdresse', 'string', ['after' => 'kaName', 'limit' => 45, 'null' => true])
    ->addColumn('kaPLZOrt', 'string', ['after' => 'kaAdresse', 'limit' => 45, 'null' => true])
    ->addColumn('kaTel', 'string', ['after' => 'kaPLZOrt', 'limit' => 45, 'null' => true])
    ->addColumn('kaMail', 'string', ['after' => 'kaTel', 'limit' => 45, 'null' => true])
    ->save();
  }

  public function down()
  {
  }
}