<?php

use Phinx\Migration\AbstractMigration;

class ConvertBlobColumnsToText extends AbstractMigration
{

  public function up()
  {
    $tableHalle = $this->table('halle');
    $tableHalle->changeColumn('halle', 'string')
      ->save();
    $tableAnmeldung = $this->table('anmeldung');
    $tableAnmeldung->changeColumn('konflikt', 'text', ['null' => true])
      ->save();
  }

  public function down()
  {
  }
}