<?php

use Phinx\Migration\AbstractMigration;

class ChangeRegistrationFields extends AbstractMigration
{

  public function up()
  {
  	$this->execute("SET SESSION sql_mode='ALLOW_INVALID_DATES'");
    $tableAnmeldung = $this->table('anmeldung');
    $tableAnmeldung->renameColumn('sName', 'sFirstname')
    ->renameColumn('sAdresse', 'sLastname')
    ->removeColumn('sPLZOrt')
    ->removeColumn('sTel')
    ->save();
  }

  public function down()
  {
  }
}