<?php

use Phinx\Migration\AbstractMigration;

class AddPasswordHash extends AbstractMigration
{

  public function up()
  {
    $this->execute("ALTER TABLE user ADD passwortHash varchar(255) AFTER passwort;");
    
    /**
     * Password in Hash umwandeln
     */
    $stmt = $this->query("SELECT * FROM user WHERE passwortHash IS NULL");
    $rows = $stmt->fetchAll();

    foreach ($rows as $row) {
      $passwortHash = password_hash($row['passwort'], PASSWORD_DEFAULT);

      $builder = $this->getQueryBuilder();
      $builder
        ->update('user')
        ->set('passwortHash', $passwortHash)
        ->where(['ID' => $row['ID']])
        ->execute();
    }
    $tableUser = $this->table('user');
    $tableUser->removeColumn('passwort')->save();
  }

  public function down()
  {
  }
}