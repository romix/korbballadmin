<?php

use Phinx\Migration\AbstractMigration;

class MakeGameTagZeitHalleNullable extends AbstractMigration
{

  public function up()
  {
    $tableSpiel = $this->table('spiel');
    $tableSpiel->changeColumn('tag', 'date', ['null' => true])
      ->changeColumn('zeit', 'time', ['null' => true])
      ->changeColumn('halleID', 'integer', ['signed' => false, 'null' => true])
      ->save();
  }

  public function down()
  {
  }
}