<?php
require_once '/home/roman/Eclipse/PHP/workspace/Turnverband/www.turnverband.ch/korbball/admin/SaveAnmeldung.php';

class AnmeldungDataTest extends PHPUnit_Framework_TestCase {

	var $ruswil1InJson = '{"category":"H","age":"Aktiv","liga":"2","agree":true, "vereinAddress": {"name":"Roman Schaller","address":"R�tsch 16","ZIPCity":"6130 Willisau","tel":"079 607 51 40","mail":"roman.schaller@gmail.com"}, "jugendAddress":{}, "teamAddress":{"name":"Thomas Gr�ter","address":"Ch�ppeliacher 2a","ZIPCity":"6017 Ruswil","tel":"+41 78 604 12	72","mail":"thgrueter@datazug.ch"}, "refereeAddress":{},"verein":"Korbball Fides Ruswil","verband":"keiner", "teamNbr":"1","shirt":"dunkelrot","hose":"rot","ersatzshirt":"grau","ersatzhose":"schwarz", "bank": "bank", "conflict": "conflict", "recaptchaChallenge":"03AHJ_VusodyXj6qFx8uU4DMgMwSFkW6mquoRI0PtoUKI57ctBycFQ0bp6Q0yOPTBY5zG89Ajun1CL07m7XRFBT32cMDK_jMVxb10owtMPvJG5cI7tz60FdGdihd6hGBtjhXUoa1qTzN6UWrtF-QWSVgLJND_EKK6EXKlfMOvpo58UCwbqWq0kN_H9OjTnQ5QGiiptEwgwZo13LLQzdRaq6qfmf4yPohE9FQ", "recaptchaResponse":"hemeMyt among"}';
	
	var $ruswil2InJson = '{"category":"H","age":"Aktiv","liga":"3","agree":true,"vereinAddress":{"name":"Roman Schaller","address":"R�tsch 16","ZIPCity":"6130 Willisau","tel":"079 607 51 40","mail":"roman.schaller@gmail.com"},"jugendAddress":{},"teamAddress":{"name":"Beat Erni","address":"M�hlehof 2","ZIPCity":"6018 Buttisholz","tel":"079 241 45 93","mail":"ch.erni@bluewin.ch"},"refereeAddress":{"name":"Marco Hafner","address":"Freiehofstrasse 3","ZIPCity":"6017 Ruswil","tel":"079 768 15 83","mail":"hakaru@bluewin.ch"},"verein":"Korbball Fides Ruswil","verband":"keiner","teamNbr":"2","shirt":"schwarz","hose":"dunkelschwarz","ersatzshirt":"gelb","ersatzhose":"dunkelgelb","conflict":"Fides Ruswil U20","recaptchaChallenge":"03AHJ_Vuu4k8OShafHibKdjnuOP1uk8AvZbrSV8JXMoBej2mF1o79IMwmRQ2djCameFBapxsbm7rV_9NIrXPBvRsu67Gq5T3UsYPJM1q0cQzNhtWN_ZKL79-dQeT7JZ-IrMsxQsI6lRppDLkDMik8ZCw7yfWvwJCgdqarNvknyv-wnHv4hO60rZeP9zFBm0a3ukdCuLZm5IbZOux3PUrJofltPrtDRAuey-w","recaptchaResponse":"hdirdase CIVIL"}';
	
	var $ruswilU20InJson = '{"category":"H","age":"U20","liga":"2","agree":true,"vereinAddress":{"name":"Roman Schaller","address":"R�tsch 16","ZIPCity":"6130 Willisau","tel":"079 607 51 40","mail":"roman.schaller@gmail.com"},"jugendAddress":{"name":"Patrick Krauer","address":"Freiehof 14","ZIPCity":"6017 Ruswil","tel":"+41 79 543 99 59","mail":"patrick.krauer@datazug.ch"},"teamAddress":{"name":"Thomas Gr�ter","address":"Ch�ppeliacher 2a","ZIPCity":"6017 Ruswil","tel":"+41 78 604 12 72","mail":"thgrueter@datazug.ch"},"refereeAddress":{},"verein":"Korbball Fides Ruswil","verband":"keiner","teamNbr":"1","shirt":"rot","hose":"gelb","ersatzshirt":"grau","ersatzhose":"schwarz","birthYoung":"1998-01-09","birthOld":"1996-07-20","bank":"pc-konto-99","conflict":"Ruswil 1","recaptchaChallenge":"03AHJ_VuvGCMoIN2xTbeihpP0zVAXQmdcCHkuLF8oK9XnvE2wSCIJaC8PwAcp0ENoSrP49tRBIlhyz3JJ_wC1_d0Vmk1J1BX25J1EXZJQDOGx7vaTg1l_x0uz1UFXIQSvKjUQiz_m8nMjzTedXOtyPjsebCTk_mb92z3EXyE3iwqsKR7zLq_n8rVi41bbWHBB-17ftrAb_YFtR8iZnsgF4IFXXes5MNUuNdg","recaptchaResponse":"Ispote Declaration"}';


	public function testRuswil_1() {
		$json = json_decode($this->ruswil1InJson);
		$data = new AnmeldungData($json);
		$this->assertEquals("Korbball Fides Ruswil", $data->verein);
		$this->assertEquals("keiner", $data->verband);
		$this->assertEquals("1", $data->mNummer);
		$this->assertEquals("2", $data->liga);
		$this->assertEquals("Aktiv", $data->alter);
		$this->assertEquals("H", $data->kategorie);
		$this->assertEquals("dunkelrot", $data->tenueShirt);
		$this->assertEquals("rot", $data->tenueHose);
		$this->assertEquals("grau", $data->ersatzShirt);
		$this->assertEquals("schwarz", $data->ersatzHose);
		$this->assertEquals("Roman Schaller", $data->korrespondenz->name);
		$this->assertEquals("R�tsch 16", $data->korrespondenz->adresse);
		$this->assertEquals("6130 Willisau", $data->korrespondenz->plzOrt);
		$this->assertEquals("079 607 51 40", $data->korrespondenz->tel);
		$this->assertEquals("roman.schaller@gmail.com", $data->korrespondenz->mail);
		$this->assertEquals("Thomas Gr�ter", $data->mannschaft->name);
		$this->assertEquals("Ch�ppeliacher 2a", $data->mannschaft->adresse);
		$this->assertEquals("6017 Ruswil", $data->mannschaft->plzOrt);
		$this->assertEquals("+41 78 604 12	72", $data->mannschaft->tel);
		$this->assertEquals("thgrueter@datazug.ch", $data->mannschaft->mail);
		$this->assertEquals('', $data->schiedsrichter->name);
		$this->assertEquals('', $data->schiedsrichter->adresse);
		$this->assertEquals('', $data->schiedsrichter->plzOrt);
		$this->assertEquals('', $data->schiedsrichter->tel);
		$this->assertEquals('', $data->schiedsrichter->mail);
		$this->assertEquals('conflict', $data->konflikt);
		$this->assertEquals('bank', $data->bank);
	}

	public function testRuswil_2() {
		$json = json_decode($this->ruswil2InJson);
		$data = new AnmeldungData($json);
		$this->assertEquals("Korbball Fides Ruswil", $data->verein);
		$this->assertEquals("keiner", $data->verband);
		$this->assertEquals("2", $data->mNummer);
		$this->assertEquals("3", $data->liga);
		$this->assertEquals("Aktiv", $data->alter);
		$this->assertEquals("H", $data->kategorie);
		$this->assertEquals("schwarz", $data->tenueShirt);
		$this->assertEquals("dunkelschwarz", $data->tenueHose);
		$this->assertEquals("gelb", $data->ersatzShirt);
		$this->assertEquals("dunkelgelb", $data->ersatzHose);
		$this->assertEquals("Roman Schaller", $data->korrespondenz->name);
		$this->assertEquals("R�tsch 16", $data->korrespondenz->adresse);
		$this->assertEquals("6130 Willisau", $data->korrespondenz->plzOrt);
		$this->assertEquals("079 607 51 40", $data->korrespondenz->tel);
		$this->assertEquals("roman.schaller@gmail.com", $data->korrespondenz->mail);
		$this->assertEquals("Beat Erni", $data->mannschaft->name);
		$this->assertEquals("M�hlehof 2", $data->mannschaft->adresse);
		$this->assertEquals("6018 Buttisholz", $data->mannschaft->plzOrt);
		$this->assertEquals("079 241 45 93", $data->mannschaft->tel);
		$this->assertEquals("ch.erni@bluewin.ch", $data->mannschaft->mail);
		$this->assertEquals('Marco Hafner', $data->schiedsrichter->name);
		$this->assertEquals('Freiehofstrasse 3', $data->schiedsrichter->adresse);
		$this->assertEquals('6017 Ruswil', $data->schiedsrichter->plzOrt);
		$this->assertEquals('079 768 15 83', $data->schiedsrichter->tel);
		$this->assertEquals('hakaru@bluewin.ch', $data->schiedsrichter->mail);
		$this->assertEquals('Fides Ruswil U20', $data->konflikt);
		$this->assertEquals('', $data->bank);
	}
	public function testRuswil_U20() {
		$json = json_decode($this->ruswilU20InJson);
		$data = new AnmeldungData($json);
		$this->assertEquals("Korbball Fides Ruswil", $data->verein);
		$this->assertEquals("keiner", $data->verband);
		$this->assertEquals("1", $data->mNummer);
		$this->assertEquals("U20", $data->alter);
		$this->assertEquals("1998-01-09", $data->gebJung);
		$this->assertEquals("1996-07-20", $data->gebAlt);
		$this->assertEquals("H", $data->kategorie);
		$this->assertEquals("rot", $data->tenueShirt);
		$this->assertEquals("gelb", $data->tenueHose);
		$this->assertEquals("grau", $data->ersatzShirt);
		$this->assertEquals("schwarz", $data->ersatzHose);
		$this->assertEquals("Roman Schaller", $data->korrespondenz->name);
		$this->assertEquals("R�tsch 16", $data->korrespondenz->adresse);
		$this->assertEquals("6130 Willisau", $data->korrespondenz->plzOrt);
		$this->assertEquals("079 607 51 40", $data->korrespondenz->tel);
		$this->assertEquals("roman.schaller@gmail.com", $data->korrespondenz->mail);
		$this->assertEquals("Thomas Gr�ter", $data->mannschaft->name);
		$this->assertEquals("Ch�ppeliacher 2a", $data->mannschaft->adresse);
		$this->assertEquals("6017 Ruswil", $data->mannschaft->plzOrt);
		$this->assertEquals("+41 78 604 12 72", $data->mannschaft->tel);
		$this->assertEquals("thgrueter@datazug.ch", $data->mannschaft->mail);
		$this->assertEquals('Patrick Krauer', $data->jugend->name);
		$this->assertEquals('Freiehof 14', $data->jugend->adresse);
		$this->assertEquals('6017 Ruswil', $data->jugend->plzOrt);
		$this->assertEquals('+41 79 543 99 59', $data->jugend->tel);
		$this->assertEquals('patrick.krauer@datazug.ch', $data->jugend->mail);
		$this->assertEquals('Ruswil 1', $data->konflikt);
		$this->assertEquals('pc-konto-99', $data->bank);
	}
	
	
}
