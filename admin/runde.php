<?php

class c_runde extends BaseCRUD {

    //Constructor
    function __construct(IVKAdmin $adm) {
    	parent::__construct($adm);
      $this->title = "Runde";

      $this->sqlSel['admin'] = "SELECT runde.ID, liga, gruppe, runde, 'system' FROM runde LEFT JOIN gruppe ON runde.gruppeID=gruppe.ID LEFT JOIN liga ON gruppe.ligaID=liga.ID";
      $this->fldSel['admin'] = array("liga", "gruppe", "runde", "system");
      $this->descSel['admin'] = array("Liga", "Gruppe", "Runde", "System");

      $this->sqlSel['pub'] = "SELECT runde.ID, liga, gruppe, runde, 'system' FROM runde LEFT JOIN gruppe ON runde.gruppeID=gruppe.ID LEFT JOIN liga ON gruppe.ligaID=liga.ID";
      $this->fldSel['pub'] = array("liga", "gruppe", "runde", "system");
      $this->descSel['pub'] = array("Liga", "Gruppe", "Runde", "System");

      $this->sqlUpd['edit'] = "SELECT runde.ID, gruppeID, runde, 'system' FROM runde";
      $this->fldUpd['edit'] = array("gruppeID", "runde");
      $this->descUpd['edit'] = array("Gruppe", "Runde");
      $this->updUpd['edit'] = "UPDATE runde SET gruppeID=:gruppeID, runde=:runde";

      $this->sqlIns['new'] = "INSERT INTO runde SET gruppeID=:gruppeID, runde=:runde";
      $this->fldIns['new'] = array("gruppeID", "runde");
      $this->descIns['new'] = array("Gruppe", "Runde");

      $this->key = "runde.ID";
      $this->required = array("gruppeID", "runde");

      $this->fieldtype = array(
        "gruppeID"=>"combo"
      );
      
      //Gruppen einfüllen
      $stmt = $this->prepareStatement("SELECT gruppe.ID AS ID, liga, gruppe FROM gruppe LEFT JOIN liga ON gruppe.ligaID=liga.ID ORDER BY liga, gruppe;");
      $this->executeStatement($stmt);
      while($row = $stmt->fetch()) {
        $this->fieldinfo['gruppeID']['datasource'][$row['ID']] = $row['liga'] . " " . $row['gruppe'];
      }

    } //End Constructor

    //Links einfügen
    function row_finalize($row, $context) {
      if ($context=="admin") {
        $prefix = '<a title="Spiele mit Spielschl&uuml;ssel erstelen und einzelne Spiele &auml;ndern" href="index.php?action=print_spiele&runde=' . $row['ID'] . '"><img src="images/spiele.gif" border=0></a> ';
        $prefix .= '<a title="Alle Spiele dieser Runde l&ouml;schen" href="index.php?action=print_delete_spiele&runde=' . $row['ID'] . '"><img src="images/deletespiele.gif" border=0></a> ';
        $prefix .= '<a title="Eigenschaften der Runde &auml;ndern" href="index.php?action=print_edit_runde&runde=' . $row['ID'] . '"><img src="images/edit.gif" border=0></a> ';
        $prefix .= '<a title="Runde löschen" href="index.php?action=print_delete_runde&runde=' . $row['ID'] . '"><img src="images/delete.gif" border=0></a> ';
        $row['liga'] = $prefix . $row['liga'];
        return $row;
      } else {
        return $row;
      }
    }

    //Runde l�schen fragen
    function askDelete($runde) {
    	$stmt = $this->prepareStatement("SELECT runde, liga, gruppe FROM runde LEFT JOIN gruppe ON runde.gruppeID=gruppe.ID LEFT JOIN liga ON gruppe.ligaID=liga.ID WHERE runde.ID=:rundeID");
    	$this->executeStatement($stmt, array('rundeID' => $runde));
      
      $row = $stmt->fetch();
      $str  = "<p>Wollen Sie die Runde <b>$row[runde]</b> aus der Liga <b>$row[liga]</b>, Gruppe <b>$row[gruppe]</b> wirklich l&ouml;schen?<br>\n";
      $str .= "<a class='btn btn-primary' href=\"index.php?action=delete_runde&runde=$runde\">Runde löschen</a> <a class='btn btn-default' href=\"javascript:history.back();\">Abbrechen</a><br>\n";
      $str .= "</p>";
      return $str;
    }

    //Runde l�schen
    function Delete($runde) {
    	$stmt = $this->prepareStatement("DELETE FROM runde WHERE ID=:runde;");
    	$stmt->execute(array('runde' => $runde));
      $str  = "<p>Runde wurde gel&ouml;scht!</p>";
      return $str;
    }

    //Spiele in Runde löschen fragen
    function askDeleteSpiel($runde) {
    	$stmt = $this->prepareStatement("SELECT runde, liga, gruppe FROM runde LEFT JOIN gruppe ON runde.gruppeID=gruppe.ID LEFT JOIN liga ON gruppe.ligaID=liga.ID WHERE runde.ID=:rundeID");
    	$stmt->execute(array('rundeID' => $runde));
      $row = $stmt->fetch();
      $str  = "<p>Wollen Sie <b>alle Spiele</b> der Runde <b>$row[runde]</b> aus der Liga <b>$row[liga]</b>, Gruppe <b>$row[gruppe]</b> wirklich l&ouml;schen?<br>\n";
      $str .= "<a class='btn btn-primary' href=\"index.php?action=delete_spiele&runde=$runde\">Spiele löschen</a> <a class='btn btn-default' href=\"javascript:history.back();\">Abbrechen</a><br>\n";
      $str .= "</p>";
      return $str;
    }

    //Runde löschen
    function DeleteSpiel($runde) {
    	$stmt = $this->prepareStatement("DELETE FROM spiel WHERE rundeID=:rundeID;");
      $stmt->execute(array('rundeID' => $runde));
      $str  = "<p>Runde wurde gel&ouml;scht!</p>";
      return $str;
    }

  } //End Class

?>
