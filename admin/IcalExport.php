<?php
include "lib.php";
//Admin-Objekt erstellen
$adm = new IVKAdmin();

$tag = $_GET[tag];
$gruppe = $_GET[gruppe];
$runde = $_GET[runde];
$team = $_GET[team];

header("Content-Type: text/calendar");
header("Content-Disposition: attachment; filename=iCalendar.ics");

$dtstamp_date = new DateTime("now");
$dtstamp = $dtstamp_date->format("Ymd\THis\Z");

echo "BEGIN:VCALENDAR\r\n";
echo "VERSION:2.0\r\n";
echo "PRODID:-//romix.ch\r\n";
echo "CALSCALE:GREGORIAN\r\n";
echo "X-WR-CALNAME:Korbball Wintermeisterschaft Zentralschweiz\r\n"; 
echo "METHOD:PUBLISH\r\n";

$games = getGames($tag, $gruppe, $runde, $team, "", $adm);

for ($i = 0; $i < count($games); $i++) {
	$row = $games[$i];
  $summary = "$row[txtTeamA] : $row[txtTeamB]";
  if ($row[resultatA]) {
    $summary .= " ($row[resultatA] : $row[resultatB])";
  } else {
	  $summary .= " (" . $row['halle'] . ")";
  }
  echo "BEGIN:VEVENT\r\n";
  echo "SUMMARY:Meisterschaftsspiel $summary\r\n";
  echo "X-MICROSOFT-CDO-BUSYSTATUS:BUSY\r\n";
  echo "URL:http://www.turnverband.ch/korbball/spielplan.php?m1=1000\r\n";
  echo "DESCRIPTION:http://www.turnverband.ch/korbball/spielplan.php?m1=1000\r\n";
  echo "DTSTAMP:$dtstamp\r\n";
  echo "UID:$row[ID]@www.turnverband.ch\r\n";
  $dayAndTime = $row[tag] . " " . $row[zeit];
  $timeZone = new DateTimeZone("Europe/Zurich");
  $datetime = new DateTime($dayAndTime, $timeZone);
  
  echo "DTSTART;TZID=Europe/Zurich:" . $datetime->format("Ymd") . "T" . $datetime->format("His") . "\r\n";
  $datetime->modify("+30 min");
  echo "DTEND:" . $datetime->format("Ymd") . "T" . $datetime->format("His") . "\r\n";
  echo "END:VEVENT\r\n";
}
echo "END:VCALENDAR\r\n";
?>