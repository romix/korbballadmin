<?php

class c_template extends BaseCRUD {

    function __construct(IVKAdmin $adm) {
    	parent::__construct($adm);
      $this->title = "Spiel-Vorlagen";

      $this->sqlSel['admin'] = "SELECT ID, template, hallen, spieltage, teams, zeilen, `system` FROM template";
      $this->fldSel['admin'] = array("template", "hallen", "spieltage", "teams", "zeilen", "system");
      $this->descSel['admin'] = array("Beschreibung", "Anzahl Hallen", "Anzahl Spieltage", "Anzahl Teams", "Anzahl Zeilen", "System");

      $this->sqlUpd['edit'] = "SELECT ID, template, hallen, spieltage, teams, zeilen, `system` FROM template";
      $this->fldUpd['edit'] = array("template", "hallen", "spieltage", "teams", "zeilen", "system");
      $this->descUpd['edit'] = array("Beschreibung", "Anzahl Hallen", "Anzahl Spieltage", "Anzahl Teams", "Anzahl Zeilen", "System");
      $this->updUpd['edit'] = "UPDATE template SET template=:template, hallen=:hallen, spieltage=:spieltage, teams=:teams, zeilen=:zeilen, `system`=:system";

      $this->sqlIns['new'] = "INSERT INTO template SET template=:template, hallen=:hallen, spieltage=:spieltage, teams=:teams, zeilen=:zeilen, `system`=:system";
      $this->fldIns['new'] = array("template", "hallen", "spieltage", "teams", "zeilen", "system");
      $this->descIns['new'] = array("Beschreibung", "Anzahl Hallen", "Anzahl Spieltage", "Anzahl Teams", "Anzahl Zeilen", "System");

      $this->key = "ID";

      $this->required = array("template", "hallen", "spieltage", "teams", "zeilen", "system");

      $this->fieldtype = array(
        "system"=>"combo"
      );

      $this->fieldinfo["system"]['datasource']["all"] = "Alle gegen alle";
      //$this->fieldinfo["system"][datasource]["ko"] = "KO-System";

    } //End Constructor

    //Links einf�gen
    function row_finalize($row, $context) {
      if ($context=="admin") {
        $row['template'] = "<a href='index.php?action=print_delete_template&template=".$row['ID']."'><img src='images/delete.svg' width=20 heigh=20 border=0 alt='l&ouml;schen'></a> " . $row['template'];
        $row['template'] = "<a href='index.php?action=print_edit_template&template=".$row['ID']."'><img src='images/edit.svg' width=20 heigh=20 border=0 alt='bearbeiten'></a> " . $row['template'];
        $row['template'] = "<a href='index.php?action=print_edit_temp_matrix&template=".$row['ID']."'><img src='images/grid.svg' width=20 heigh=20 border=0 alt='bearbeiten'></a> " . $row['template'];
        if ($row['system']=="all") {
          $row['system']="Alle gegen alle";
        } elseif ($row['system']) {
          $row['system']="KO System";
        }
        return $row;
      } else {
        return $row;
      }
    }

    //Template l�schen fragen
    function askDelete($template) {
    	$stmt = $this->prepareStatement("SELECT * FROM template WHERE ID=:ID");
    	$stmt->execute(array(":ID" => $template));
    	$row = $stmt->fetch();
      $str  = "<p>Wollen Sie die Spiel-Vorlage <b>$row[template]</b> wirklich l&ouml;schen?<br>\n";
      $str .= "<a class='btn btn-primary' href=\"index.php?action=delete_template&template=$row[ID]\">Spiel-Schlüssel löschen</a> <a class='btn btn-default' href=\"index.php?action=print_template\">Nein, zurück</a><br>\n";
      $str .= "</p>";
      $stmt->closeCursor();
      return $str;
    }

    //template l�schen
    function Delete($template) {
    	$stmt = $this->prepareStatement("DELETE FROM template WHERE ID=:ID");
    	$stmt->execute(array(':ID' => $template));
    	$stmt->closeCursor();
      return '<div class="alert alert-success" role="alert">Spiel-Schlüssel wurde gelöscht.</div>';
    }

    // Template ausw�hlen
    function editMatrix($template) {
    	$str = '';
    	
      //Tabelle template durchsuchen
      $stmt = $this->prepareStatement("SELECT * FROM template WHERE ID=:ID;");
      $stmt->execute(array(':ID' => $template));
      $rTemplate = $stmt->fetch();
      $stmt->closeCursor();

      //Formular beginnen
      $str .= "<form name=\"matrix\" action=\"index.php?action=edit_temp_matrix\" method=\"POST\">\n";

      //Kopf ausgeben
      $str .= "<h1>$rTemplate[template]</h1>\n";
      $str .= '<table class="table table-condensed table-striped" style="width: auto;">';

      // Matrix zusammenstellen
      for ($t=1; $t<=$rTemplate['spieltage']; $t++) {
        $str .= "<tr><th colspan=" . ($rTemplate['spieltage'] + 1) . ">$t. Spieltag</th></tr>\n";
        $str .= "<tr>";
        $str .= "<th>Zeile</th>";
        for($halle=1; $halle<=$rTemplate['hallen']; $halle++) {
          $str .= "<th>Spiele Halle $halle</th>";
        }
        $str .= "</tr>\n";

        for ($z=1; $z<=$rTemplate['zeilen']; $z++) {
          $str .= "<tr>\n<td>$z</td>\n";

          for ($h=1; $h<=$rTemplate['hallen']; $h++) {
            $str .= '<td style="padding-right: 30px">';
            $stmt = $this->prepareStatement("SELECT * FROM temp_matrix WHERE templateID=:templateID AND zeile=:zeile AND halle=:halle AND spieltag=:tag;");
            $stmt->execute(array(':templateID' => $template, ':zeile' => $z, ':halle' => $h, ':tag' => $t));
            $row = $stmt->fetch();
            if ($row) {
              $str .= "<input type=\"text\" name=\"teamA[" . $row['ID'] . "]\" value=\"$row[teamA]\" size=7>";
              $str .= " : ";
              $str .= "<input type=\"text\" name=\"teamB[" . $row['ID'] . "]\" value=\"$row[teamB]\" size=7>";
            } else {
              $str .= "<input type=\"text\" name=\"newTeamA[$t;$z;$h]\" size=7>";
              $str .= " : ";
              $str .= "<input type=\"text\" name=\"newTeamB[$t;$z;$h]\" size=7>";
            }
            $str .= "</td>\n";
            $stmt->closeCursor();
          }
          $str .= "</tr>\n";
        }
      }

      $str .= "</table><br>\n";

      $str .= "<input type=\"hidden\" name=\"template\" value=\"$template\">\n";
      $str .= '<button type="submit" class="btn btn-primary">Speichern</button>';
      $str .= "</form>\n";

      return $str;
    } //End editMatrix


    function ParseEditMatrix($template, $teamA, $teamB, $newTeamA, $newTeamB) {
      //Bestehende Datens�tze durchgehen
      if ($teamA) {
        foreach ($teamA as $key => $value) {
          if ($teamA[$key] || $teamB[$key]) {
          	$stmt = $this->prepareStatement("UPDATE temp_matrix SET teamA=:teamA, teamB=:teamB WHERE ID=:ID;");
          	$stmt->execute(array(':teamA' => $teamA[$key], ':teamB' => $teamB[$key], ':ID' => $key));
          } else {
          	$stmt = $this->prepareStatement("DELETE FROM temp_matrix WHERE ID=:ID;");
          	$stmt->execute(array(':ID' => $key));
          }
          $stmt->closeCursor();
        }
      }

      $sql = "INSERT INTO temp_matrix SET templateID=:templateID, spieltag=:spieltag, zeile=:zeile, halle=:halle, teamA=:teamA, teamB=:teamB;";
      $insertStmt = $this->prepareStatement($sql);
      foreach ($newTeamA as $key => $value) {
        if ($newTeamA[$key] || $newTeamB[$key]) {
          $keys = explode(";", $key);
        	$insertStmt->execute(array(':templateID' => $template, ':spieltag' => $keys[0], ':zeile' => $keys[1], ':halle' => $keys[2], ':teamA' => $newTeamA[$key], ':teamB' => $newTeamB[$key]));
		      $insertStmt->closeCursor();
        }
      }

      return '';
    } //End ParseEditMatrix


    //Matrix gem�ss dem System kontrollieren
    //erst System "Alle gegen alle" realisiert!
    function checkMatrix($template) {
    	$str = '';
      //Tabelle template durchsuchen
      $stmt = $this->prepareStatement("SELECT * FROM template WHERE ID=:ID");
      $stmt->execute(array(':ID' => $template));
      $rTemplate = $stmt->fetch();
      $stmt->closeCursor();

      if ($rTemplate['system'] == 'all') {
        //Alle gegen alle

        //Alle Paarungen erstellen
        for ($i=1; $i<=$rTemplate['teams']; $i++) {
          for ($j=$i+1; $j<=$rTemplate['teams']; $j++) {
            $paarung[$i][$j] = 0;
          }
        }

        //Paarungen eintragen
        $stmt = $this->prepareStatement("SELECT * FROM temp_matrix WHERE templateID=:templateID");
        $stmt->execute(array(':templateID' => $template));
        while($row=$stmt->fetch()) {
          if ($row['teamA']<$row['teamB']) {
            $paarung[$row['teamA']][$row['teamB']]++;
          } elseif ($row['teamA']>$row['teamB']) {
            $paarung[$row['teamB']][$row['teamA']]++;
          } else {
            $str .= "Zwei mal gleiches Team in Zeile $row[zeile] Halle $row[halle].<br>\n";
          }
        }
        $stmt->closeCursor();

        //Paarungen kontrollieren
        for ($i=1; $i<=$rTemplate['teams']; $i++) {
          for ($j=$i+1; $j<=$rTemplate['teams']; $j++) {
            if ($paarung[$i][$j]==0) {
              $str .= "Die Paarung $i : $j fehlt!<br>";
            }
            if ($paarung[$i][$j]>1) {
              $str .= "Die Paarung $i : $j ist " . $paarung[$i][$j] . " mal vorhanden !<br>";
            }
          }
        }

        //Anzahl Anspiele kontrollieren
        for ($i=1; $i<=$rTemplate['teams']; $i++) {
        	$stmt = $this->prepareStatement("SELECT ID FROM temp_matrix WHERE templateID=:templateID AND teamA=:teamID");
        	$stmt->execute(array(':templateID' => $template, ':teamID' => $i));
          $anspiel = $stmt->rowCount();
          $stmt->closeCursor();
          
          $stmt = $this->prepareStatement("SELECT ID FROM temp_matrix WHERE templateID=:templateID AND teamB=:teamID");
          $stmt->execute(array(':templateID' => $template, ':teamID' => $i));
          $keinanspiel = $stmt->rowCount();
          $stmt->closeCursor();

          if (abs($anspiel - $keinanspiel) > 1) {
            $str .= "Die Mannschaft $i hat $anspiel Anspiele und $keinanspiel mal kein Anspiel<br>\n";
          }
        }

        //Zeitabst�nde kontrollieren
        $stmt = $this->prepareStatement("SELECT * FROM temp_matrix WHERE templateID=:templateID ORDER BY spieltag, zeile;");
        $stmt->execute(array(':templateID' => $template));
        
        $vor = array();
        $jetzt = array();
        $z = 1;
        $t = 1;
        while ($row = $stmt->fetch()) {
          if ($z!=$row['zeile']) {
            if (($row['zeile']-1) == $z) {
              //Spiele folgend aufeinander?
              foreach ($jetzt as $team) {
                if (in_array($team, $vor)) {
                  $str .= "Team $team spielt am Spieltag $t in Zeile " . ($z-1) . " und $z zweimal aufeinander.<br>\n";
                }
              }
            }
            $vor = $jetzt;
            $jetzt = array();
            $z = $row['zeile'];
            if ($t!=$row['spieltag']) {
              $vor = array();
              $z = 1;
              $t = $row['spieltag'];
            }
          }
          //zwei Spiele gleichzeitig?
          if (in_array($row['teamA'], $jetzt)) {
            $str .= "Team $row[teamA] spielt am Spieltag $t in Zeile $z gleichzeitig.<br>\n";
          }
          $jetzt[] = $row['teamA'];
          if (in_array($row['teamB'], $jetzt)) {
            $str .= "Team $row[teamB] spielt am Spieltag $t in Zeile $z gleichzeitig.<br>\n";
          }
          $jetzt[] = $row['teamB'];
        }

        $stmt->closeCursor();

      }

      return $str;
    } //End checkMatrix

  } //End Class

?>