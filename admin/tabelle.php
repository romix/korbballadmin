<?php

class c_tabelle
{

  const GROUP_NAME = 'groupName';
  const ROUNDS = 'rounds';
  const TEAMS = 'teams';
  const GAMES = 'games';

  var $adm;
  var $rankings = array();

  function c_tabelle(IVKAdmin $adm)
  {
    $this->adm = $adm;
  }

  //wandelt den TeamCode in Text um
  function arrTeamCode($code)
  {

    if (strpos($code, ".")) {

      $rang = substr($code, 0, strpos($code, "."));
      $GR = substr($code, strpos($code, ".") + 1, 1);
      $GRNr = substr($code, strrpos($code, ".") + 1);

      $str = $rang . ". ";
      switch ($GR) {
        case "G":
          $stmt = $this->adm->prepareStatement("SELECT * FROM gruppe WHERE ID=:groupID;");
          $stmt->execute(array('groupID' => $GRNr));
          $row = $stmt->fetch();
          $str .= "Gruppe " . $row['gruppe'] . " ";

          $stmt = $this->adm->prepareStatement("SELECT * FROM spiel WHERE gruppeID=:gruppeID AND (punkteA > 0 OR punkteB > 0);");
          $stmt->execute(array('gruppeID' => $GRNr));
          if ($stmt->rowCount() >= 3) {
            //Effektives Team gemäss der aktuellen Resultate suchen
            $r = $this->rankings[$GRNr];
            if (!$r) {
              $r = new c_rangliste($this->adm);
              $r->setGroup($GRNr);
              $this->rankings[$GRNr] = $r;
            }
            $arrRangListe = $r->getRangliste();
            foreach ($arrRangListe as $Krang => $rankingTeams) {
              foreach ($rankingTeams as $Vrang) {
                if ($rang == $Krang + 1) {
                  $arrTeam = $this->arrTeamCode($Vrang);
                  $arr['team'] = $arrTeam['txt'];
                }
              }
            }
          }

          break;
        case "R":
          $sql = "SELECT runde, gruppe FROM runde LEFT JOIN gruppe ON runde.gruppeID = gruppe.ID WHERE runde.ID = :roundId";
          $stmt = $this->adm->prepareStatement($sql);
          $stmt->execute(array('roundId' => $GRNr));
          $row = $stmt->fetch();

          $str .= 'Gruppe ' . $row['gruppe'] . ' ' . $row['runde'];
          break;

        case "S":
          if ($rang == '1') {
            $str = 'Sieger ';
          } else if ($rang == '2') {
            $str = 'Verlierer ';
          }
          $str .= 'Spiel ' . $GRNr;
          $sql = "SELECT teamA, teamB, punkteA, punkteB FROM spiel WHERE ID = :Id";
          $stmt = $this->adm->prepareStatement($sql);
          $stmt->execute(array('Id' => $GRNr));
          $row = $stmt->fetch();
        
          if ($row['punkteA'] > $row['punkteB']) {
            $teamCode = $this->arrTeamCode($row['teamA']);
            $arr['team'] = $teamCode['team'];
          } else if ($row['punkteA'] < $row['punkteB']) {
            $teamCode = $this->arrTeamCode($row['teamB']);
            $arr['team'] = $teamCode['team'];
          } else {
            $arr['team'] = 'Resultat ausstehend';
          }
          break;
      }

    } else {

      $sql = "SELECT * FROM mannschaft WHERE ID=:code";
      $stmt = $this->adm->prepareStatement($sql);
      $stmt->execute(array('code' => $code));
      $row = $stmt->fetch();
      $str = $row['mannschaft'];
      $arr['team'] = $row['mannschaft'];
    }

    $arr['txt'] = $str;
    return $arr;
  }

  function getGroupTable($gruppe)
  {
    $table = array();

    // group
    $group = $this->readGroup($gruppe);
    $table[c_tabelle::GROUP_NAME] = $group['gruppe'];
    $gruppeArt = $group["typ"];

    // rounds
    $rounds = $this->readRounds($gruppe);
    $anzRunde = 0;
    foreach ($rounds as $roundRow) {
      $runde[$roundRow['ID']] = $roundRow['runde'];
      $anzRunde++;
    }
    $table[c_tabelle::ROUNDS] = $runde;

    //search for teams and save them in $team
    $teams = array();
    if ($gruppeArt == "liga") {
      //Normal group
      $sql = "SELECT mannschaft.ID, mannschaft.mannschaft
			FROM teamgruppe LEFT JOIN mannschaft ON teamgruppe.teamID=mannschaft.ID
			WHERE teamgruppe.gruppeID=:gruppeID
			ORDER BY mannschaft";
      $teamQuery = $this->adm->pdodb->prepare($sql);
      if (!$teamQuery->execute(array(':gruppeID' => $gruppe))) {
        echo $teamQuery->errorInfo();
        return;
      }
      $anzTeam = 0;
      while ($teamRow = $teamQuery->fetch()) {
        $teams[$teamRow['ID']] = $teamRow['mannschaft'];
      }

    } elseif ($gruppeArt == "final" || $gruppeArt == "cup") {
      //Final group
      $sql = "SELECT teamA FROM spiel where gruppeID=:gruppeID GROUP BY teamA ORDER BY teamA";
      $teamQuery = $this->adm->pdodb->prepare($sql);
      if (!$teamQuery->execute(array(':gruppeID' => $gruppe))) {
        echo $teamQuery->errorInfo();
        return;
      }
      $anzTeam = 0;
      while ($teamRow = $teamQuery->fetch()) {
        $arrTeam = $this->arrTeamCode($teamRow['teamA']);
        $teamName = $arrTeam['txt'];
        if ($arrTeam['team']) {
          $teamName .= $arrTeam['team'];
        }
        $teams[$teamRow['teamA']] = $teamName;
        $anzTeam++;
      }
    }
    $table[c_tabelle::TEAMS] = $teams;

    //Alle Spiele importieren
    $sql = "SELECT * FROM `spiel` WHERE spiel.gruppeID=:gruppeID";
    $gamesQuery = $this->adm->pdodb->prepare($sql);
    if (!$gamesQuery->execute(array(':gruppeID' => $gruppe))) {
      echo $gamesQuery->errorInfo();
      return;
    }
    while ($gamesRow = $gamesQuery->fetch()) {
      $spiel[$gamesRow['rundeID']][$gamesRow['teamA']][$gamesRow['teamB']] = $gamesRow['resultatA'] . "&nbsp;:&nbsp;" . $gamesRow['resultatB'];
      $spiel[$gamesRow['rundeID']][$gamesRow['teamB']][$gamesRow['teamA']] = $gamesRow['resultatB'] . "&nbsp;:&nbsp;" . $gamesRow['resultatA'];
    }

    $table[c_tabelle::GAMES] = $spiel;

    return $table;
  }

  private function readGroup($gruppe)
  {
    $groupQuery = $this->adm->pdodb->prepare("SELECT * FROM gruppe WHERE ID=:gruppeID");
    $groupQuery->execute(array(':gruppeID' => $gruppe));
    $group = $groupQuery->fetch();
    $groupQuery->closeCursor();
    return $group;
  }

  private function readRounds($gruppe)
  {
    $sql = "SELECT * FROM runde WHERE gruppeID=:gruppeID order by ID";
    $roundQuery = $this->adm->pdodb->prepare($sql);
    $roundQuery->execute(array(':gruppeID' => $gruppe));
    $rounds = array();
    while ($round = $roundQuery->fetch()) {
      $rounds[$round['ID']] = $round;
    }
    $roundQuery->closeCursor();
    return $rounds;
  }

  //gibt eine Tabelle einer angegebenen Gruppe zurck
  function renderGroupTable($gruppe = '1', $useTeamImages = true)
  {
    $table = $this->getGroupTable($gruppe);
    $NameGruppe = $table[c_tabelle::GROUP_NAME];
    $runde = $table[c_tabelle::ROUNDS];
    $team = $table[c_tabelle::TEAMS];
    $spiel = $table[c_tabelle::GAMES];
    $r = new c_rangliste($this->adm);
    $r->setGroup($gruppe);

    //Tabelle ausgeben
    $str = "<table border=1 bordercolor=black cellpadding=3 cellspacing=0 class='resulttable'>\n";
    $str .= "<tr>\n";
    $str .= "<td class=\"group_name\" colspan='2'><br>$NameGruppe</td>\n";
    foreach ($team as $Vteam) {
      if ($useTeamImages) {
        $str .= "<td style='vertical-align: bottom;'>";
        $str .= "<img style='float: none;' src='admin/teamImage.php?text=$Vteam'/>";
      } else {
        $str .= "<td class=\"team_header\">";
        $str .= $Vteam;
      }
      $str .= "</td>\n";
    }
    $str .= "</tr>\n";

    //Zeile für Zeile durchlaufen
    $anzRunde = count($runde);
    foreach ($team as $Kteam => $Vteam) {
      $str .= "<tr>\n";
      $str .= "<td rowspan='$anzRunde'>$Vteam</td>\n";

      //Mit jeder Runde eine Zeile machen
      foreach ($runde as $Krunde => $Vrunde) {

        $str .= "<td>$Vrunde</td>\n";

        //Wieder jede Zeile horizontal durchlaufen
        $teamHor = $team;
        foreach ($teamHor as $KteamHor => $VteamHor) {
          if ($Kteam == $KteamHor) {
            $str .= "<td bgcolor='#474C4E'></td>\n";
          } else {
            $str .= "<td align='center'>" . $spiel[$Krunde][$Kteam][$KteamHor] . "</td>\n";
          }
        }

        $str .= "</tr><tr>\n";

      } // ende foreach $runde

      $str .= "</tr>\n";
    } // ende foreach team vertikal


    // Jetzt geben wir das Korbverhätnis aus
    $str .= "<tr>\n<td colspan='2'>K&ouml;rbe</td>\n";
    foreach ($team as $Kteam => $Vteam) {
      $Koerbe = $r->korbVerhaeltnis($Kteam);
      $str .= "<td align='center'>$Koerbe[1]:$Koerbe[0]</td>\n";
      $KorbVerhaeltnis[] = $Koerbe[0] - $Koerbe[1];
    }
    $str .= "</tr>\n";
    $str .= "<tr>\n<td colspan='2'>Korbverh&auml;ltnis</td>\n";
    foreach ($KorbVerhaeltnis as $KV) {
      $str .= "<td align='center'>$KV</td>\n";
    }
    $str .= "</tr>\n";

    //Punkte ausgeben
    $str .= "<tr>\n<td colspan='2'>Punkte</td>\n";
    foreach ($team as $Kteam => $Vteam) {
      if (isset($Kteam)) {
        $punkte = $r->punkte($Kteam);
      } else {
        $punkte = '0';
      }
      $str .= "<td align='center'>$punkte</td>\n";
    }
    $str .= "</tr>\n";

    //Rang ausgeben
    $ranking = $r->getRangliste();
    $str .= "<tr>\n<th colspan='2'>Rang</th>\n";
    foreach ($team as $Kteam => $Vteam) {
      foreach ($ranking as $rank => $rankTeams) {
        foreach ($rankTeams as $rankTeam) {
          if ($rankTeam == $Kteam) {
            $str .= '<th>' . ($rank + 1) . '</th>';
          }
        }
      }
    }
    $str .= "</tr>\n";

    $str .= "</table>\n";

    return $str;
  }

  function renderGroupTableForBootstrap($gruppe)
  {
    $table = $this->getGroupTable($gruppe);
    $NameGruppe = $table[c_tabelle::GROUP_NAME];
    $runde = $table[c_tabelle::ROUNDS];
    $team = $table[c_tabelle::TEAMS];
    $spiel = $table[c_tabelle::GAMES];
    $r = new c_rangliste($this->adm);
    $r->setGroup($gruppe);

    //Tabelle ausgeben
    $str = '<div class="col-md-12">';
    $str .= '<table class="table table-condensed table-bordered">';
    $str .= "<thead><tr>\n";
    $str .= "<th colspan='2'>$NameGruppe</th>\n";
    foreach ($team as $Vteam) {
      $str .= "<td style='vertical-align: bottom; text-align:center;'>";
      $str .= "<img style='float: none;' src='teamImage.php?text=$Vteam'/>";
      $str .= "</td>\n";
    }
    $str .= "</tr></thead>\n";

    //Zeile f�r Zeile durchlaufen
    $str .= '<tbody>';
    $anzRunde = count($runde);
    foreach ($team as $Kteam => $Vteam) {
      $str .= "<tr>\n";
      $str .= "<td rowspan='$anzRunde'>$Vteam</td>\n";

      //Mit jeder Runde eine Zeile machen
      foreach ($runde as $Krunde => $Vrunde) {

        $str .= "<td>$Vrunde</td>\n";

        //Wieder jede Zeile horizontal durchlaufen
        $teamHor = $team;
        foreach ($teamHor as $KteamHor => $VteamHor) {
          if ($Kteam == $KteamHor) {
            $str .= "<td bgcolor='#474C4E'></td>\n";
          } else {
            $str .= "<td align='center'>" . $spiel[$Krunde][$Kteam][$KteamHor] . "</td>\n";
          }
        }

        $str .= "</tr><tr>\n";

      } // ende foreach $runde

      $str .= "</tr>\n";
    } // ende foreach team vertikal


    // Jetzt geben wir das Korbverhätnis aus
    $str .= "<tr>\n<td colspan='2'>K&ouml;rbe</td>\n";
    foreach ($team as $Kteam => $Vteam) {
      $Koerbe = $r->korbVerhaeltnis($Kteam);
      $str .= "<td align='center'>$Koerbe[1]:$Koerbe[0]</td>\n";
      $KorbVerhaeltnis[] = $Koerbe[0] - $Koerbe[1];
    }
    $str .= "</tr>\n";
    $str .= "<tr>\n<td colspan='2'>Korbverh&auml;ltnis</td>\n";
    foreach ($KorbVerhaeltnis as $KV) {
      $str .= "<td align='center'>$KV</td>\n";
    }
    $str .= "</tr>\n";

    //Punkte ausgeben
    $str .= "<tr>\n<td colspan='2'>Punkte</td>\n";
    foreach ($team as $Kteam => $Vteam) {
      if (isset($Kteam)) {
        $punkte = $r->punkte($Kteam);
      } else {
        $punkte = '0';
      }
      $str .= "<td align='center'>$punkte</td>\n";
    }
    $str .= "</tr>\n";

    //Rang ausgeben
    $ranking = $r->getRangliste();
    $str .= '<tr class="active"><th colspan="2">Rang</th>';
    foreach ($team as $Kteam => $Vteam) {
      foreach ($ranking as $rank => $rankTeams) {
        foreach ($rankTeams as $rankTeam) {
          if ($rankTeam == $Kteam) {
            $str .= '<th class="text-center">' . ($rank + 1) . '</th>';
          }
        }
      }
    }
    $str .= "</tr>\n";

    $str .= "</table></div>\n";

    return $str;
  }

  //gibt eine Tabelle aller Gruppen mit den Links auf die Tabelle zurck
  function printTableLinks()
  {
    $stmt = $this->adm->prepareStatement("SELECT * from gruppe ORDER BY gruppe;");
    $stmt->execute();

    $str = '<div class="col-sm-12 col-md-6">';
    $str .= '<h3>Kreuztabellen</h3>';
    $str .= '<p>Wähle eine der folgenden Gruppen um deren Kreuztabelle zu sehen:</p>';
    $str .= '<div class="list-group">';

    while ($row = $stmt->fetch()) {
      $str .= '<a class="list-group-item" href="single_tabelle.php?gruppe=' . $row['ID'] . '" target="_blank">' . $row['gruppe'] . '</a>';
    }

    $str .= '</div>';
    $str .= '</div>';

    return $str;
  }

  /**
   * Print links to results
   *
   * @return string
   */
  function printResultLinks()
  {
    $stmt = $this->adm->prepareStatement("SELECT * from gruppe ORDER BY gruppe;");
    $stmt->execute();

    $str = '<div class="col-sm-12 col-md-6">';
    $str .= "<h3>Resultate</h3>";
    $str .= '<p>Wähle eine der folgenden Gruppen um deren Resultate-Tabelle zu sehen:</p>';
    $str .= '<div class="list-group">';

    while ($row = $stmt->fetch()) {
      $str .= '<a class="list-group-item" href="?action=print_result&gruppe=' . $row['ID'] . '">' . $row['gruppe'] . '</a>';
    }

    $str .= '</div>';
    $str .= '</div>';
    return $str;
  }

  /**
   * Print actual results of a group
   * @param string $gruppe
   * @return string
   */
  function printResult($gruppe)
  {
    $group = $this->readGroup($gruppe);
    $rounds = $this->readRounds($gruppe);

    $stmt = $this->adm->prepareStatement("SELECT * FROM spiel WHERE gruppeID=:gruppeID AND (punkteA >= 0 OR punkteB >= 0) ORDER BY tag, zeit;");
    $stmt->execute(array('gruppeID' => $gruppe));
    while ($game = $stmt->fetch()) {
      $rounds[$game['rundeID']]['games'][$game['tag']][] = $game;
    }
    $stmt->closeCursor();

    $str = '<div class="row">';
    $str .= '<div class="col-lg-7 col-md-9 col-sm-12 col-xs-12">';
    $str .= '<h2>Resultate ' . $group['gruppe'] . '</h2>';

    foreach ($rounds as $round) {
      if(get('games',$round,false)) {
        foreach ($round['games'] as $day => $games) {
          $gameStr = '<h3>' . $round['runde'] . ' ' . dat_m2u($day) . ':</h3>';
          $gameStr .= '<table class="table table-striped table-condensed">';
          foreach ($games as $gameId => $game) {
            $gameStr .= '<tr>';
            $gameStr .= '<td>' . substr($game['zeit'], 0, strpos($game['zeit'], ':', 3)) . '</td>';
            $gameStr .= '<td>' . $this->arrTeamCode($game['teamA'])['txt'] . '</td>';
            $gameStr .= '<td>' . $this->arrTeamCode($game['teamB'])['txt'] . '</td>';
            $gameStr .= '<td class="text-right">' . $game['resultatA'] . ' : ' . '</td>';
            $gameStr .= '<td class="text-right" style="width: 26px;">' . $game['resultatB'] . '</td>';
            $gameStr .= '</tr>';
          }
          $str .= '</table>';
        }
      }
    }
    if(!isset($gameStr)) {
      $str .= '<span>Es haben noch keine Spiele stattgefunden.</span>';
    } else {
      $str .= $gameStr;
    }

    $str .= '</div></div>';

    return $str;
  }

  /**
   * Simply print a link to the ranking in a single page for embedding into own website as iframe
   * @param string $gruppe
   */
  function printRankingSinglePageLink($gruppe)
  {
    $group = $this->readGroup($gruppe);
    $str = '<div class="alert alert-info" role="alert">';
    $str .= 'Möchtest du die Rangliste auf deiner Homepage einbetten? Dann benutze den folgenden Link und zeige die Seite in einem IFrame an: ';
    $str .= '<a class="btn btn-info" href="single_ranking.php?gruppe=' . $group['ID'] . '" target="_blank">';
    $str .= 'Gruppe ' . $group['gruppe'] . ' als IFrame';
    $str .= '</a>';
    $str .= '</div>';
    return $str;
  }

  /**
   * Print ranking of a group
   * @param string $gruppe
   */
  function printRanking($gruppe, $onlyTooltips = true)
  {
    $group = $this->readGroup($gruppe);
    $r = new c_rangliste($this->adm);
    $r->setGroup($gruppe);
    $str = '<div class="row">';
    $str .= '<div>';
    $str .= '<h2>Rangliste ' . $group['gruppe'] . '</h2>';
    $str .= '<table class="ranking-table">';
    $str .= '<tr>';
    $str .= '<th></th>';
    $str .= '<th></th>';
    if ($onlyTooltips) {
      $str .= '<th class="text-right"><span class="glyphicon glyphicon-question-sign" title="Anzahl gespielter Spiele"></span></th>';
      $str .= '<th class="text-right"><span class="glyphicon glyphicon-question-sign" title="gewonnen"></span></th>';
      $str .= '<th class="text-right"><span class="glyphicon glyphicon-question-sign" title="verloren"></span></th>';
      $str .= '<th class="text-right"><span class="glyphicon glyphicon-question-sign" title="unentschieden"></span></th>';
      $str .= '<th class="text-right"><span class="glyphicon glyphicon-question-sign" title="geschossen : erhalten"></span></th>';
      $str .= '<th class="text-right"><span class="glyphicon glyphicon-question-sign" title="Torverhältnis"></span></th>';
      $str .= '<th class="text-right"><span class="glyphicon glyphicon-question-sign" title="Punkte"></span></th>';
    } else {
      $str .= '<th class="text-right">gespielt</th>';
      $str .= '<th class="text-right">gewonnen</th>';
      $str .= '<th class="text-right">verloren</th>';
      $str .= '<th class="text-right">unentschieden</th>';
      $str .= '<th class="text-right">Korbverhältnis</th>';
      $str .= '<th class="text-right">Torverhältnis</th>';
      $str .= '<th class="text-right">Punkte</th>';
    }
    $str .= '</tr>';
    $ranking = $r->getRangliste();
    foreach ($ranking as $rank => $teams) {
      foreach ($teams as $team) {
        $str .= '<tr>';
        $str .= '<td>' . (1 + $rank) . '.</td>';
        $teamCode = $this->arrTeamCode($team);
        $str .= '<td>' . $teamCode['txt'];
        if (get('team', $teamCode, false) && !$teamCode['txt'] == $teamCode['team']) {
          $str .= ': ' . $teamCode['team'];
        }
        $str .= '</td>';
        $str .= '<td class="text-right">' . $r->anzahlGespielteSpiele($team) . '</td>';
        $str .= '<td class="text-right">' . $r->gamesWon($team) . '</td>';
        $str .= '<td class="text-right">' . $r->gamesLost($team) . '</td>';
        $str .= '<td class="text-right">' . $r->gamesTie($team) . '</td>';
        $str .= '<td class="text-right">' . $r->korbVerhaeltnis($team)[0] . ' : ' . $r->korbVerhaeltnis($team)[1] . '</td>';
        $rel = ($r->korbVerhaeltnis($team)[0] - $r->korbVerhaeltnis($team)[1]);
        if ($rel >= 0) {
          $rel = '+' . $rel;
        }
        $str .= '<td class="text-right">' . $rel . '</td>';
        $str .= '<td class="text-right">' . $r->punkte($team) . '</td>';
        $str .= '</tr>';
      }
    }
    $str .= '</table>';
    $str .= '</div></div>';

    $annotations = $r->getAllAnnotations();
    if (count($annotations) > 0) {
      $str .= '<div class="row">';
      $str .= '<div class="col-lg-7 col-md-9 col-sm-12 col-xs-12">';
      $str .= '<h4>Bemerkungen:</h4>';
      foreach ($annotations as $teamCode => $annosPerTeam) {
        $team = $this->arrTeamCode($teamCode);
        foreach ($annosPerTeam as $anno) {
          $str .= $team['txt'];
          if ($team['team']) {
            $str .= ' (' . $team['team'] . ')';
          }
          $str .= ': ' . $anno . '<br/>';
        }
      }
      $str .= '</div></div>';
    }

    return $str;
  }
}

?>
