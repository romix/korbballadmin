<?php

class PresentationConfig
{

  const PICS_DIR = "./uploads/presentation/";
  const PICS_URL_PATH = "uploads/presentation/";

  /**
   * @var IVKAdmin
   */
  private $adm;

  function PresentationConfig(IVKAdmin $adm)
  {
    $this->adm = $adm;
  }

  public function printPresentationConfig()
  {
    $str = $this->head();
    $pics = $this->loadPics();
    $str .= $this->adm->getTemplates()->render('presentation/pictures', ['pics' => $pics]);

    $rows = $this->loadData();
    $newDay = new DayResponsibility(null, '', '', '', '');
    $str .= $this->showResponsible(array_merge($rows, array($newDay)));
    return $str;
  }


  public function uploadPicture($files)
  {
    $picInfo = get('picUpload', $files);

    if ($picInfo != null) {
      $mime = mime_content_type($picInfo['tmp_name']);
      $newFileName = pathinfo($picInfo['name'], PATHINFO_FILENAME);
      if ($this->isValidFilename($newFileName)) {
        $newFileName = PresentationConfig::PICS_DIR . $newFileName;
        if (substr($mime, -4) == "jpeg") {
          move_uploaded_file($picInfo['tmp_name'], $newFileName . '.jpg');
        } else if (substr($mime, -3) == "png") {
          move_uploaded_file($picInfo['tmp_name'], $newFileName . '.png');
        }
      }
    }
  }

  public function deletePicture($data)
  {
    $file = $data['pic'];
    if ($this->isValidFilename($file)) {
      unlink(PresentationConfig::PICS_DIR . $file);
    }
  }

  public function getDayResponsibilityFor($day)
  {
    $resp = $this->loadData();
    foreach ($resp as $resp) {
      if ($resp->getDay() == $day) {
        return $resp;
      }
    }
    return false;
  }

  private function isValidFilename($file)
  {
    return !preg_match('([^\w\s\d\-_~,;\[\]\(\).])', $file);
  }

  private function head()
  {
    return $this->adm->getTemplates()->render('presentation/head', []);
  }

  private function showResponsible(array $dayResponsibilities)
  {
    $pics = $this->loadPics();
    return $this->adm->getTemplates()->render('presentation/configForm', ['resps' => $dayResponsibilities, 'pics' => $pics]);
  }

  private function loadData()
  {
    $stmt = $this->adm->prepareStatement('SELECT * from day_responsibility ORDER BY day;');
    $stmt->execute(array());
    $rows = array();
    while ($row = $stmt->fetch()) {
      $rows[] = new DayResponsibility($row['ID'], $row['day'], $row['caption'], $row['subcaption'], $row['picname']);
    }
    $stmt->closeCursor();
    return $rows;
  }

  private function loadPics()
  {
    $pics = array();
    $files = false;
    if (is_dir(PresentationConfig::PICS_DIR)) {
      $files = scandir(PresentationConfig::PICS_DIR);
    }

    if ($files) {
      foreach ($files as $file) {
        if (substr($file, 0, 1) != ".") {
          $pic = array(
            'fileName' => $file,
            'fileUrl' => PresentationConfig::PICS_URL_PATH . $file,
            'height' => 200
          );
          $pics[] = $pic;
        }
      }
    }
    return $pics;
  }

  public function updateResponsible($postData)
  {
    $dayResponsibility = new DayResponsibility($postData['ID'], $postData['datum'], $postData['ueberschrift'], $postData['unterschrift'], $postData['picture']);
    if (array_key_exists('save_btn', $postData)) {
      $this->saveResponsible($dayResponsibility);
    } else if (array_key_exists('delete_btn', $postData)) {
      $this->deleteResponsible($dayResponsibility);
    } else if (array_key_exists('new_btn', $postData)) {
      $this->newResponsible($dayResponsibility);
    }
  }

  private function saveResponsible(DayResponsibility $dayResponsibility)
  {
    $stmt = $this->adm->prepareStatement('UPDATE day_responsibility SET day=:day, caption=:caption, subcaption=:subcaption, picname=:picname WHERE ID=:ID;');
    $stmt->execute(array(
      ':ID' => $dayResponsibility->getID(),
      ':day' => $dayResponsibility->getDay(),
      ':caption' => $dayResponsibility->getCaption(),
      ':subcaption' => $dayResponsibility->getSubCaption(),
      ':picname' => $dayResponsibility->getPicName()
    ));
    $stmt->closeCursor();
    $str = '<div class="alert alert-success" role="alert">Verantwortliche Person erfolgreich gespeichert.</div>';
    return $str;
  }

  private function deleteResponsible(DayResponsibility $dayResponsibility)
  {
    $stmt = $this->adm->prepareStatement('DELETE FROM day_responsibility WHERE ID=:ID;');
    $stmt->execute(array(
      ':ID' => $dayResponsibility->getID()
    ));
    $stmt->closeCursor();
    $str = '<div class="alert alert-success" role="alert">Verantwortliche Person erfolgreich gelöscht.</div>';
    return $str;
  }

  private function newResponsible(DayResponsibility $dayResponsibility)
  {
    $stmt = $this->adm->prepareStatement('INSERT INTO day_responsibility SET day=:day, caption=:caption, subcaption=:subcaption, picname=:picname;');
    $stmt->execute(array(
      ':day' => $dayResponsibility->getDay(),
      ':caption' => $dayResponsibility->getCaption(),
      ':subcaption' => $dayResponsibility->getSubCaption(),
      ':picname' => $dayResponsibility->getPicName()
    ));
    $stmt->closeCursor();
    $str = '<div class="alert alert-success" role="alert">Verantwortliche Person erfolgreich hinzugefügt.</div>';
    return $str;
  }

}

class DayResponsibility
{

  var $id;
  var $day;
  var $caption;
  var $subCaption;
  var $picName;

  function DayResponsibility($id, $day, $caption, $subCaption, $picName)
  {
    $this->id = $id;
    $this->day = $day;
    $this->caption = $caption;
    $this->subCaption = $subCaption;
    $this->picName = $picName;
  }

  public function getID()
  {
    return $this->id;
  }

  public function getDay()
  {
    return $this->day;
  }

  public function setDay($day)
  {
    $this->day = $day;
  }

  public function getCaption()
  {
    return $this->caption;
  }

  public function setCaption($caption)
  {
    $this->caption = $caption;
  }

  public function getSubCaption()
  {
    return $this->subCaption;
  }

  public function setSubCaption($subCaption)
  {
    $this->subCaption = $subCaption;
  }

  public function getPicName()
  {
    return $this->picName;
  }

  public function setPicName($picName)
  {
    $this->picName = $picName;
  }

  public function getPicUrl()
  {
    return PresentationConfig::PICS_URL_PATH . $this->getPicName();
  }

  public function getPicture(array $pics)
  {
    $foundPic = false;
    foreach ($pics as $pic) {
      if ($this->getPicName() == $pic['fileName']) {
        $foundPic = $pic;
        break;
      }
    }
    return $foundPic;
  }
}