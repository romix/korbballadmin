<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit2212efc73dba482539abdf79bf35dcce
{
    public static $prefixLengthsPsr4 = array (
        'L' => 
        array (
            'League\\Plates\\' => 14,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'League\\Plates\\' => 
        array (
            0 => __DIR__ . '/..' . '/league/plates/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit2212efc73dba482539abdf79bf35dcce::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit2212efc73dba482539abdf79bf35dcce::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
