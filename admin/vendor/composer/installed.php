<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '3873def382aa1e36dbf837d0a96b86c277f3d8d0',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '3873def382aa1e36dbf837d0a96b86c277f3d8d0',
            'dev_requirement' => false,
        ),
        'league/plates' => array(
            'pretty_version' => '3.3.0',
            'version' => '3.3.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/plates',
            'aliases' => array(),
            'reference' => 'b1684b6f127714497a0ef927ce42c0b44b45a8af',
            'dev_requirement' => false,
        ),
    ),
);
