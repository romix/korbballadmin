<?php

// This class takes registrations and saves them. This is used for the registration form.

class SaveAnmeldung {

	private $adm;
	private $mailReceiver;

	//Constructor
	function SaveAnmeldung(IVKAdmin $adm) {
		$this->adm = $adm;
	}

	function anmelden(AnmeldungData $data) {
		$this->doAnmeldung($data);
	}

	private function doAnmeldung(AnmeldungData $data) {
		$stmt = $this->adm->pdodb->prepare('insert into anmeldung set
				verein=:verein,
				verband=:verband,
				mNummer=:mNummer,
				kategorie=:kategorie,
				malter=:malter,
				liga=:liga,
				minGebDat=:minGebDat,
				maxGebDat=:maxGebDat,
				tenueShirt=:tenueShirt,
				tenueHose=:tenueHose,
				ersatzShirt=:ersatzShirt,
				ersatzHose=:ersatzHose,
				vName=:vName,
				vAdresse=:vAdresse,
				vPLZOrt=:vPLZOrt,
				vTel=:vTel,
				vMail=:vMail,
				jName=:jName,
				jAdresse=:jAdresse,
				jPLZOrt=:jPLZOrt,
				jTel=:jTel,
				jMail=:jMail,
				mName=:mName,
				mAdresse=:mAdresse,
				mPLZOrt=:mPLZOrt,
				mTel=:mTel,
				mMail=:mMail,
				kaName=:kaName,
				kaAdresse=:kaAdresse,
				kaPLZOrt=:kaPLZOrt,
				kaTel=:kaTel,
				kaMail=:kaMail,
				sFirstname=:sFirstname,
				sLastname=:sLastname,
				sMail=:sMail,
				bank=:bank,
				konflikt=:konflikt,
				bemerkungen=:bemerkungen,
				datum=CURDATE(),
				saison=:saison');
		$params = array(
				'verein' => $data->verein,
				'verband' => $data->verband,
				'mNummer' => $data->mNummer,
				'kategorie' => $data->kategorie,
				'malter' => $data->alter,
				'liga' => $data->liga,
				'minGebDat' => $data->gebAlt,
				'maxGebDat' => $data->gebJung,
				'tenueShirt' => $data->tenueShirt,
				'tenueHose' => $data->tenueHose,
				'ersatzShirt' => $data->ersatzShirt,
				'ersatzHose' => $data->ersatzHose,
				'vName' => $data->korrespondenz->name,
				'vAdresse' => $data->korrespondenz->adresse,
				'vPLZOrt' => $data->korrespondenz->plzOrt,
				'vTel' => $data->korrespondenz->tel,
				'vMail' => $data->korrespondenz->mail,
				'jName' => $data->jugend->name,
				'jAdresse' => $data->jugend->adresse,
				'jPLZOrt' => $data->jugend->plzOrt,
				'jTel' => $data->jugend->tel,
				'jMail' => $data->jugend->mail,
				'mName' => $data->mannschaft->name,
				'mAdresse' => $data->mannschaft->adresse,
				'mPLZOrt' => $data->mannschaft->plzOrt,
				'mTel' => $data->mannschaft->tel,
				'mMail' => $data->mannschaft->mail,
				'kaName' => $data->kassier->name,
				'kaAdresse' => $data->kassier->adresse,
				'kaPLZOrt' => $data->kassier->plzOrt,
				'kaTel' => $data->kassier->tel,
				'kaMail' => $data->kassier->mail,
				'sFirstname' => $data->schiedsrichter->firstname,
				'sLastname' => $data->schiedsrichter->lastname,
				'sMail' => $data->schiedsrichter->mail,
				'bank' => $data->bank,
				'konflikt' => $data->konflikt,
				'bemerkungen' => $data->bemerkungen,
				'saison' => $this->adm->saison);
		$success = $stmt->execute($params);
		$affectedRows = $stmt->rowCount();
		print_r($stmt->errorInfo());
		if ($success) {
			$this->sendAnmeldeMail($data);
		} else {
			throw new Exception('Anmeldung fehlgeschlagen. Daten konnten nicht gespeichert werden.');
		}
	}

	private function sendAnmeldeMail(AnmeldungData $data) {
		$this->mailReceiver = $data->korrespondenz->mail;
		if (isset($data->mannschaft->mail)) {
			$this->mailReceiver .= ";" . $data->mannschaft->mail;
		}
		if (isset($data->jugend->mail)) {
			$this->mailReceiver .= ";" . $data->jugend->mail;
		}
		$subject = "Anmeldung Innerschweizer Korbball Wintermeisterschaft";

		$bank = nl2br(htmlspecialchars($data->bank));
		$konflikt = nl2br(htmlspecialchars($data->konflikt));
		$bemerkungen = nl2br(htmlspecialchars($data->bemerkungen));
		$k = $data->korrespondenz;
		$k_name = $k->name;
		$k_adresse = $k->adresse;
		$k_plzOrt = $k->plzOrt;
		$k_tel = $k->tel;
		$k_mail = $k->mail;
		$j = $data->jugend;
		$j_name = $j->name;
		$j_adresse = $j->adresse;
		$j_plzOrt = $j->plzOrt;
		$j_tel = $j->tel;
		$j_mail = $j->mail;
		$m = $data->mannschaft;
		$m_name = $m->name;
		$m_adresse = $m->adresse;
		$m_plzOrt = $m->plzOrt;
		$m_tel = $m->tel;
		$m_mail = $m->mail;
		$ka = $data->kassier;
		$ka_name = $ka->name;
		$ka_adresse = $ka->adresse;
		$ka_plzOrt = $ka->plzOrt;
		$ka_tel = $ka->tel;
		$ka_mail = $ka->mail;
		$s = $data->schiedsrichter;
		$s_firstname = $s->firstname;
		$s_lastname = $s->lastname;
		$s_mail = $s->mail;
		$body = <<<EOF
<html>
  <body>
    <p>Hallo $k_name</p>
    <p>Wir haben deine Anmeldung für die kommende Saison erhalten. Folgende Daten wurden uns übermittelt:</p>
    <table border='0'>
    <tr>
    <td>Verein:</td>
    <td>$data->verein</td>
    </tr>
    <tr>
    <td>Verband:</td>
    <td>$data->verband</td>
    </tr>
    <tr>
    <td>Mannschaftsnummer:</td>
    <td>$data->mNummer</td>
    </tr>
    <tr>
    <td>Kategorie:</td>
    <td>$data->kategorie</td>
    </tr>
    <tr>
    <td>Alter:</td>
    <td>$data->alter</td>
    </tr>
EOF;
		if ($data->alter == "Aktiv") {
			$body .= <<<EOF
		<tr>
    <td>Liga:</td>
    <td>$data->liga</td>
    </tr>
EOF;
		} else {
			$body .= <<<EOF
    <tr>
    <td>Geb.-Datum älteste/r Spieler/in:</td>
    <td>$data->gebAlt</td>
    </tr>
    <tr>
    <td>Geb.-Datum jüngste/r Spieler/in:</td>
    <td>$data->gebJung</td>
    </tr>
	  <tr>
    <th align="left">Jugend-Verantwortliche(r):</th>
    </tr>
    <tr>
    <td>Name:</td>
    <td>$j_name</td>
    </tr>
    <tr>
    <td>Adresse:</td>
    <td>$j_adresse</td>
    </tr>
    <tr>
    <td>PLZ Ort:</td>
    <td>$j_plzOrt</td>
    </tr>
    <tr>
    <td>Telefon:</td>
    <td>$j_tel</td>
    </tr>
    <tr>
    <td>E-Mail:</td>
    <td>$j_mail</td>
    </tr>
EOF;
		}
		$body .= <<<EOF
    <tr>
    <th align="left">Tenue:</th>
    </tr>
    <tr>
    <td>Shirt:</td>
    <td>$data->tenueShirt</td>
    </tr>
    <tr>
    <td>Hose:</td>
    <td>$data->tenueHose</td>
    </tr>
    <tr>
    <th align="left">Ersatz-Tenue:</th>
    </tr>
    <tr>
    <td>Shirt:</td>
    <td>$data->ersatzShirt</td>
    </tr>
    <tr>
    <td>Hose:</td>
    <td>$data->ersatzHose</td>
    </tr>
    <tr>
    <th align="left">Ansprechperson:</th>
    </tr>
    <tr>
    <td>Name:</td>
    <td>$k_name</td>
    </tr>
    <tr>
    <td>Adresse:</td>
    <td>$k_adresse</td>
    </tr>
    <tr>
    <td>PLZ Ort:</td>
    <td>$k_plzOrt</td>
    </tr>
    <tr>
    <td>Telefon:</td>
    <td>$k_tel</td>
    </tr>
    <tr>
    <td>E-Mail:</td>
    <td>$k_mail</td>
    </tr>
    <tr>
    <th align="left">Mannschafts-Verantwortliche(r):</th>
    </tr>
    <tr>
    <td>Name:</td>
    <td>$m_name</td>
    </tr>
    <tr>
    <td>Adresse:</td>
    <td>$m_adresse</td>
    </tr>
    <tr>
    <td>PLZ Ort:</td>
    <td>$m_plzOrt</td>
    </tr>
    <tr>
    <td>Telefon:</td>
    <td>$m_tel</td>
    </tr>
    <tr>
    <td>E-Mail:</td>
    <td>$m_mail</td>
    </tr>
    <tr>
    <th align="left">Kassier(in):</th>
    </tr>
    <tr>
    <td>Name:</td>
    <td>$ka_name</td>
    </tr>
    <tr>
    <td>Adresse:</td>
    <td>$ka_adresse</td>
    </tr>
    <tr>
    <td>PLZ Ort:</td>
    <td>$ka_plzOrt</td>
    </tr>
    <tr>
    <td>Telefon:</td>
    <td>$ka_tel</td>
    </tr>
    <tr>
    <td>E-Mail:</td>
    <td>$ka_mail</td>
    </tr>
    <tr>
    <th align="left">Schiedsrichter:</th>
    </tr>
    <tr>
    <td>Vorname:</td>
    <td>$s_firstname</td>
    </tr>
    <tr>
    <td>Nachname:</td>
    <td>$s_lastname</td>
    </tr>
    <tr>
    <td>E-Mail:</td>
    <td>$s_mail</td>
    </tr>
    <tr align="left">
    <th>Bankverbindung des Vereins:</th>
    </tr>
    <tr>
    <td>Bankverbindung:</td>
    <td>$bank</td>
    </tr>
    <tr>
    <td colspan="2">Spieler/innen dieser Mannschaft spielen ebenfalls in der folgenden Mannschaft:</td>
    </tr>
    <tr>
    <td>$konflikt</td>
    </tr>
    <tr>
    		<td>$bemerkungen</td>
    </tr>
    </table>
  </body>
  <p>Vielen Dank und bis bald.</br>
  </br>
  IVK</p>
</html>
EOF;
		mail($this->mailReceiver, $subject, $body, $this->getMailHeaders());
	}

	private function getMailHeaders() {
		return 'FROM: ' . $this->adm->anmeldungFROM . "\r\n" .
				'CC: ' . $this->adm->anmeldungCC . "\r\n" .
				'X-Mailer: PHP/' . phpversion() . "\r\n" .
				"Content-type: text/html; charset=UTF-8\r\n";
	}

	public function getMailReceiver() {
		return $this->mailReceiver;
	}
}

class AnmeldungData {
	public $verein;
	public $verband;
	public $mNummer;
	public $kategorie;
	public $alter;
	public $gebAlt;
	public $gebJung;
	public $liga;
	public $grund;
	public $tenueShirt;
	public $tenueHose;
	public $ersatzShirt;
	public $ersatzHose;
	/**
	 * @var Person
	 */
	public $korrespondenz;
	/**
	 * @var Person
	 */
	public $jugend;
	/**
	 * @var Referee
	 */
	public $schiedsrichter;
	/**
	 * @var Person
	 */
	public $kassier;
	/**
	 * @var Person
	 */
	public $mannschaft;
	public $bank;
	public $konflikt;
	public $bemerkungen;

	function __construct($json) {
		$this->verein = $json->verein;
		$this->verband = $json->verband;
		if (isset($json->teamNbr)) {
			$this->mNummer = $json->teamNbr;
		} else {
			$this->mNummer = null;
		}
		if (isset($json->liga)) {
			$this->liga = $json->liga;
		} else {
			$this->liga=null;
		}
		$this->alter = $json->age;
		if (isset($json->birthOld)) {
			$this->gebAlt = $json->birthOld;
		} else {
			$this->gebAlt= null;
		}
		if (isset($json->birthYoung)) {
			$this->gebJung = $json->birthYoung;
		} else {
			$this->gebJung = null;
		}
		$this->kategorie = $json->category;
		$this->tenueShirt = $json->shirt;
		$this->tenueHose = $json->hose;
		$this->ersatzShirt = $json->ersatzshirt;
		$this->ersatzHose = $json->ersatzhose;
		$this->korrespondenz = new Person($json->vereinAddress);
		$this->mannschaft = new Person($json->teamAddress);
		$this->schiedsrichter = new Referee($json->refereeAddress);
		$this->kassier = new Person($json->kassierAddress);
		$this->jugend = new Person($json->jugendAddress);
		if (isset($json->conflict)) {
			$this->konflikt = $json->conflict;
		} else {
			$this->konflikt = '';
		}
		if (isset($json->bank)) {
			$this->bank = $json->bank;
		} else {
			$this->bank='';
		}
		if (isset($json->bemerkungen)) {
			$this->bemerkungen = $json->bemerkungen;
		} else {
			$this->bemerkungen = '';
		}
	}

	function setUninitializedFields() {
		if (!isset($this->grund)) {
			$this->grund = '';
		}
		if (!isset($this->bank)) {
			$this->bank = '';
		}
		$this->korrespondenz->setUninitializedFields();
		$this->jugend->setUninitializedFields();
		$this->schiedsrichter->setUninitializedFields();
	}
}

class Person {

	function __construct($json) {
		if (isset($json->name)) {
			$this->name = $json->name;
		} else {
			$this->name = '';
		}
		if (isset($json->address)) {
			$this->adresse = $json->address;
		} else {
			$this->address = '';
		}
		if (isset($json->ZIPCity)) {
			$this->plzOrt = $json->ZIPCity;
		} else {
			$this->plzOrt='';
		}
		if (isset($json->tel)) {
			$this->tel = $json->tel;
		} else {
			$this->tel = '';
		}
		if (isset($json->mail)) {
			$this->mail = $json->mail;
		} else {
			$this->mail='';
		}
	}

	public $name;
	public $adresse;
	public $plzOrt;
	public $tel;
	public $mail;

	function setUninitializedFields() {
		if (!isset($this->name)) {
			$this->name='';
		}
		if (!isset($this->adresse)) {
			$this->adresse='';
		}
		if (!isset($this->plzOrt)) {
			$this->plzOrt='';
		}
		if (!isset($this->tel)) {
			$this->tel='';
		}
		if (!isset($this->mail)) {
			$this->mail='';
		}
	}
}

class Referee {

	function __construct($json) {
		if (isset($json->firstname)) {
			$this->firstname = $json->firstname;
		} else {
			$this->firstname = '';
		}
		if (isset($json->lastname)) {
			$this->lastname = $json->lastname;
		} else {
			$this->lastname = '';
		}
		if (isset($json->mail)) {
			$this->mail = $json->mail;
		} else {
			$this->mail='';
		}
	}

	public $firstname;
	public $lastname;
	public $mail;

	function setUninitializedFields() {
		if (!isset($this->firstname)) {
			$this->firstname='';
		}
		if (!isset($this->lastname)) {
			$this->lastname='';
		}
		if (!isset($this->mail)) {
			$this->mail='';
		}
	}
}
?>
