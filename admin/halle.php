<?php

class c_halle extends BaseCRUD {

    //Constructor
    function __construct(IVKAdmin $adm) {
    	parent::__construct($adm);
      $this->title = "Halle";

      $this->sqlSel['admin'] = "SELECT ID, halle FROM halle";
      $this->fldSel['admin'] = array("halle");
      $this->descSel['admin'] = array("Halle");

      $this->sqlUpd['edit'] = "SELECT ID, halle FROM halle";
      $this->fldUpd['edit'] = array("halle");
      $this->descUpd['edit'] = array("Halle");
      $this->updUpd['edit'] = "UPDATE halle SET halle=:halle";

      $this->sqlSel['pub'] = "SELECT ID, halle FROM halle";
      $this->fldSel['pub'] = array("halle");
      $this->descSel['pub'] = array("Halle");

      $this->sqlIns["new"] = "INSERT INTO halle SET halle=:halle";
      $this->fldIns["new"] = array("halle");
      $this->descIns["new"] = array("Halle");

      $this->key = "ID";
      $this->required = array("halle");
      
      $this->fieldtype = array(
      		"halle"=>"text"
      );
    }

    //Links einf�gen
    function row_finalize($row, $context) {
      if ($context=="admin") {
        $row['halle'] = "<a class='btn btn-default btn-xs' href='index.php?action=print_delete_halle&halle=".$row['ID']."'><img src='images/delete.gif' border=0 alt='l&ouml;schen'></a> " . $row['halle'];
        $row['halle'] = "<a class='btn btn-default btn-xs' href='index.php?action=print_edit_halle&halle=".$row['ID']."'><img src='images/edit.gif' border=0 alt='bearbeiten'></a> " . $row['halle'];
        return $row;
      } else {
        return $row;
      }
    }

    //Halle l�schen fragen
    function askDelete($halle) {
    	$stmt = $this->prepareStatement("SELECT * FROM halle WHERE ID=:halle");
    	$this->executeStatement($stmt, array(":halle" => $halle));
    	$row = $stmt->fetch();
      $str  = "<p>Wollen Sie die Halle <b>" . $row['halle'] . "</b> wirklich l&ouml;schen?</p>";
      $str .= "<a class='btn btn-primary' href=\"index.php?action=delete_halle&halle= " . $row['ID'] . "\">Halle löschen</a> <a class='btn btn-default' href=\"index.php?action=print_halle\">Nein, zur&uuml;ck</a><br>\n";
      return $str;
    }

    //Halle l�schen
    function Delete($halle) {
    	$stmt = $this->prepareStatement("DELETE FROM halle WHERE ID=:halle");
    	$this->executeStatement($stmt, array(':halle' => $halle));
    	return '<div class="alert alert-success" role="alert">Halle wurde gel&ouml;scht.</div>';
    }

  } //End Class

?>
