<?php

class c_teamgruppe
{

  private $adm;

  function c_teamgruppe(IVKAdmin $adm)
  {
    $this->adm = $adm;
  }

  function editForm(IVKAdmin $admin, $target, $gruppeID)
  {
    $str = '';
    $in = array();
    $out = array();
    $gruppe = $admin->pdodb->prepare("SELECT * FROM gruppe WHERE ID=:gruppeID");
    if (!$gruppe->execute(array(':gruppeID' => $gruppeID))) {
      return $gruppe->errorInfo();
    }
    $rowgruppe = $gruppe->fetch();

    $liga = $admin->pdodb->prepare("SELECT * FROM liga WHERE ID=:ligaID");
    if (!$liga->execute(array(':ligaID' => $rowgruppe['ligaID']))) {
      return $liga->errorInfo();
    }
    $rowliga = $liga->fetch();

    $teamliga = $admin->pdodb->prepare("SELECT m.* FROM mannschaft m LEFT JOIN teamgruppe t ON m.ID = t.teamID WHERE ligaID=:ligaID ORDER BY t.ID");
    if (!$teamliga->execute(array(':ligaID' => $rowgruppe['ligaID']))) {
      return $teamliga->errorInfo();
    }

    $str .= "<h3>Mannschaften der Gruppe <Strong>$rowgruppe[gruppe]</strong> aus der Liga <strong>$rowliga[liga]</strong> bearbeiten</h3>\n";

    switch ($rowgruppe['typ']) {

      case "liga":

        //Vorgehen bei normaler Gruppe
        //Gebe Liste aus mit Teams, die in und ausserhalb der Gruppe sind.

        //JavaScript f�r Submit:
        $str .= "<SCRIPT TYPE=\"text/javascript\">\n";
        $str .= "<!-- comment to end of line\n";
        $str .= "function fucputIn() {\n";
        $str .= "  document.teamgruppe.action = document.teamgruppe.action + \"&direction=in\";\n";
        $str .= "  document.teamgruppe.submit();\n";
        $str .= "}\n";
        $str .= "function fucputOut() {\n";
        $str .= "  document.teamgruppe.action = document.teamgruppe.action + \"&direction=out\";\n";
        $str .= "  document.teamgruppe.submit();\n";
        $str .= "}\n";
        $str .= "// comment to end of line -->\n";
        $str .= "</SCRIPT>\n";

        //ganze Liga durchlaufen und in das richtige Array einteilen
        while ($rowteamliga = $teamliga->fetch()) {
          $teamGruppeStmt = $this->adm->prepareStatement('SELECT * FROM teamgruppe WHERE gruppeID=:gruppeID AND teamID=:teamID;');
          $teamGruppeStmt->execute(array(':gruppeID' => $gruppeID, ':teamID' => $rowteamliga['ID']));
          if ($teamGruppeStmt->rowCount() > 0) {
            $in[$rowteamliga['ID']] = $rowteamliga['mannschaft'];
          } else {
            $out[$rowteamliga['ID']] = $rowteamliga['mannschaft'];
          }
        }

        //Formular-Beginn
        $str .= "<form action=\"$target\" name=\"teamgruppe\" method=\"POST\">\n";
        $str .= "<div class=\"table\" style=\"width: auto;\">\n";
        $str .= "<div class=\"tr\">\n";
        $str .= "<div class=\"td\">\n";

        //Liste mit Teams in der Gruppe anzeigen
        if (count($in) > 0) {
          $str .= "<select name=\"in[]\" size=10 multiple>\n";
          foreach ($in as $key => $value) {
            $str .= "<option value=$key>$value</option>\n";
          }
          $str .= "</select>\n";
        } else {
          $str .= "Keine Mannschaft in dieser Gruppe\n";
        }

        $str .= "</div><div class=\"td\">\n";

        //Buttons anzeigen
        $str .= "<input type=\"button\" name=\"putIn\" value=\"<<\" onclick=\"fucputIn()\">\n";
        $str .= "<br><br>\n";
        $str .= "<input type=\"button\" name=\"putOut\" value=\">>\" onclick=\"fucputOut()\">\n";

        $str .= "</div><div class=\"td\">\n";

        //Liste mit Teams ausserhalb der Gruppe anzeigen
        if (count($out) > 0) {
          $str .= "<select name=\"out[]\" size=10 multiple>\n";
          foreach ($out as $key => $value) {
            $str .= "<option value=$key>$value</option>\n";
          }
          $str .= "</select>\n";
        } else {
          $str .= "Keine Mannschaft ausserhalb Gruppe\n";
        }


        $str .= "</div></div></div>\n";
        $str .= "<input type=\"hidden\" name=\"gruppe\" value=\"$gruppeID\">\n";
        $str .= "</form>\n";

        break;

      case "final":
      case "cup":

        //Vorgehen bei Final-Gruppe
        //Gebe die Bezeichnung der Gruppenmitglieder aus und deren Bedeutung

        $tab = new c_tabelle($this->adm);

        //Formular- und Tabellen-Beginn
        $str .= "<form action=\"$target\" name=\"teamgruppe\" method=\"POST\">\n";
        $str .= '<table class="table table-condensed">';
        $str .= "<tr><th>Gruppen-Mitglieder</th><th>Löschen</th><th>Ausgeschrieben</th><th>Momentanes Team</th></tr>\n";

        //Bestehenden Zeilen ausgeben
        $stmt = $this->adm->prepareStatement("SELECT * FROM teamgruppe WHERE gruppeID=:gruppeID;");
        $stmt->execute(array(':gruppeID' => $gruppeID));
        while ($row = $stmt->fetch()) {
          $str .= "<tr><td><input class=\"form-control\" type=\"text\" name=\"data[$row[ID]]\" value=\"$row[teamCode]\"></td>\n";
          $str .= "<td><input type=\"checkbox\" name=\"delete[$row[ID]]\"></td>\n";
          $arrTeamCode = $tab->arrTeamCode($row['teamCode']);
          $str .= "<td>" . $arrTeamCode['txt'] . "</td>\n";
          $str .= "<td></td>\n";
          $str .= "</tr>\n";
        }

        $str .= "<tr><td><input class=\"form-control\" type=\"text\" name=\"data[new]\" value=\"$row[teamCode]\"><td>(neu)</td><td></td><td></td></td></tr>\n";

        //Formular und Tabellen Ende
        $str .= "</table>\n";
        $str .= '<button type="submit" class="btn btn-primary">Senden</button>';
        $str .= "<input type=\"hidden\" name=\"gruppe\" value=\"$gruppeID\">\n";
        $str .= "</form>\n";

        $str .= "<p>
      <h3>Anleitung</h3><br>
      So werden die Mitglieder eingegeben:
      <ol>
        <li>Rang</li>
        <li><i>G</i> für Gruppe, <i>R</i> für Runde oder <i>S</i> für Spiel</li>
        <li>ID der Gruppe bzw. Runde / Spiel</li>
      </ol>
      Alles wird mit einem Punkt getrennt eingegeben. Z.B. <b>1.G.5</b> (erster der Gruppe 5) oder
      <b>5.R.22</b> (fünfter der Runde 22) oder <b>1.S.42</b> (Sieger Spiel 42).
      </p>\n";
    }

    return $str;
  }

  function parseEditForm($direction, $gruppeID, $in, $out, $data, $delete)
  {
    $gruppeStmt = $this->adm->prepareStatement('SELECT * FROM gruppe WHERE ID = :gruppeID');
    $gruppeStmt->execute(array(':gruppeID' => $gruppeID));
    $rowgruppe = $gruppeStmt->fetch();
    $gruppeStmt->closeCursor();

    //Je nach Gruppenart was anderes machen

    switch ($rowgruppe['typ']) {

      case "liga":
        //Normale Gruppe
        if ($direction == "in" and is_array($out)) {
          foreach ($out as $key => $value) {
            $insertStmt = $this->adm->prepareStatement("INSERT INTO teamgruppe SET gruppeID=:gruppeID, teamID=:teamID;");
            $insertStmt->execute(array(':gruppeID' => $gruppeID, ':teamID' => $value));
            $insertStmt->closeCursor();
          }
        } elseif ($direction == "out" and is_array($in)) {
          foreach ($in as $key => $value) {
            $deleteStmt = $this->adm->prepareStatement("DELETE FROM teamgruppe WHERE gruppeID=:gruppeID AND teamID=:teamID;");
            $deleteStmt->execute(array(':gruppeID' => $gruppeID, ':teamID' => $value));
            $deleteStmt->closeCursor();
          }
        }
        break;

      //Final Gruppe
      case "final":
      case "cup":
        if (is_array($delete)) {
          foreach ($delete as $key => $value) {
            if ($value == true) {
              $stmt = $this->adm->prepareStatement("DELETE FROM teamgruppe WHERE ID=:ID;");
              $stmt->execute(array(':ID' => $key));
            }
          }
        }

        if (is_array($data)) {
          foreach ($data as $key => $value) {
            if ($key != "new") {
              $stmt = $this->adm->prepareStatement("UPDATE teamgruppe SET teamCode=:value WHERE ID=:key");
              $stmt->execute(array(':value' => $value, ':key' => $key));
            }
          }

          if ($data["new"]) {
            $stmt = $this->adm->prepareStatement("INSERT INTO teamgruppe SET gruppeID=:gruppeID, teamCode=:teamCode;");
            $stmt->execute(array(':gruppeID' => $gruppeID, ':teamCode' => $data['new']));
          }
        }
        break;
    }
  }

} //End Class

?>
