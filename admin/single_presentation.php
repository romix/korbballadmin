<?php
header('Content-Type:text/html; charset=UTF-8');
/**********************************
 Bibliotheken laden und  prüfen
***********************************/
include "lib.php";
include "presentation.php";
include "resultate.php";

session_start();

// datum einstellungen
setlocale(LC_ALL, "de_DE");
putenv( "PHP_TZ=Europe/Berlin" );

//Admin-Objekt erstellen
$adm = new IVKAdmin();

// Präsentation erstellen
$p = new Presentation($adm, get('day',$_GET));

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Turnverband Luzern, Ob- und Nidwalden</title>
<meta name="generator" content="Bluefish 1.0.7">
<meta http-equiv="Refresh" content="<?php echo $p->getSeconds() ?>">
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="css/presentation.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<meta name="description" content="Turnverband Luzern, Ob- und Nidwalden">
<meta name="author" content="Roman Schaller">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
</head>

<body style="zoom: 1;" class="font-size-small">
	<div class="container">
		<nav class="navbar navbar-default" role="navigation">
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<?php
			$p->showSlides();
			?>
		</div>
		</nav>

		<div class="row">
			<?php
			$p->present();
			?>
      <div>
        <h4>
          Hole dir die Resultate mit der App "Korbball-Meisterschaft" auf dein Handy.
        </h4>   
        <div>
          <img class="presentation-qr-code" src="templates/presentation/img/qr-code-play-store.png" alt="QR Code Play Store">
          <img class="presentation-qr-code" src="templates/presentation/img/qr-code-app-store.png" alt="QR Code App Store">
        </div>
      </div>
		</div>

	</div>
</body>
</html>
