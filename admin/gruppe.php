<?php

class Gruppe extends BaseCRUD {

    //Constructor
    function __construct(IVKAdmin $adm) {
    	parent::__construct($adm);
      $this->title = "Gruppen";

      $this->sqlSel['admin'] = "SELECT gruppe.ID, gruppe, liga, typ FROM gruppe LEFT JOIN liga ON gruppe.ligaID=liga.ID";
      $this->fldSel['admin'] = array("gruppe", "liga", "typ");
      $this->descSel['admin'] = array("Gruppe", "Liga", "Typ");

      $this->sqlSel['pub'] = "SELECT gruppe.ID, gruppe, liga, typ FROM gruppe LEFT JOIN liga ON gruppe.ligaID=liga.ID";
      $this->fldSel['pub'] = array("gruppe", "liga", "typ");
      $this->descSel['pub'] = array("Gruppe", "Liga", "Typ");

      $this->sqlUpd['edit'] = "SELECT gruppe.ID, gruppe, ligaID, typ FROM gruppe";
      $this->fldUpd['edit'] = array("gruppe", "ligaID", "typ");
      $this->descUpd['edit'] = array("Gruppe", "Liga", "Typ");
      $this->updUpd['edit'] = "UPDATE gruppe SET gruppe=:gruppe, ligaID=:ligaID, typ=:typ";

      $this->sqlIns['new'] = "INSERT INTO gruppe SET ligaID=:ligaID, typ=:typ, gruppe=:gruppe;";
      $this->fldIns['new'] = array("ligaID", "gruppe", "typ");
      $this->descIns['new'] = array("Liga", "Gruppe", "Typ");

      $this->key = "gruppe.ID";
      $this->required = array("ligaID", "gruppe");

      $this->fieldtype = array(
        "ligaID"=>"combo",
        "typ"=>"combo"
      );

      //Ligen einf�llen
      $stmt = $this->prepareStatement("SELECT * FROM liga;");
      $this->executeStatement($stmt);
      while($row = $stmt->fetch()) {
        $this->fieldinfo['ligaID']['datasource'][$row['ID']] = $row['liga'];
      }

      //final-Combobox f�llen
      $this->fieldinfo['typ']['datasource']['liga'] = "normale Gruppe";
      $this->fieldinfo['typ']['datasource']['final'] = "Final-Gruppe";
      $this->fieldinfo['typ']['datasource']['cup'] = "Cup";

    } //End Constructor

    //Links einf�gen
    function row_finalize($row, $context) {
      if ($context=="admin") {
        $row['gruppe'] = "<a class='btn btn-default btn-xs' href='index.php?action=print_delete_gruppe&gruppe=$row[ID]'><img src='images/delete.gif' border=0 title='Gruppe löschen'></a> " . $row['gruppe'];
        $row['gruppe'] = "<a class='btn btn-default btn-xs' href='index.php?action=print_extrapoints&gruppe=$row[ID]'><img src='images/plus_minus.png' border=0 title='Punktstrafen oder -gutschriften'></a> " . $row['gruppe'];
        $row['gruppe'] = "<a class='btn btn-default btn-xs' href='index.php?action=print_edit_gruppe&gruppe=$row[ID]'><img src='images/edit.gif' border=0 title='bearbeiten'></a> " . $row['gruppe'];
        $row['gruppe'] = "<a class='btn btn-default btn-xs' href='index.php?action=print_edit_teamgruppe&gruppe=$row[ID]'><img src='images/spieler.gif' border=0 title='Mannschaften bearbeiten'></a> " . $row['gruppe'];
      }      

      switch ($row['typ']) {
      case "liga":
        $row['typ']="";
        break;
      case "final":
        $row['typ']="Final-Runde";
        break;
        case "cup":
          $row['typ']="Cup";
      }

      return $row;
    }

    //Gruppe l�schen fragen
    function askDelete($gruppe) {
    	$stmt = $this->prepareStatement("SELECT * FROM gruppe LEFT JOIN liga ON gruppe.ligaID=liga.ID WHERE gruppe.ID=:gruppe");
    	$this->executeStatement($stmt, array(":gruppe" => $gruppe));
      $row = $stmt->fetch();
      $str  = "<p>Wollen Sie die Gruppe <b>$row[gruppe]</b> aus der Liga <b>$row[liga]</b> wirklich l&ouml;schen?<br>\n";
      $str .= "<a class='btn btn-primary' href=\"index.php?action=delete_gruppe&gruppe=$gruppe\">Gruppe löschen</a> <a class='btn btn-default' href=\"javascript:history.back();\">Nein</a><br>\n";
      $str .= "</p>";
      return $str;
    }

    //Gruppe l�schen
    function delete($gruppe) {
    	$stmt = $this->prepareStatement("DELETE FROM gruppe WHERE ID=:gruppe");
    	$this->executeStatement($stmt, array(':gruppe' => $gruppe));
      return '<div class="alert alert-success" role="alert">Gruppe wurde gelöscht.</div>';
    }
}
?>
