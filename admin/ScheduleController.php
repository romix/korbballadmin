<?php

namespace admin;

use admin\traits\ColorTrait;
use c_spiel;
use IVKAdmin;
use PDO;

include 'traits/ColorTrait.php';

class ScheduleController
{
  protected IVKAdmin $adm;
  protected $pdo;

  use ColorTrait;

  public function __construct(IVKAdmin $adm)
  {
    $this->adm = $adm;
    $this->pdo = $adm->getConnection();
    $this->gap = $adm->gameDuration;
  }

  /**
   * Index View anzeigen
   */
  public function index()
  {
    echo $this->adm->templates->render('schedule/index', [
      'days' => $this->addSessionScheduleData($this->getDays()),
      'groups' => $this->getGroups(),
      'templates' => $this->getTemplates(),
      'courts' => $this->getCourts(),
      'clipboard' => $this->clipboard()
    ]);
  }

  protected function getDays(): array
  {
    $dates_stmt = $this->pdo->query('SELECT DISTINCT tag FROM spiel WHERE tag IS NOT NULL ORDER BY tag ASC');
    $dates = $dates_stmt->fetchAll();
    $days = [];

    foreach ($dates as $date) {
      $fields_stmt = $this->pdo->prepare('SELECT DISTINCT spiel.halleID as ID, halle.halle as name FROM spiel INNER JOIN halle on spiel.halleID = halle.ID WHERE spiel.tag LIKE :tag');
      $fields_stmt->execute(['tag' => $date[0]]);
      $fields = $fields_stmt->fetchAll(PDO::FETCH_ASSOC);

      $first_game_of_day_stmt = $this->pdo->prepare('SELECT MIN(spiel.zeit) FROM spiel WHERE spiel.tag LIKE :tag group by spiel.tag');
      $first_game_of_day_stmt->execute(['tag' => $date[0]]);
      $first_game_of_day = $first_game_of_day_stmt->fetch(PDO::FETCH_NUM)[0];

      $fields_array = [];

      foreach ($fields as $field) {
        $slots_stmt = $this->pdo->prepare('
          SELECT spiel.ID, spiel.zeit, gruppe.gruppe, mannschaftA.mannschaft as teamA, mannschaftB.mannschaft as teamB, spiel.tag 
          FROM spiel
          JOIN gruppe ON gruppe.ID = spiel.gruppeID
          JOIN mannschaft AS mannschaftA ON mannschaftA.ID = spiel.teamA
          JOIN mannschaft AS mannschaftB ON mannschaftB.ID = spiel.teamB
          WHERE spiel.tag LIKE :tag AND spiel.halleID LIKE :field ORDER BY zeit ASC
        ');
        $slots_stmt->execute([
          'tag' => $date[0],
          'field' => $field['ID']
        ]);
        $slots = $slots_stmt->fetchAll(PDO::FETCH_ASSOC);

        $slots_array = [];

        if (sizeof($slots) > 0) {
          // make sure all fields have same starting time - one gap
          $start_gap = abs($this->timeToDec($slots[0]['zeit']) - $this->timeToDec($first_game_of_day));
          array_push($slots_array, [
            'time' => date('H:i', strtotime($first_game_of_day . ' - ' . $this->gap . ' minutes'))
          ]);
          $correction = 0;
          while ($start_gap >= $this->gap / 60) {
            array_push($slots_array, [
              'time' => date('H:i', strtotime($first_game_of_day . ' + ' . $correction . ' minutes'))
            ]);
            $start_gap = $start_gap - $this->gap / 60;
            $correction = $correction + $this->gap;
          }

          foreach ($slots as $key => $slot) {
            $gap = abs($this->timeToDec($slots[max($key - 1, 0)]['zeit']) - $this->timeToDec($slot['zeit']));
            $correction = 0;
            while ($gap > $this->gap / 60) {
              $correction = $correction + $this->gap;
              array_push($slots_array, ['time' => date('H:i', strtotime($slots[$key - 1]['zeit'] . ' + ' . $correction . ' minutes'))]);
              $gap = $gap - $this->gap / 60;
            }

            $bgColor = $this->stringToColorCode($slot['gruppe']);
            $textColor = $this->contrastColor($bgColor);
            array_push($slots_array, [
              'time' => date('H:i', strtotime($slot['zeit'])),
              'game' => [
                'id' => $slot['ID'],
                'opponents' => $slot['teamA'] . ' - ' . $slot['teamB'],
                'league' => $slot['gruppe'],
                'color' => $bgColor,
                'textColor' => $textColor
              ]
            ]);
          }
          // add additional time at end
          array_push($slots_array, ['time' => date('H:i', strtotime($slot['zeit'] . ' + ' . $this->gap . ' minutes'))]);

          $fields_array[$field['ID']] = [
            'id' => $field['ID'],
            'name' => $field['name'],
            'slots' => $slots_array,
          ];
        }
      }

      setlocale(LC_TIME, "de_CH");

      array_push($days, [
        'date' => date('d.m.Y', strtotime($date[0])),
        'weekday' => strftime('%A', strtotime($date[0])),
        'fields' => $fields_array,
      ]);
    }

    return $days;
  }

  protected function addSessionScheduleData($days): array
  {
    $schedule = $_SESSION['schedule'];
    if (!isset($schedule)) {
      return $days;
    }
    $scheduleDates = $schedule['date'];
    $scheduleCourts = $schedule['court'];
    $scheduleStartTimes = $schedule['start'];

    if (isset($scheduleDates)) {
      $this->ensureDates($days, $scheduleDates);

      foreach ($scheduleDates as $scheduleDateKey => $scheduleDate) {
        if (is_string($scheduleDate) && strlen($scheduleDate) > 0) {
          $dayKey = $this->findDayKey($days, $scheduleDate);
          $day = &$days[$dayKey];
          $scheduleCourtIdsOfDay = $scheduleCourts[$scheduleDateKey];
          $this->ensureFields($day, $scheduleCourtIdsOfDay);
          $scheduleStartTime = $scheduleStartTimes[$scheduleDateKey];
          $this->ensureTime($day, $scheduleStartTime);

          foreach ($scheduleCourtIdsOfDay as $scheduleCourtId) {
            $courtKey = $this->findCourtKey($day, $scheduleCourtId);
            $court = &$day['fields'][$courtKey];
            $court['slots'][0]['templateGame']['opponents'] = 'Template Game';
          }

        }
      }
    }
    return $days;
  }

  protected function ensureDates(&$days, $scheduleDates)
  {
    foreach ($scheduleDates as $scheduleDate) {
      $formattedDate = date('d.m.Y', strtotime($scheduleDate));
      $found = false;
      foreach ($days as $day) {
        if ($day['date'] == $formattedDate) {
          $found = true;
          break;
        }
      }
      if (!$found) {
        $emptyDay = ['date' => $formattedDate];
        array_push($days, $emptyDay);
      }
    }
  }

  protected function ensureFields(&$day, $fieldIds)
  {
    foreach ($fieldIds as $fieldId) {
      $alreadyExists = false;
      foreach ($day['fields'] as $fieldKey => $fieldValue) {
        if ($fieldId == $fieldValue['id']) {
          $alreadyExists = true;
          break;
        }
      }
      if (!$alreadyExists) {
        $fields_stmt = $this->pdo->prepare('SELECT DISTINCT halle.ID, halle.halle as name FROM halle WHERE halle.ID = :halleId');
        $fields_stmt->execute(['halleId' => $fieldId]);
        $field = $fields_stmt->fetch(PDO::FETCH_ASSOC);
        $emptyField = ['id' => $field['ID'], 'name' => $field['name'], 'slots' => []];
        $day['fields'][] = $emptyField;
      }
    }
  }

  protected function findCourtKey(&$day, $scheduleCourtId)
  {
    foreach ($day['fields'] as $courtKey => $court) {
      if ($court['id'] == $scheduleCourtId) {
        return $courtKey;
      }
    }
  }

  protected function findDayKey($days, $date)
  {
    $formattedDate = date('d.m.Y', strtotime($date));
    foreach ($days as $key => $day) {
      error_log('day: ' . $day['date']);
      if ($day['date'] == $formattedDate) {
        return $key;
      }
    }
  }

  protected function ensureTime(&$day, $time)
  {
    foreach ($day['fields'] as $fieldKey => $field) {
      $emptyGame = [
        'time' => $time,
        'game' => []];
      array_unshift($day['fields'][$fieldKey]['slots'], $emptyGame);
    }

  }

  /**
   * Zwischenablage
   * @return array
   */
  protected function clipboard(): array
  {
    $clipboard_stmt = $this->pdo->query('
      SELECT spiel.ID, spiel.zeit, gruppe.gruppe, mannschaftA.mannschaft as teamA, mannschaftB.mannschaft as teamB, spiel.tag 
      FROM spiel
      JOIN gruppe ON gruppe.ID = spiel.gruppeID
      JOIN mannschaft AS mannschaftA ON mannschaftA.ID = spiel.teamA
      JOIN mannschaft AS mannschaftB ON mannschaftB.ID = spiel.teamB
      WHERE spiel.tag IS NULL ORDER BY gruppe.gruppe, mannschaftA.mannschaft ASC
    ');
    $clipboardGames = $clipboard_stmt->fetchAll();
    $clipboard = [];
    foreach ($clipboardGames as $game) {
      $bgColor = $this->stringToColorCode($game['gruppe']);
      $textColor = $this->contrastColor($bgColor);
      array_push($clipboard, [
        'time' => date('H:i', strtotime($game['zeit'])),
        'id' => $game['ID'],
        'opponents' => $game['teamA'] . ' - ' . $game['teamB'],
        'league' => $game['gruppe'],
        'color' => $bgColor,
        'textColor' => $textColor
      ]);
    }

    return $clipboard;
  }

  function getLeagues()
  {
    $groups_stmt = $this->pdo->prepare('
          SELECT liga.ID, liga.liga as name, COUNT(mannschaft.mannschaft) AS teams
          FROM liga
          INNER JOIN mannschaft ON mannschaft.ligaID LIKE liga.ID
          GROUP BY liga.ID
        ');
    $groups_stmt->execute();
    $groups = $groups_stmt->fetchAll(PDO::FETCH_ASSOC);
    return $groups ?? [];
  }

  function getGroups()
  {
    $groups_stmt = $this->pdo->prepare('
      SELECT gruppe.ID, gruppe.gruppe as name, COUNT(mannschaft.mannschaft) AS teams
      FROM gruppe
      INNER JOIN teamgruppe ON teamgruppe.gruppeID = gruppe.ID
      INNER JOIN mannschaft ON mannschaft.ID LIKE teamgruppe.teamID
      GROUP BY gruppe.ID, name
    ');
    $groups_stmt->execute();
    $groups = $groups_stmt->fetchAll(PDO::FETCH_ASSOC);
    return $groups ?? [];
  }

  function getTemplates()
  {
    if (isset($_SESSION['schedule']['group']['ID'])) {
      $teams_stmt = $this->pdo->prepare('
        SELECT COUNT(mannschaft.mannschaft) AS teams
        FROM gruppe
        INNER JOIN teamgruppe ON teamgruppe.gruppeID = gruppe.ID
        INNER JOIN mannschaft ON mannschaft.ID LIKE teamgruppe.teamID
        WHERE gruppe.ID LIKE :id
        GROUP BY gruppe.ID
      ');
      $teams_stmt->execute([
        'id' => $_SESSION['schedule']['group']['ID']
      ]);
      $teams = $teams_stmt->fetchColumn();
    }

    $templates_stmt = $this->pdo->prepare('
          SELECT ID, template as name, teams
          FROM template
          WHERE teams LIKE :teams
        ');
    $templates_stmt->execute([
      'teams' => $teams ?? 999
    ]);
    $templates = $templates_stmt->fetchAll(PDO::FETCH_ASSOC);
    return $templates ?? [];
  }

  public function getCourts()
  {
    $courts_stmt = $this->pdo->prepare('
          SELECT ID, halle as name
          FROM halle
        ');
    $courts_stmt->execute([]);
    $courts = $courts_stmt->fetchAll(PDO::FETCH_ASSOC);
    return $courts ?? [];
  }

  /**
   * Move one game
   */
  public function moveGame($gameId, $targetDate, $targetTime, $targetHalle)
  {
    $spiel = new c_spiel($this->adm);
    $spiel->moveGame($gameId, dat_u2m($targetDate), $targetTime, $targetHalle);
  }

  /**
   * Move one game to clipboard
   */
  public function moveGameToClipboard($gameId)
  {
    $spiel = new c_spiel($this->adm);
    $spiel->moveGame($gameId, null, null, null);
  }

  protected function timeToDec($time)
  {
    $out = explode(':', $time);
    return (float)$out[0] + (float)$out[1] / 60;
  }

  /**
   * Aktualisiert View
   * @param $group_id
   * @param $round
   * @param $template_id
   * @param $date
   * @param $start
   * @param $court
   * @param $move_team
   */
  public function addGames($group_id, $round, $template_id, $date, $start, $court, $move_team)
  {
    if ($_SESSION['schedule']['group']['ID'] !== $group_id) {
      $_SESSION['schedule']['group']['ID'] = $group_id;

      // update teams only if group has changed
      $teams = $this->getTeams($group_id);
      foreach ($teams as $key => $team) {
        $teams[$key]['order'] = $team['ID'];
      }

      $_SESSION['schedule']['group']['teams'] = $teams;
    }

    $this->changeTeamsOrder($move_team);

    $_SESSION['schedule']['round'] = $round;
    $_SESSION['schedule']['template'] = $this->getTemplate($template_id); // so kann man gleich checken wie viele Spieltage es gibt
    $_SESSION['schedule']['date'] = $date;
    $_SESSION['schedule']['start'] = $start;
    $_SESSION['schedule']['court'] = $court;

    // bereit zur Erstellung?
    $this->checkIfReady();

    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_create_schedule");
  }

  /**
   * Execute to check if form is filled out
   * @return int|void
   */
  protected function checkIfReady()
  {
    $_SESSION['schedule']['ready'] = true;
    if ($this->checkIfReadyPart('date') === 0) {
      return 0;
    }
    if ($this->checkIfReadyPart('start') === 0) {
      return 0;
    }
  }

  protected function checkIfReadyPart($part)
  {
    if (count($_SESSION['schedule'][$part]) === 0) {
      $_SESSION['schedule']['ready'] = false;
      return 0;
    }

    foreach ($_SESSION['schedule'][$part] as $field) {
      if (is_null($field) || $field === '') {
        $_SESSION['schedule']['ready'] = false;
        return 0;
      }
    }
  }

  protected function changeTeamsOrder($move_team)
  {
    $this->switchOrder(1, $move_team);
    $this->switchOrder(-1, $move_team);
  }

  protected function switchOrder($by, $move_team)
  {
    $up_index = array_search($by, $move_team);
    if ($up_index && isset($_SESSION['schedule']['group']['teams'])) {
      $key = array_search($up_index, array_column($_SESSION['schedule']['group']['teams'], 'ID'));
      $_SESSION['schedule']['group']['teams'][max($key - 1 * $by, 0)]['order'] -= $by;
      $_SESSION['schedule']['group']['teams'][$key]['order'] += $by;

      $this->sortTeamsSessionByOrder();
    }
  }

  protected function sortTeamsSessionByOrder()
  {
    $order = [];
    foreach ($_SESSION['schedule']['group']['teams'] as $key => $row) {
      $order[$key] = $row['order'];
    }
    array_multisort($order, SORT_DESC, $_SESSION['schedule']['group']['teams']);
  }

  /**
   * Nimmt eine Template ID und gibt das passende Template zurück
   * @param $id
   * @return array|mixed
   */
  public function getTemplate($id)
  {
    $template_stmt = $this->pdo->prepare('
      SELECT ID, template as name, hallen as courts, spieltage, teams, zeilen
      FROM template
      WHERE ID LIKE :id
    ');
    $template_stmt->execute([
      'id' => $id
    ]);
    $template = $template_stmt->fetchAll(PDO::FETCH_ASSOC);
    return $template[0] ?? [];
  }

  public function getTeams($group_id): array
  {
    $sql = "SELECT mannschaft.ID as ID, mannschaft.mannschaft as name 
        FROM gruppe
        INNER JOIN teamgruppe ON teamgruppe.gruppeID = gruppe.ID
        INNER JOIN mannschaft ON mannschaft.ID LIKE teamgruppe.teamID
        WHERE gruppe.ID LIKE :group_id";
    $teams_stmt = $this->pdo->prepare($sql);
    $teams_stmt->execute([
      'group_id' => $group_id
    ]);
    $teams = $teams_stmt->fetchAll(PDO::FETCH_ASSOC);
    return $teams ?? [];
  }

  /**
   * Runde und ihre Spiele in der Datenbank erfassen
   */
  public function insertRoundIntoDB()
  {
    $round_id = $this->createRound($_SESSION['schedule']['round'], $_SESSION['schedule']['group']['ID']);

    // Hallen umwandeln in Format aus c_spiele
    $hallen = [];
    foreach ($_SESSION['schedule']['court'] as $round => $courts) {
      foreach ($courts as $place => $court_id) {
        $hallen[$round . ';' . $place] = $court_id;
      }
    }

    // Array muss bei 1 starten statt bei 0 für Funktion
    // Team ID ausweisen
    $teams = [];
    foreach ($_SESSION['schedule']['group']['teams'] as $key => $team) {
      $teams[$key + 1] = $team['ID'];
    }

    // Datum umwandeln in Y-m-d
    $dates = [];
    foreach ($_SESSION['schedule']['date'] as $key => $date) {
      $dates[$key] = date('Y-m-d', strtotime($date));
    }

    //spiele erstellen
    $spiel = new c_spiel($this->adm);
    $spiel->insertGames(
      $_SESSION['schedule']['template']['ID'],
      $round_id,
      $_SESSION['schedule']['group']['ID'],
      $hallen,
      $teams,
      $dates,
      $_SESSION['schedule']['start'],
      $this->gap
    );

    $this->resetSchedule();
  }

  /**
   * Erstellt Runde und gibt gruppenID zurück
   *
   * @param $name
   * @param $group_id
   * @param string $system
   * @return mixed
   */
  public function createRound($name, $group_id, $system = "all")
  {
    $sql = "INSERT INTO runde (`gruppeID`, `runde`, `system`) VALUES (:group_id, :name, :system);";
    $round_create_stmt = $this->pdo->prepare($sql);
    $round_create_stmt->execute([
      'group_id' => $group_id,
      'name' => $name,
      'system' => $system
    ]);

    $sql = "SELECT ID FROM runde WHERE gruppeID LIKE :group_id AND runde LIKE :name LIMIT 1;";
    $round_return_stmt = $this->pdo->prepare($sql);
    $round_return_stmt->execute([
      'group_id' => $group_id,
      'name' => $name,
    ]);

    return $round_return_stmt->fetch()['ID'];
  }

  /**
   * Reset nach Erstellung von Runden
   */
  function resetSchedule()
  {
    $_SESSION['schedule']['start'] = [];
  }
}