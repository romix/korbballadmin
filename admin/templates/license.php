<h2>Was ist das?</h2>
<p>
KorbballAdmin ist eine Web-Applikation, welche ich für die IVK (Interverbands Kommission) geschrieben habe. 
Mit dieser App können Anmeldungen, Spielpläne und Resultate verwaltet und publiziert werden.
</p>

<h2>Urheber</h2>
<p>
Urherber dieser Web-App sind
<ul>
<li>Roman Schaller, Obereyweg 1a, 6207 Nottwil (roman.schaller@gmail.com)</li>
<li>Samuel Schwegler, Willisauerstrasse 11, 6122 Menznau (samuel.schwegler@gmx.ch)</li>
</ul>
</p>

<h2>Nutzungsrecht</h2>
<p>
Die IVK hat das alleinige Nutzungsrecht der Anwendung. Sie kann dies aber auch an weitere Organisationen über individuelle Abmachungen übertragen. 
Bei der Übertragung des Nutzungsrechtes muss der Urheber einbezogen werden. Das Nutzungsrecht kann nur an Non-Profit-Organisationen übertragen werden.
</p> 