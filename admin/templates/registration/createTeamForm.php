<h2>Team erstellen (<?= $teamDescription?>)</h2>
<form action="index.php?action=add_team_from_registration" method="POST">
	<div class="row">
		<div class="col-md-6">
			<input type="hidden" name="regId" value="<?= $this->e($regId) ?>" />
			<div class="form-group">
				<label for="team">Team</label> <input type="text" class="form-control" id="team" name="team" value="<?= $this->e($teamName) ?>">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="liga">Liga</label> <select class="form-control" id="liga" name="liga" required>
					<option value="">Bitte wählen...</option>
					<?php foreach($leagues as $leagueId => $leagueName): ?>
					<option value="<?=$this->e($leagueId)?>">
						<?=$this->e($leagueName)?>
					</option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="verein">Verein</label> <select class="form-control" id="verein" name="verein" required>
					<option value="">Bitte wählen...</option>
					<?php foreach($vereine as $vereinId => $vereinName): ?>
					<option value="<?=$this->e($vereinId)?>" <?= $vereinId == $guessedVerein ? 'selected': '' ?>>
						<?=$this->e($vereinName)?>
					</option>
					<?php endforeach ?>
				</select>
			</div>
		</div>
	</div>
	<div class="row" style="margin-bottom: 2em; margin-top: 2em">
		<div class="col-md-6">
			<button type="submit" name="new_btn" class="btn btn-primary">Team erstellen</button>
		</div>
	</div>
</form>
