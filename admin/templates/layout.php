<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8">
	<meta name="description" content="Turnverband Luzern, Ob- und Nidwalden">
	<meta name="author" content="Roman Schaller">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="css/content.css" rel="stylesheet">
	<link href="css/dashboard.css" rel="stylesheet">
	<script src="ts/dist/bundle.js"></script>
<title><?= $this->e($adm->tournamentName) ?></title>
</head>
<body>

  <div class="container-fluid">
    <nav class="navbar navbar-inverse navbar-fixed-top" up-fixed="top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
            aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Korbball-App [<?=$this->e($adm->tournamentName) ?>]</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <?=
          $this->e($adm->showLogin())
          ?>
        </div>
      </div>
    </nav>
    <div class="row">

      <!--Navigation-->
      <div class="col-sm-3 col-md-2 sidebar hidden-print">
        <?php
        $adm->navigation();
        ?>
      </div>
      <!--Ende Navigation-->

      <!--Content-->
      <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

      <?= $this->section('content') ?? $content ?? "" ?>

      </div>
      <!--Ende Content-->
    </div>
  </div>
  <script src="js/jquery-3.0.0.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>