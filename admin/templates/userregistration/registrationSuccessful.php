<h2>Hallo <?= $vorname ?> <?= $name ?></h2>

<p>Dein Benutzer wurde angelegt. Du kannst dich nun mit deiner E-Mail-Adresse und deinem Passwort anmelden.</p>

<?php if ($isAdmin): ?>
<p>Da du der erste Benutzer für Korbball-Admin bist, haben wir dich als Administrator berechtigt.</p>
<?php endif?>