<h2>Benutzerregistrierung</h2>

<form action="index.php?action=register2" method="POST">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group">
        <label for="vorname">Vorname</label>
        <input type="vorname" class="form-control" name="vorname" id="vorname" placeholder="Dein Vorname">
      </div>
      <div class="form-group">
        <label for="name">Name</label>
        <input class="form-control" name="name" id="name" placeholder="Dein Name">
      </div>
      <div class="form-group">
        <label for="email">E-Mail Adresse</label>
        <input type="email" class="form-control" name="email" id="email" placeholder="Deine E-Mail-Adresse">
      </div>
      <div class="form-group">
        <label for="password">Passwort</label>
        <input type="password" class="form-control" name="password" id="password" placeholder="Passwort">
      </div>
      <button type="submit" class="btn btn-primary">Registrieren</button>
    </div>
  </div>
</form>