<h1>Startliste</h1>
<p>
Folgende Liste enthält die erste Startzeit pro Tag jeder Mannschaft:
</p>

<table class="table table-striped table-condensed">
	<tbody>
		<tr>
			<th>Tag</th><th>Zeit</th><th>Gruppe</th><th>Mannschaft</th>
		</tr>
		<?php foreach($data as $row): ?>
			<tr>
				<td><?= dat_m2u($row['tag']) ?></td>
				<td><?= $row['ersteZeit'] ?></td>
				<td><?= $row['gruppeName'] ?></td>
				<td><?= $row['teamName'] ?></td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>