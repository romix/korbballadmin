<form action="index.php?action=upload_presentation_responsible_pic" method="post" enctype="multipart/form-data">
	<div class="row">
		<div class="col-md-12">
    	<h3>Bilder</h3>
    </div>
  </div>
  <div class="row">
  	<?php foreach ($pics as $pic):?>
  	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
  		<img height="<?= $pic['height'] ?>" width="<?= $pic['width'] ?>" src="<?= $pic['fileUrl'] ?>"><br><br>
  		<a class="btn btn-primary" href="?action=delete_presentation_responsible_pic&pic=<?= $pic['fileName']?>">Bild löschen</a>
  	</div>
  	<?php endforeach; ?>
  </div>
  <div class="row">
    <div class="col-md-6">
    	<h3>Neues Bild hochladen</h3>
    	<div class="form-group">
	  	  <input class="form-control" type="file" name="picUpload" id="picUpload">
    	</div>
	    <button type="submit" class="btn btn-primary">Hochladen</button>
		</div>
	</div>
</form>