<h2>Tagesverantwortung</h2>
<div class="row" style="margin-bottom: 2em">
	<div class="col-md-12">
		<p>Du kannst in einem Slide die Tages-Verantwortliche Person mit einem Bild und einem Text erwähnen.</p>
	</div>
</div>
<?php foreach ($resps as $resp):?>
<form action="index.php?action=edit_presentation_responsible_config" method="post">
	<?php if ($resp->getID() == null): ?>
	<div class="row">
		<div class="col-md-12">
			<h2>Neue Tagesverantwortung hinzufügen</h2>
		</div>
	</div>
	<?php endif ?>
	<div class="row">
		<div class="col-md-6">
			<input type="hidden" name="ID" value="<?= $this->e($resp->getID()) ?>" />
			<div class="form-group">
				<label for="datum">Tag, an dem diese Person verantwortlich ist</label> <input type="date" class="form-control" id="datum" name="datum"
					value="<?= $this->e($resp->getDay()) ?>" placeholder="Datum eingeben (tt.mm.jjjj)">
			</div>
			<div class="form-group">
				<label for="ueberschrift">Überschrift über dem Bild</label> <input type="text" class="form-control" id="ueberschrift"
					name="ueberschrift" value="<?= $this->e($resp->getCaption()) ?>" placeholder="Überschrift">
			</div>
			<div class="form-group">
				<label for="picSelect">Bild:</label>
				<select class="form-control" id="picSelect" name="picture">
					<?php foreach ($pics as $pic): ?>
						<option <?php if ($pic['fileName'] == $resp->getPicName()) { echo 'selected'; } ?> ><?= $pic['fileName'] ?></option>
					<?php endforeach ?>
				</select>
			</div>
			<div class="form-group">
				<label for="unterschrift">Unterschrift unter dem Bild</label> <input type="text" class="form-control" id="unterschrift"
					name="unterschrift" value="<?= $this->e($resp->getSubCaption()) ?>" placeholder="Unterschrift">
			</div>
		</div>
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-6">
				  <h1>Tagesverantwortung</h1>
				  <br>
			  	<h2><?= $resp->getCaption() ?></h2>
			  	<br>
			  	<h4><?= $resp->getSubCaption() ?></h4>
			  </div>
			  <div class="col-md-6">
			  	<img align="right" src="<?= $resp->getPicUrl() ?>">
			  </div>
			</div>
					
		</div>
	</div>
	<div class="row" style="margin-bottom: 2em">
		<div class="col-md-6">
			<?php if ($resp->getID() == null): ?>
			<button type="submit" name="new_btn" class="btn btn-primary">Neu erstellen</button>
			<?php else: ?>
			<button type="submit" name="save_btn" class="btn btn-primary">Speichern</button>
			<button type="submit" name="delete_btn" class="btn btn-primary">Löschen</button>
			<?php endif ?>
		</div>
	</div>
</form>
<?php endforeach; ?>