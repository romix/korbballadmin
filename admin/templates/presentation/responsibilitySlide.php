<div class="row">
	<div class="col-md-6">
	  <h1>Tagesverantwortung</h1>
	  <br>
  	<h2><?= $resp->getCaption() ?></h2>
  	<br>
  	<h4 style="line-height: 1.5;"><?= $resp->getSubCaption() ?></h4>
  </div>
  <div class="col-md-6">
  	<img align="right" src="<?= $resp->getPicUrl() ?>">
  </div>
</div>
