<div class="row">
	<div class="col-md-8">
	  <h1>Wir suchen dich!</h1>
	  <br>
  	<h2>Die IVK braucht Deine Unterstützung. Liegt dir Korbball und die Wintermeisterschaft am Herzen? Dann melde dich doch bei einem der IVK-Mitglieder:
  	<br><br> 
  	Vera Wicki, Marlis Egli, Daniel Schneider, Milena Müller oder Melanie Lötscher.</h2>
  	<br>
  	<h4 style="line-height: 1.5;"></h4>
  </div>
  <div class="col-md-4">
  	<img align="right" src="images/helpwanted.jpg">
  </div>
</div>
