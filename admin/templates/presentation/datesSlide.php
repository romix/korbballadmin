<div class="row">
	<div class="col-md-8">
	  <h1>Meisterschaftsdaten 2023/24!</h1>
	  <br>
  	<h2>Die Hallen sind bereits wie folgt reserviert:</h2>
    <div class="row">
      <div class="col-md-6">
        <h3>25./26.11.2023</h3>
        <h3>02./03.12.2023</h3>
        <h3>16./17.12.2023</h3>
      </div>
      <div class="col-md-6">
        <h3>06./07.01.2024</h3>
        <h3>13./14.01.2024</h3>
        <h3>20./21.02.2024</h3>
      </div>
    </div>
  	<br>
  	<h4 style="line-height: 1.5;"></h4>
  </div>
  <div class="col-md-4">
  	<img align="center" width="60%" src="images/Calendarview.png">
  </div>
</div>
