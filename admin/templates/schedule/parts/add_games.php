<form id="add-games" action="index.php?action=add_games" up-target=".main" method="POST">
  <h4>Spiele hinzufügen</h4>
  <div id="fix_template_area">
    <label for="select_group">Gruppe</label>
    <select name="group_id" id="select_group" class="form-control" up-autosubmit>
      <option value="" selected disabled>bitte wählen</option>
      <?php foreach ($groups as $group) { ?>
        <option
          <?php if (isset($_SESSION['schedule']['group']['ID']) and $_SESSION['schedule']['group']['ID'] == $group['ID']) {
            echo('selected');
          } ?>
          value="<?php echo($group['ID']); ?>">
          <?php echo($group['name']); ?>
        </option>
      <?php } ?>
    </select>
    <label for="name_round">Runde</label>
    <input name="round" id="name_round" class="form-control" value="<?php if (isset($_SESSION['schedule']['round'])) {
      echo($_SESSION['schedule']['round']);
    } ?>">
    <label for="select_template">Spielschlüssel</label>
    <select name="template_id" id="select_template" class="form-control" up-autosubmit>
      <option value="" selected disabled>bitte wählen</option>
      <?php foreach ($templates as $template) { ?>
        <option value="<?php echo($template['ID']); ?>"
          <?php if (isset($_SESSION['schedule']['template']['ID']) and $_SESSION['schedule']['template']['ID'] == $template['ID']) {
            echo('selected');
          } ?>
        >
          <?php echo($template['name']); ?>
        </option>
      <?php } ?>
    </select>
  </div>
  <div id="matchdays-area">
    <?php if (isset($_SESSION['schedule']['template']['spieltage'])) { ?>
      <?php for ($i = 1; $i <= $_SESSION['schedule']['template']['spieltage']; $i++) { ?>
        <div class="match-day">
          <div class="row-2-col">
            <div class="col">
              <label for="date">Spieltag <?php echo $i; ?></label>
              <input type="date" class="form-control date" name="date[<?php echo $i; ?>]" onblur="submitForm()"
                     value="<?php echo($_SESSION['schedule']['date'][$i] ?? ''); ?>">
            </div>
            <div class="col">
              <label for="start">Spielbeginn</label>
              <input type="text" class="form-control time" name="start[<?php echo $i; ?>]" onblur="submitForm()"
                     value="<?php echo($_SESSION['schedule']['start'][$i] ?? ''); ?>" placeholder="hh:mm">
            </div>
          </div>
          <label for="court">Hallen</label>
          <div class="row-3-col">
            <?php for ($h = 1; $h <= $_SESSION['schedule']['template']['courts']; $h++) { ?>
              <div class="col">
                <select name="court[<?php echo $i; ?>][<?php echo $h; ?>]" class="form-control" up-autosubmit>
                  <option value="" selected disabled>wählen</option>
                  <?php foreach ($courts as $court) { ?>
                    <option
                      <?php if (isset($_SESSION['schedule']['court'][$i][$h]) and $_SESSION['schedule']['court'][$i][$h] == $court['ID']) {
                        echo('selected');
                      } ?>
                      value="<?php echo($court['ID']); ?>">
                      <?php echo($court['name']); ?>
                    </option>
                  <?php } ?>
                </select>
              </div>
            <?php } ?>
          </div>
        </div>
      <?php } ?>
      <label for="team">Teams</label>
      <?php foreach ($_SESSION['schedule']['group']['teams'] as $key => $team) { ?>
        <input type="hidden" name="move_team[<?php echo($team['ID']); ?>]" value="0"
               id="team_<?php echo($team['ID']); ?>">
        <div class="team">
          <span class="name_part">
            <?php echo($team['name']); ?>
          </span>
          <button onclick="switchTeams(<?php echo($team['ID']); ?>, 1)">up</button>
          <button onclick="switchTeams(<?php echo($team['ID']); ?>, -1)">down</button>
        </div>
      <?php } ?>
    <?php } ?>
  </div>
  <button id="create-round" type="submit" name="submit"
          value="submit" <?php if (!$_SESSION['schedule']['ready'] ?? true) {
    echo 'disabled';
  } ?>>Runde erfassen
  </button>
</form>
<script>
  function switchTeams(team_id, move) {
    let team_field = document.getElementById('team_' + team_id);
    team_field.value = move;
  }

  function submitForm() {
    up.submit('form#add-games', { target: '.main' })
  }
</script>
<style>
  #add-games {
    position: fixed;
    top: 4em;
    right: 10px;
    width: 300px;
    background: #eee;
    padding: 1rem;
  }

  #fix_template_area {
    margin-bottom: 1rem;
  }

  #create-round {
    width: 100%;
    background: #2b542c;
    border-radius: 5px;
    color: white;
    padding: 5px;
  }

  #matchdays-area {
    padding: 1rem 0px;
  }

  .match-day {
    margin-bottom: 1rem;
  }

  .row-2-col {
    display: grid;
    grid-template-columns: auto auto;
  }

  .row-3-col {
    display: grid;
    grid-template-columns: auto auto auto;
  }

  .team > span.name_part {
    width: 200px;
    display: inline-block;
  }
</style>
