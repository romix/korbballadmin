<div id="clipboard" class="drop-area">
  <span>Zwischenablage</span>
  <?php foreach ($clipboard as $clip) { ?>
    <div class="game"
         style="background: #<?php echo($clip['color']) ?>; color: #<?php echo($clip['textColor']) ?>"
         title="<?php echo($clip['league']); ?>" draggable="true"
         id="<?php echo($clip['id']); ?>">
      <?php echo($clip['opponents']); ?>
    </div>
  <?php } ?>
</div>
<style>
  #clipboard {
    position: fixed;
    right: 50%;
    width: 180px;
    background: #eee;
    min-height: 5em;
    bottom: 10px;
    padding: 1rem 1rem 2em;
  }
</style>