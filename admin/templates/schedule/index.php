<?php $this->layout('layout') ?>
<div id="schedule">
  <h2>Spielplan erstellen</h2>
  <?php foreach ($days as $data) { ?>
    <span class="date"><?php echo($data['date']); ?> <?php echo($data['weekday']); ?></span>
    <div class="day">
      <?php foreach ($data['fields'] as $field) { ?>
        <div class="field">
          <span class="field-name"><?php echo($field['name']); ?></span>
          <?php foreach ($field['slots'] as $slot) { ?>
            <div class="slot">
              <div class="time">
                <?php echo($slot['time']); ?>
              </div>
              <div class="drop-area"
                  id="<?php echo($data['date'] . $field['name'] . $slot['time']); ?>"
                  data-date="<?php echo($data['date']); ?>"
                  data-time="<?php echo($slot['time']); ?>"
                  data-halle="<?php echo($field['id']); ?>">
                <?php
                if (isset($slot['game'])) { ?>
                  <div class="game"
                      style="background: #<?php echo($slot['game']['color']) ?>; color: #<?php echo($slot['game']['textColor']) ?>"
                      title="<?php echo($slot['game']['league']); ?>" draggable="true"
                      id="<?php echo($slot['game']['id']); ?>">
                    <?php echo($slot['game']['opponents']); ?>
                  </div>
                <?php } ?>
                <?php if (isset($slot['templateGame'])) { ?>
                  <div class="game template-game">
                    <?php echo ($slot['templateGame']['opponents']) ?>
                  </div>
                <?php } ?>
              </div>
            </div>
          <?php } ?>
        </div>
      <?php } ?>
    </div>
  <?php } ?>
</div>

<?php echo $this->insert('schedule/parts/add_games', [
  'groups' => $groups,
  'templates' => $templates,
  'courts' => $courts
]) ?>

<?php echo $this->insert('schedule/parts/clipboard', [
  'clipboard' => $clipboard,
]) ?>

<style>
  #schedule {
    padding-bottom: 250px;
  }

  .day {
    display: flex;
    flex-wrap: wrap;
    padding: 15px 15px 15px;
    border: 1px solid #999;
    background-color: #f8f8f8;
    overflow: hidden;
    margin-bottom: 15px;
    margin-right: 145px;
  }

  .field {
    width: 300px;
    float: left;
    position: relative;
    border: 1px solid #999;
    margin-right: 15px;
    margin-bottom: 15px;
    padding: 15px;
  }

  .slot {
    border: 1px solid #999;
    display: flex;
    margin-bottom: 5px;
    width: 100%;
  }

  .drop-area {
    width: 100%;
    min-width: 0;
  }

  .time {
    padding: 0.2em 0.4em;
    font-size: 1.1rem;
  }

  .game {
    padding: 0.2em 0.4em;
    width: 100%;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    font-size: 1.1rem;
    cursor: grab;
    -webkit-touch-callout: none; /* iOS Safari */
    -webkit-user-select: none; /* Safari */
    -moz-user-select: none; /* Old versions of Firefox */
    -ms-user-select: none; /* Internet Explorer/Edge */
    user-select: none; /* Non-prefixed version, currently supported by Chrome, Edge, Opera and Firefox */
  }

  .template-game {
    animation: blinker 1s linear infinite;
  }

  @keyframes blinker {
  50% {
    opacity: 0;
  }
}
</style>