import up from "./unpoly";

up.compiler(".drop-area", (dropTarget: HTMLElement) => {
  dropTarget.ondrop = drop;
  dropTarget.ondragover = allowDrop;
});

up.compiler(".game", (game: HTMLElement) => {
  game.ondragstart = drag;
});

function allowDrop(ev: any) {
  if (ev.target.classList.contains('drop-area')) {
    ev.preventDefault();
  }
}

function drag(ev: any) {
  ev.dataTransfer.setData("gameId", ev.target.id);
}

function drop(ev: any) {
  ev.preventDefault();
  const gameId = ev.dataTransfer.getData("gameId");
  ev.target.appendChild(document.getElementById(gameId));
  const dataset = ev.target.dataset;
  if (dataset.date && dataset.time && dataset.halle) {
    const formData = new FormData();
    formData.append('gameId', gameId);
    formData.append('targetDate', dataset.date);
    formData.append('targetTime', dataset.time);
    formData.append('targetHalle', dataset.halle);
    fetch('index.php?action=move_game', {
      method: "POST",
      body: formData
    }).then(result => {
      up.reload('.main');
    });
  } else if (ev.target.id === 'clipboard') {
    const formData = new FormData();
    formData.append('gameId', gameId);
    fetch('index.php?action=move_game_to_clipboard', {
      method: "POST",
      body: formData
    }).then(result => {
      up.reload('.main');
    });
  }
}
