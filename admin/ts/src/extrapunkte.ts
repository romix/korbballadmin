import { form2js } from 'form2js';
import * as yup from 'yup';

const initExtrapunkteForm = () => {
  for (const form of document.forms) {
    if (form.name == "extrapunkte_new") {
      console.log('instrument extrapunkte form');
      const schema = yup.object().shape({
        gruppeID: yup.number().required(), 
        teamID: yup.number().required().positive().integer(), 
        extrapunkte: yup.number().required().notOneOf([0]), 
        grund: yup.string().required()
      });

      const submitButton = form.elements.namedItem("submit") as HTMLButtonElement;

      const validate = (ev: FocusEvent) => {
        const formJson = form2js(form);
        submitButton.disabled = !schema.isValidSync(formJson);
      }
      for(const fld of form) {
        (fld as HTMLElement).onchange = validate;
      }
    }
  }
}

document.addEventListener("turbolinks:load", initExtrapunkteForm);
window.addEventListener("load", initExtrapunkteForm);