declare module "form2js" {
  function form2js(form: Node, delimiter?: string, skipEmpty?: boolean, nodeCallback?: (node: Node) => boolean, useIdIfEmptyName?: boolean): Object;

}