<?php header('Cache-Control: no-cache'); ?>
<?php header('Content-type: text/html; charset=utf-8'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Resultate</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8">
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"
	rel="stylesheet">
<link href="css/content.css" rel="stylesheet">
</head>
<body>
	<div class="single_embedded" style="text-align: left">
	<div class="container-fluid">

<?php
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$groups_ws_link = substr ( $actual_link, 0, strpos ( $actual_link, '/admin/' ) ) . '/ws/groupsV1.php';
$jsonGroups = json_decode ( file_get_contents ( $groups_ws_link ), true );
$ranking_ws_link = substr ( $actual_link, 0, strpos ( $actual_link, '/admin/' ) ) . '/ws/rankingV1.php?group=' . $_GET ['gruppe'];
$jsonRanking = json_decode ( file_get_contents ( $ranking_ws_link ), true );
$games_ws_link = substr ( $actual_link, 0, strpos ( $actual_link, '/admin/' ) ) . '/ws/gamesV1.php?group=' . $_GET ['gruppe'];
$jsonGames = json_decode ( file_get_contents ( $games_ws_link ), true );
$annotations_ws_link = substr ( $actual_link, 0, strpos ( $actual_link, '/admin/' ) ) . '/ws/annotationsV1.php?group=' . $_GET ['gruppe'];
$jsonAnnotations = json_decode ( file_get_contents ( $annotations_ws_link ), true );

function echoRanking($group, $rankings) {
	$groupName = $group ['name'];
	echo '<div class="row">';
	echo '<div class="col-md-12">';
	echo '<h3>Rangliste ' . $groupName . '</h3>';
	echo '<table class="table table-striped table-condensed">';
	foreach ( $rankings as $ranking ) {
		echo '<tr>';
		echo '<td>' . $ranking ['rank'] . '.</td>';
		echo '<td>' . $ranking ['team'] . '</td>';
		echo '<td class="text-right">' . $ranking ['played'] . '</td>';
		echo'<td class="text-right">' . $ranking ['won'] . '</td>';
		echo'<td class="text-right">' . $ranking ['lost'] . '</td>';
		echo'<td class="text-right">' . $ranking ['tie'] . '</td>';
		echo '<td class="text-right">' . $ranking['rate'][0] . ' : ' . $ranking ['rate'][1] . '</td>';
		$rel = ($ranking['rate'][0] - $ranking ['rate'][1]);
		if ($rel >= 0) {
			$rel = '+' . $rel;
		}
		echo '<td class="text-right">' . $rel . '</td>';
		echo '<td class="text-right">' . $ranking ['points'] . '</td>';
		echo '</tr>';
	}
	echo '</table>';
	echo '</div>';
	echo '</div>';
}

function echoAnnotations($jsonAnnotations) {
	if (count($jsonAnnotations) > 0) {
		echo '<div class="row">';
		echo '<div class="col-md-12">';
		echo '<h4>Bemerkungen:</h4>';
		foreach ($jsonAnnotations as $annotation) {
			echo $annotation . '<br/>';
		}
		echo '</div>';
		echo '</div>';
	}
}

function echoResults($group, $games) {
	echo '<div class="row">';
	echo '<div class="col-md-12">';
	$groupName = $group ['name'];
	echo '<h3>Resultate ' . $groupName . '</h3>';
	$prevDay = '';
	$tableStarted = false;
	foreach ($games as $game) {
		if (!$game['resultatA']) {
			continue;
		}
		if ($game['tag'] != $prevDay) {
			if ($tableStarted) {
				echo '</table>';
			}
			echo '<h4>';
			$prevDay = $game['tag'];
			echo $game['runde'] . ' ' . dat_m2u($game['tag']) . ':';
			echo '</h4>';
			echo '<table class="table table-striped table-condensed">';
			$tableStarted = true;
		}
		echo '<tr>';
		echo '<td>' . substr($game['zeit'],0,strpos($game['zeit'], ':', 3)) . '</td>';
		echo '<td>'. $game['txtTeamA'] . '</td>';
		echo '<td>'. $game['txtTeamB'] . '</td>';
    	echo '<td class="text-right">'. $game['resultatA'] . ' : ' . '</td>';
    	echo '<td class="text-right" style="width: 26px;">'. $game['resultatB'] . '</td>';
		echo '</tr>';
	}
	if ($tableStarted) {
		echo '</table>';
		$tableStarted = false;
	}
	echo '</div>';
	echo '</div>';
	
}

function dat_m2u($datum) {
	$day = substr($datum, 8, 2);
	$month = substr($datum, 5, 2);
	$year = substr($datum, 0, 4);
	return "$day.$month.$year";
}

if (isset ( $jsonRanking )) {
	$gruppe = array();
	foreach ($jsonGroups as $jsonGroup) {
		if ($jsonGroup['group']['ID'] == $_GET['gruppe']) {
			$gruppe = $jsonGroup['group'];
		}
	}
	echoRanking($gruppe, $jsonRanking);
}

if (isset($jsonAnnotations)) {
	echoAnnotations($jsonAnnotations);
}

if (isset($jsonGames)) {
	echoResults($gruppe, $jsonGames);
}
?>

</div>
</div>

</body>
</html>
