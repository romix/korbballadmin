<?php

class c_team extends BaseCRUD {

    function __construct(IVKAdmin $adm) {
    	parent::__construct($adm);
      $this->title = "Mannschaften";

      $this->sqlSel['pub'] = "SELECT mannschaft.ID, liga, gruppe, mannschaft, verein, concat(b_name, ' ', b_vorname) AS captain, b_strasse, b_plz, b_ort, b_tel, b_email
                              FROM mannschaft LEFT JOIN liga ON mannschaft.ligaID=liga.ID LEFT JOIN verein on mannschaft.vereinID=verein.ID LEFT JOIN teamgruppe ON teamgruppe.teamID=mannschaft.ID LEFT JOIN gruppe ON gruppe.ID=teamgruppe.gruppeID";
      $this->fldSel['pub'] = array("liga", "gruppe", "mannschaft", "verein", "captain", "b_strasse", "b_plz", "b_ort", "b_tel", "b_email");
      $this->descSel['pub'] = array("Liga", "Gruppe", "Mannschaft", "Verein", "Captain", "Strasse", "PLZ", "Ort", "Telefon" ,"E-Mail");

      $this->sqlSel['admin'] = "SELECT mannschaft.ID, liga, gruppe, mannschaft, verein, concat(b_name, ' ', b_vorname) AS captain, b_strasse, b_plz, b_ort, b_tel
                              FROM mannschaft LEFT JOIN liga ON mannschaft.ligaID=liga.ID LEFT JOIN verein on mannschaft.vereinID=verein.ID LEFT JOIN teamgruppe ON teamgruppe.teamID=mannschaft.ID LEFT JOIN gruppe ON gruppe.ID=teamgruppe.gruppeID";
      $this->fldSel['admin'] = array("liga", "gruppe", "mannschaft", "verein", "captain", "b_strasse", "b_plz", "b_ort", "b_tel");
      $this->descSel['admin'] = array("Liga", "Gruppe", "Mannschaft", "Verein", "Captain", "Strasse", "PLZ", "Ort", "Telefon" );

      $this->sqlUpd['edit'] = "SELECT mannschaft.ID, mannschaft, vereinID, ligaID, tenue, hosen, ersatz_tenue, ersatz_hosen, b_anrede, b_name, b_vorname, b_strasse, b_plz, b_ort, b_tel FROM mannschaft";
      $this->fldUpd['edit'] = array("mannschaft", "vereinID", "ligaID", "tenue", "hosen", "ersatz_tenue", "ersatz_hosen", "b_anrede", "b_name", "b_vorname", "b_strasse", "b_plz", "b_ort", "b_tel");
      $this->descUpd['edit'] = array("Mannschaft", "Verein", "Liga", "Tenue", "Hosen", "Ersatz Tenue", "Ersatz Hosen", "<b>Betreuer</b>\n<br>Anrede", "Name", "Vorname", "Strasse", "PLZ", "Ort", "Telefon");
      $this->updUpd['edit'] = "UPDATE mannschaft SET mannschaft=:mannschaft, vereinID=:vereinID, ligaID=:ligaID, tenue=:tenue, hosen=:hosen, ersatz_tenue=:ersatz_tenue, ersatz_hosen=:ersatz_hosen, b_anrede=:b_anrede, b_name=:b_name, b_vorname=:b_vorname, b_strasse=:b_strasse, b_plz=:b_plz, b_ort=:b_ort , b_tel=:b_tel";

      $this->sqlIns['new'] = "INSERT INTO mannschaft SET mannschaft=:mannschaft, vereinID=:vereinID, ligaID=:ligaID, tenue=:tenue, hosen=:hosen, ersatz_tenue=:ersatz_tenue, ersatz_hosen=:ersatz_hosen, b_anrede=:b_anrede, b_name=:b_name, b_vorname=:b_vorname, b_strasse=:b_strasse, b_plz=:b_plz, b_ort=:b_ort, b_tel=:b_tel;";
      $this->fldIns['new'] = array("mannschaft", "vereinID", "ligaID", "tenue", "hosen", "ersatz_tenue", "ersatz_hosen", "b_anrede", "b_name", "b_vorname", "b_strasse", "b_plz", "b_ort", "b_tel");
      $this->descIns['new'] = array("Mannschaft", "Verein", "Liga", "Tenue", "Hosen", "Ersatz Tenue", "Ersatz Hosen", "<b>Betreuer</b>\n<br>Anrede", "Name", "Vorname", "Strasse", "PLZ", "Ort", "Telefon");

      $this->key = "mannschaft.ID";
      $this->required = array("mannschaft", "vereinID", "ligaID", "tenue", "hosen", "ersatz_tenue", "ersatz_hosen", "b_name", "b_vorname");
      $this->fieldtype = array(
        "ligaID"=>"combo",
        "vereinID"=>"combo"
      );

      $stmt = $adm->prepareStatement("SELECT * FROM liga");
      $stmt->execute();
      while($row = $stmt->fetch()) {
        $this->fieldinfo['ligaID']['datasource'][$row['ID']] = $row['liga'];
      }

    } 

    //Auswahl auf den Besitzer beschränken
    function user($ID) {
    	//Admin-Benutzer dürfen alle Vereine benutzen.
    	$stmt = $this->prepareStatement("SELECT rights FROM user WHERE ID=:id");
    	$stmt->execute(array("id" => $ID));
      $row = $stmt->fetch();
      if ($row['rights']=="adm") {
      	$stmt = $this->prepareStatement("SELECT ID, verein FROM verein;");
      	$stmt->execute();
      } else {
      	$stmt = $this->prepareStatement("SELECT ID, verein FROM verein WHERE owner=:id");
      	$stmt->execute(array("id" => $ID));
      }
      while($row = $stmt->fetch()) {
        $this->fieldinfo['vereinID']['datasource'][$row['ID']] = $row['verein'];
      }
    }

    //Links einfügen
    function row_finalize($row, $context) {
      if ($context=="admin") {
        $row['mannschaft'] = "<a class='btn btn-default btn-xs' href='index.php?action=print_edit_team&team=$row[ID]' title='bearbeiten'><img src='images/edit.gif' border=0></a> " . $row['mannschaft'];
        $row['mannschaft'] = "<a class='btn btn-default btn-xs' href='index.php?action=print_delete_team&team=$row[ID]' title='löschen'><img src='images/delete.gif' border=0></a> " . $row['mannschaft'];
        return $row;
      } else {
        return $row;
      }
    }

    //Team Löschen fragen
    function askDelete($teamId) {
    	$stmt = $this->prepareStatement("SELECT * FROM mannschaft WHERE mannschaft.ID=:teamId");
    	$this->executeStatement($stmt, array(":teamId" => $teamId));
    	$teamName = $stmt->fetch()['mannschaft'];
    	
    	$deleteValidation = $this->deleteValidation($teamId);
    	if ($deleteValidation == '') {
	    	$str  = '<p>Willst du das Team <b>' . $teamName . '</b> wirklich l&ouml;schen?</p>';
  	  	$str .= '<p>';
  	  	$str .= '<form name="deleteForm" method="POST" action="index.php?action=delete_team">';
  	  	$str .= '<input type="hidden" name="teamId" value="' . $teamId . '"/>';
  	  	$str .= '<a class="btn btn-primary" href="javascript:document.forms[\'deleteForm\'].submit();">Team ' . $teamName . ' löschen</a> ';
  	  	$str .= '<a class="btn btn-default" href="javascript:history.back();">zur&uuml;ck</a>';
  	  	$str .= '</form>';
  	  	$str .= '</p>';
    		return $str;
    	} else {
    		$str  = '<p>';
    		$str .= $deleteValidation;
    		$str .= '</p>';
        $str .= '<p><a class="btn btn-default" href="javascript:history.back();">zur&uuml;ck</a></p>';
        return $str;
    	}
    }
    
    //Team löschen
    function delete($teamId) {
    	$deleteValidation = $this->deleteValidation($teamId);
    	if ($deleteValidation == '') {
	    	$stmt = $this->prepareStatement("DELETE FROM mannschaft WHERE ID=:teamId");
  	  	$this->executeStatement($stmt, array(':teamId' => $teamId));
    		$str  = "<p>Mannschaft wurde gel&ouml;scht!</p>";
    		$str .= '<p><a class="btn btn-default" href="index.php?action=print_teams">zur&uuml;ck</a></p>';
    		echo $str;
    	} else {
    		echo '<p>';
    		echo $deleteValidation;
    		echo '</p>';
  	  	echo '<p><a class="btn btn-default" href="javascript:history.back();">zur&uuml;ck</a></p>';
    	}
    }
    
    function deleteValidation($teamId) {
    	$validation = '';
    	$stmt = $this->prepareStatement("SELECT count(*) FROM spiel WHERE teamA = :teamId OR teamB = :teamId");
    	$stmt->execute(array(":teamId" => $teamId));
    	$gamesCount = $stmt->fetch()[0];
    	if ($gamesCount > 0) {
    		$validation .= 'Mannschaft kann nicht gelöscht werden, da sie noch in ' . $gamesCount . ' Spiele eingeteilt ist. Bitte l&ouml;sche diese Spiele zuerst.<br/>';
    	}
    	$stmt = $this->prepareStatement("SELECT count(*) FROM teamgruppe WHERE teamID = :teamId");
    	$stmt->execute(array(":teamId" => $teamId));
    	$groupCount = $stmt->fetch()[0];
    	if ($groupCount > 0) {
    		$validation .= 'Mannschaft kann nicht gelöscht werden, da sie noch in ' . $groupCount . ' Gruppen eingeteilt ist. Bitte lösche diese Zuordnung zuerst.<br/>';
    	}
    	
    	return $validation;
    }
  } 

?>
