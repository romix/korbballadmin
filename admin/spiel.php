<?php

class c_spiel extends BaseCRUD {

	//Constructor
	function __construct(IVKAdmin $adm) {
		parent::__construct($adm);
		$this->adm = $adm;
		$this->title = "Spiele";

		$this->sqlSel['admin'] = "SELECT spiel.ID, tag, rundeID, halle, zeit, liga, gruppe, teamA, teamB, tabteamA.mannschaft AS txtTeamA, tabteamB.mannschaft AS txtTeamB
		FROM spiel
		LEFT JOIN halle ON spiel.halleID=halle.ID
		LEFT JOIN gruppe ON spiel.gruppeID=gruppe.ID
		LEFT JOIN liga ON gruppe.ligaID=liga.ID
		LEFT JOIN mannschaft AS tabteamA ON spiel.teamA=tabteamA.ID
		LEFT JOIN mannschaft AS tabteamB ON spiel.teamB=tabteamB.ID";
		$this->fldSel['admin'] = array("tag", "halle", "zeit", "liga", "gruppe", "txtTeamA", "txtTeamB");
		$this->descSel['admin'] = array("Tag", "Halle", "Zeit", "Liga", "Gruppe", "Mannschaft 1", "Mannschaft 2");

		$this->sqlSel['pub'] = "SELECT spiel.ID, tag, rundeID, halle, zeit, liga, gruppe, tabteamA.mannschaft AS txtTeamA, tabteamB.mannschaft AS txtTeamB
		FROM spiel
		LEFT JOIN halle ON spiel.halleID=halle.ID
		LEFT JOIN gruppe ON spiel.gruppeID=gruppe.ID
		LEFT JOIN liga ON gruppe.ligaID=liga.ID
		LEFT JOIN mannschaft AS tabteamA ON spiel.teamA=tabteamA.ID
		LEFT JOIN mannschaft AS tabteamB ON spiel.teamB=tabteamB.ID";
		$this->fldSel['pub'] = array("tag", "halle", "zeit", "liga", "gruppe", "txtTeamA", "txtTeamB");
		$this->descSel['pub'] = array("Tag", "Halle", "Zeit", "Liga", "Gruppe", "Mannschaft 1", "Mannschaft 2");

		$this->sqlUpd['edit'] = "SELECT spiel.ID, tag, zeit, halleID FROM spiel";
		$this->fldUpd['edit'] = array("tag", "zeit", "halleID");
		$this->descUpd['edit'] = array("Tag", "Zeit", "Halle");
		$this->updUpd['edit'] = "UPDATE spiel SET tag=:tag, zeit=:zeit, halleID=:halleID";

		$this->key = "spiel.ID";
		$this->required = array("tag", "zeit", "halleID");

		$this->fieldtype = array(
				"halleID"=>"combo"
		);

		$stmt = $adm->prepareStatement("SELECT halle.ID AS ID, halle FROM halle");
		$stmt->execute();
		while($row = $stmt->fetch()) {
			$this->fieldinfo['halleID']['datasource'][$row['ID']] = $row['halle'];
		}

	} //End Constructor

	//Auszugebende Zeile bearbeiten
	function row_finalize($row, $context) {
		if ($context=='admin') {
			$row['tag'] = '<a href="index.php?action=print_edit_spiel&spiel=' . $row['ID'] . '&runde=' . $row['rundeID'] . '" title="Spiel bearbeiten"><img src="images/edit.gif" border=0></a> ' . $this->dat_m2u($row['tag']);
			$row['zeit'] = $this->time_m2u($row['zeit']);
		}
		if (strpos($row['teamA'], ".")) {
			$tab = new c_tabelle($this->adm);
			$arr = $tab->arrTeamCode($row['teamA']);
			$row['txtTeamA'] = $arr['txt'];
		}
		if (strpos($row['teamB'], ".")) {
			$tab = new c_tabelle($this->adm);
			$arr = $tab->arrTeamCode($row['teamB']);
			$row['txtTeamB'] = $arr['txt'];
		}
		return $row;
	}

	//Formular zum Einfügen von Runden-Spielen
	function addRunde1($runde, $target) {
		//Tabelle runde öffnen
		$stmt = $this->adm->prepareStatement("SELECT * FROM runde WHERE ID=:ID");
		$stmt->execute(['ID' => $runde]);
		$rRunde = $stmt->fetch();
		$stmt->closeCursor();

		//Tabelle gruppe und liga öffnen
		$stmt = $this->prepareStatement("SELECT * FROM gruppe LEFT JOIN liga ON gruppe.ligaID=liga.ID WHERE gruppe.ID=:gruppeId");
		$stmt->execute(['gruppeId' => $rRunde['gruppeID']]);
		$rGL = $stmt->fetch();
		$stmt->closeCursor();
		
		$stmt = $this->adm->prepareStatement('SELECT count(*) FROM teamgruppe WHERE gruppeID = :gruppeId');
		$stmt->execute(['gruppeId' => $rRunde['gruppeID']]);
		$groupSize = $stmt->fetch()[0];
		$stmt->closeCursor();

		//Tabelle template öffnen
		$stmt = $this->adm->prepareStatement("SELECT * FROM template WHERE teams = :teams");
		$stmt->execute(['teams' => $groupSize]);

		//Formular beginnen
		$str = "<form name=\"matrix\" action=\"$target\" method=\"POST\">\n";

		//Kopf ausgeben
		$str .= '<h3>Spiele&nbsp;erstellen&nbsp;f&uuml;r ' . $rGL['gruppe'] . ' ' . $rRunde['runde'] . ':</h3>';

		$str .= '<div class="form-group">';
		$str .= '<label for="vorlage">Spiel-Vorlage</label>';
		$str .= '<select id="vorlage" class="form-control" name="data[template]">';
		while ($row = $stmt->fetch()) {
			$str .= '<option value="' . $row['ID'] . '">' . $row['template'] . '</option>\n';
		}
		$stmt->closeCursor();
		$str .= "</select>";
		$str .= '</div>';


		$str .= "<input type=\"hidden\" name=\"data[runde]\" value=\"$runde\">\n";
		$str .= '<button type="submit" class="btn btn-primary">Spiele erstellen</button>';
		$str .= "</form>\n";

		return $str;
	} //End addRunde1

	//Zweites Formular f�r die Runden-Spiele
	function addRunde2($data, $target) {
		$str = '';
		//Tabelle runde �ffnen
		$stmt = $this->prepareStatement("SELECT * FROM runde WHERE ID=:runde");
		$stmt->execute(['runde' => $data['runde'] ?? '']);
		$rRunde = $stmt->fetch();
		$stmt->closeCursor();

		//Tabelle gruppe �ffnen
		$stmt = $this->prepareStatement("SELECT * FROM gruppe WHERE ID=:gruppeID");
		$stmt->execute(['gruppeID' => $rRunde['gruppeID'] ?? '']);
		$rGruppe = $stmt->fetch();
		$stmt->closeCursor();

		//Tabelle template �ffnen
		$stmt = $this->prepareStatement("SELECT * FROM template WHERE ID=:template;");
		$stmt->execute(['template' => $data['template'] ?? '']);
		$rTemplate = $stmt->fetch();
		$stmt->closeCursor();
		
		//Tabelle template matrix
		$stmt = $this->prepareStatement('SELECT * FROM temp_matrix WHERE templateID=:template ORDER BY zeile');
		$stmt->execute(['template' => $data['template'] ?? '']);
		$temp_matrix = array();
		while ($row = $stmt->fetch()) {
			$spieltag = $row['spieltag'];
			$zeile = $row['zeile'];
			$halle = $row['halle'];
			if ($row['teamA'] && $row['teamB']) {
				$spiel = $row['teamA'] . ' : ' . $row['teamB'];
			} else {
				$spiel = '';
			}
			$temp_matrix[$spieltag][$zeile][$halle] = $spiel;
		}
		$stmt->closeCursor();
		
		//Tabelle gruppe und liga öffnen
		$stmt = $this->prepareStatement("SELECT * FROM gruppe LEFT JOIN liga ON gruppe.ligaID=liga.ID WHERE gruppe.ID=:gruppeId");
		$stmt->execute(['gruppeId' => $rRunde['gruppeID'] ?? '']);
		$rGL = $stmt->fetch();
		$stmt->closeCursor();

		//Formular beginnen
		$str .= "<form class=\"form-horizontal\" name=\"matrix\" action=\"$target\" method=\"POST\">\n";

		//Kopf ausgeben
		$str .= "<h3>Spiele&nbsp;erstellen&nbsp;f&uuml;r ".($rGL['gruppe'] ?? '')." ".($rRunde['runde'] ?? '').":</h3>";

		$str .= "<h4>Vorlage: ".($rTemplate['template'] ?? '')."</h4>";

		$str .= '<div class="form-group">';
		$str .= '<label class="col-sm-2 control-label" for="dauer">Dauer eines Spiels (Minuten)</label>';
		$str .= '<div class="col-sm-3">';
		$str .= '<input id="dauer" class="form-control" type="number" size=5 name="data[dauer]" value="'.$this->adm->gameDuration.'">';
		$str .= '</div>';
		$str .= '</div>';

		// über Spieltage iterieren
		for ($i=1; $i<=$rTemplate['spieltage']; $i++) {
			
			$str .= '<div class="row">';
			$str .= '<div class="col-sm-6">';
			$str .= "<h4> Spieltag $i</h4>";
			//Datum
			$str .= '<div class="form-group">';
			$str .= '<label class="col-sm-4 control-label" for="datum' . $i . '">Datum</label>';
			$str .= '<div class="col-sm-6">';
			$str .= '<input id="datum' . $i . '" class="form-control" type="date" name="datum[' . $i . ']" size=10 required>';
			$str .= '</div>';
			$str .= '</div>';

			//Beginn
			$str .= '<div class="form-group">';
			$str .= '<label class="col-sm-4 control-label" for="beginn' . $i . '">Spielbeginn</label>';
			$str .= '<div class="col-sm-6">';
			$str .= '<input id="beginn' . $i . '" class="form-control" type="time" name="beginn[' . $i . ']" value="09:00" size=5>';
			$str .= '</div>';
			$str .= '</div>';
			
			//Hallen
			$stmt = $this->prepareStatement('SELECT * FROM halle');
			for ($h=1; $h<=$rTemplate['hallen']; $h++) {
				$stmt->execute();
				$str .= '<div class="form-group">';
				$str .= '<label class="col-sm-4 control-label" for="halle_' . $i . '_' . $h . '">Halle ' . $h . '</label>';
				$str .= '<div class="col-sm-6">';
				$str .= '<select id="halle_' . $i . '_' . $h . '" class="form-control" name="halle[' .  	$i . ';' . $h . ']">';
				while ($row = $stmt->fetch()) {
					$str .= "<option value=\"$row[ID]\">$row[halle]</option>";
				}
				$str .= "</select>";
				$str .= '</div>';
				$str .= '</div>';
				$stmt->closeCursor();
			}
			$str .= '</div>';
			
			$str .= '<div class="col-sm-6">';
			$str .= '<h4>Spielschlüssel</h4>';
			$currentMatrix = $temp_matrix[$i];
			$str .= '<table class="table table-bordered table-condensed">';
			foreach ($currentMatrix as $zeileId => $zeile) {
				$str .= '<tr>';
				for ($h=1; $h<=$rTemplate['hallen']; $h++) {
					$str .= '<td>' . $zeile[$h] . '</td>';
				}
				$str .= '</tr>';
			}
			$str .= '</table>';
			$str .= '</div>';
			
			$str .= '</div>'; // end row
		}
		
		$str .= '<h4>Mannschaften</h4>';

		//Mannschaften
		if ($rGruppe["type"]=="liga") {
			//Normale Gruppe
			$stmt = $this->prepareStatement('SELECT mannschaft.ID as ID, mannschaft FROM runde
						LEFT JOIN gruppe ON runde.GruppeID=gruppe.ID
						LEFT JOIN teamgruppe ON gruppe.ID=teamgruppe.gruppeID
						LEFT JOIN mannschaft ON teamgruppe.teamID = mannschaft.ID
						WHERE runde.ID=:runde ORDER BY teamgruppe.ID');
			for ($i=1; $i<=$rTemplate['teams']; $i++) {
				$stmt->execute(['runde' => $data['runde']]);
				$str .= '<div class="form-group">';
				$str .= '<label class="col-sm-2 control-label" for="team' . $i . '">Mannschaft ' . $i . '</label>';
				$str .= '<div class="col-sm-3">';
				$str .= "<select id=\"team[$i]\" class=\"form-control\" name=\"team[$i]\">";
				$selected=0;
				while ($row = $stmt->fetch()) {
					$selected++;
					if ($selected!=$i) {
						$str .= "<option value=\"$row[ID]\">$row[mannschaft]</option>";
					} else {
						$str .= "<option value=\"$row[ID]\" selected>$row[mannschaft]</option>";
					}
				}
				$stmt->closeCursor();
				$str .= "</select></div></div>";
			}

		} else {
			//Final Gruppe
			$tab = new c_tabelle($this->adm);
			$stmt = $this->prepareStatement('SELECT teamgruppe.ID, teamCode FROM runde
						LEFT JOIN gruppe ON runde.GruppeID=gruppe.ID
						LEFT JOIN teamgruppe ON gruppe.ID=teamgruppe.gruppeID
						WHERE runde.ID=:runde');
			for ($i=1; $i<=$rTemplate['teams']; $i++) {
				$stmt->execute(['runde' => $data['runde']]);
				$str .= '<div class="form-group">';
				$str .= '<label class="col-sm-2 control-label" for="team' . $i . '">Mannschaft ' . $i . '</label>';
				$str .= '<div class="col-sm-3">';
				$str .= "<select id=\"team[$i]\" class=\"form-control\" name=\"team[$i]\">";
				$selected = 0;
				while ($row = $stmt->fetch()) {
					$selected++;
					$arrTeamCode = $tab->arrTeamCode($row['teamCode']);
					if ($selected!=$i) {
						$str .= "<option value=\"$row[teamCode]\">" . $arrTeamCode['txt'] . "</option>\n";
					} else {
						$str .= "<option value=\"$row[teamCode]\" selected>" . $arrTeamCode['txt'] . "</option>\n";
					}
				}
				$stmt->closeCursor();
				$str .= "</select></div></div>";
			}
		}

		$str .= "<input type=\"hidden\" name=\"data[runde]\" value=\"$data[runde]\">\n";
		$str .= "<input type=\"hidden\" name=\"data[template]\" value=\"$rTemplate[ID]\">\n";
		$str .= '<button type="submit" class="btn btn-primary">Spiele erstellen</button>';
		$str .= "</form>\n";

		return $str;
	} //End addRunde2


	function addRunde3($data, $halle, $team, $datum, $beginn) {
		//Tabelle runde �ffnen
		$stmt = $this->prepareStatement('SELECT * FROM runde WHERE ID=:ID');
		$stmt->execute(['ID' => $data['runde']]);
		$rRunde = $stmt->fetch();
		$stmt->closeCursor();

		//Tabelle gruppe �ffnen
		$stmt = $this->prepareStatement('SELECT * FROM gruppe WHERE ID=:ID');
		$stmt->execute(['ID' => $rRunde['gruppeID']]);
		$rGruppe = $stmt->fetch();
		$stmt->closeCursor();

		//Tabelle template �ffnen
		$stmt = $this->prepareStatement('SELECT * FROM template WHERE ID=:template');
		$stmt->execute(['template' => $data['template']]);
		$rTemplate = $stmt->fetch();
		$stmt->closeCursor();

		$this->insertGames($data['template'], $rRunde['ID'], $rRunde['gruppeID'], $halle, $team, $datum, $beginn, $data['dauer']);
	} //End addRunde3

  /**
   * Insert Games for a runde
   *
   * @param $template_id
   * @param $runde_id
   * @param $gruppe_id
   * @param $halle
   * @param $team
   * @param $datum
   * @param $beginn
   * @param $dauer
   */
  public function insertGames($template_id, $runde_id, $gruppe_id, $halle, $team, $datum, $beginn, $dauer) {
    //Template Matrix öffnen
    $templateStmt = $this->prepareStatement('SELECT * FROM temp_matrix WHERE templateID=:template ORDER BY zeile');
    $templateStmt->execute(['template' => $template_id]);

    //Spiele erstellen
    while ($row = $templateStmt->fetch()) {
      $ts = mktime(substr($beginn[$row['spieltag']], 0, 2), substr($beginn[$row['spieltag']], -2));
      $time=date("H:i", $ts + ($row['zeile']-1) * 60 * $dauer);
      $myDatum = $datum[$row['spieltag']];
      $insertStmt = $this->prepareStatement('INSERT INTO spiel SET
				rundeID=:rundeID,
				gruppeID=:gruppeID,
				halleID=:halleID,
				tag=:datum,
				zeit=:zeit,
				teamA=:teamAID,
				teamB=:teamBID
			');
      $params = [
        'rundeID' => $runde_id,
        'gruppeID' => $gruppe_id,
        'halleID' => $halle["$row[spieltag];$row[halle]"],
        'datum' => $myDatum,
        'zeit' => $time,
        'teamAID' => $team[$row['teamA']],
        'teamBID' => $team[$row['teamB']]
      ];
      $insertStmt->execute($params);
    }
  }
	
	function showMoveDay() {
		$str = '<h3>Spieltag verschieben</h3>';
		$str .= '<div class="col-md-2">';
		$str .= '<form action="index.php?action=do_move_day" method="POST">';
		
		$stmt = $this->adm->prepareStatement("select tag from spiel group by tag order by tag");
		$stmt->execute();
		$allDays = $stmt->fetchAll();
		$str .= '<div class="form-group">';
		$str .= '<label for="tag">Spieltag</label>';
		$str .= '<select class="form-control" id="tag" name="data[day]">';
		foreach ($allDays as $key => $day) {
			$tag = dat_m2u($day['tag']);
			$str .= '<option value="' . $day['tag'] . '">' . $tag . '</option>';
		}
		$str .= '</select>';
		$str .= '</div>';

		$str .= '<div class="form-group"><label for="tagNeu">Verschieben nach</label><input id="tagNeu" type="date" name="data[dayto]" value="" class="form-control"></div>';
		$str .= '<button class="btn btn-primary">Verschieben</button><br/><br/>';
		$str .= '</form>';
		$str .= '</div>';
		
		return $str;
	}

	function moveGame($gameId, $targetDate, $targetTime, $targetHalle) {
		$stmt = $this->adm->prepareStatement("update spiel set tag = :dayTo, halleID = :halleId, zeit = :zeit where spiel.ID = :gameId");
		$result = $stmt->execute(array('gameId' => $gameId, 'dayTo' => $targetDate, 'halleId' => $targetHalle, 'zeit' => $targetTime));
	}
	
	function moveDay(array $data) {
		$dayFrom = $data['day'];
		$dayTo = $data['dayto'];
		
		// sanity check
		$stmt = $this->adm->prepareStatement("select count(tag) from spiel where tag = :dayTo");
		$stmt->execute(array('dayTo' => $dayTo));
		$countDaysAtDestination = $stmt->fetch()[0];
		if ($countDaysAtDestination > 0) {
			return false;
		}
		
		// real movement
		$stmt = $this->adm->prepareStatement('update spiel set tag = :dayTo where tag = :dayFrom');
		$stmt->execute(array('dayFrom' => $dayFrom, 'dayTo' => $dayTo));
		return true;
	}

	function didMoveDay($day, $dayto) {
		$str .= '<div class="alert alert-success" role="alert">Spiele erfolgreich vom ' . dat_m2u($day) . ' auf den ' . dat_m2u($dayto) . ' verschoben.</div>';
		return $str;
	}

	function didNotMoveDay($day, $dayto) {
		$str .= '<div class="alert alert-danger" role="alert">
		Du kannst die Spiele vom ' . dat_m2u($day) . ' nicht auf den ' . dat_m2u($dayto) . ' verschieben. An diesem Datum existieren bereits Spiele.
				</div>';
		return $str;
	}

	//Datum vom MySQL-Datum in Benutzer-Datum umwandeln
	function dat_m2u($datum) {
		$day = substr($datum, 8, 2);
		$month = substr($datum, 5, 2);
		$year = substr($datum, 0, 4);
		return "$day.$month.$year";
	}

	//Zeit vom MySQL-Zeit in Benutzer-Datum umwandeln
	function time_m2u($zeit) {
		$hour = substr($zeit, 0, 2);
		$minute = substr($zeit, 3, 2);
		return "$hour:$minute";
	}


} //End Class

?>
