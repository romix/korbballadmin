<?php

class Presentation {

	/**
	 * @var IVKAdmin
	 */
	private $adm;
  private $presentationConfig;
  var $slides; // <slide> => <seconds>
  var $curSlideNr;
  var $slide;
  var $seconds;
  var $day;

  /*
   * Konstruktor:
   * Hier werden die Slides von der Session oder von der DB geholt.
   */
  function Presentation (IVKAdmin $adm, $day = NULL) {
    $time = time();
    if (isset($day)) {
    	$this->day = $day;
    } else {
	    $this->day = date('Y-m-d', $time);
    }
  	$this->adm = $adm;
    $this->presentationConfig = new PresentationConfig($adm);
    // Slides holen
    $this->getPresentationSlides();
    $keys = array_keys($this->slides);
    $this->slide = $keys[$this->curSlideNr];
    $this->seconds = $this->slides[$this->slide];
  }

  /*
   * Gibt die Anzahl Sekunden zurück, die der aktuelle Slide angezeigt werden sollte.
   */
  function getSeconds() {
    return $this->seconds;
  }

  function showSlides() {
    // Slides anzeigen
    $captions = $this->getSlideCaptions();
    echo ' <ul class="nav navbar-nav">';
    foreach($captions as $key => $caption) {
      $class = "";
      if ($this->curSlideNr == $key) {
        $class = "active";
      }
      echo "<li class=\"$class\"><a>$caption</a></li>";
    }
    echo '</ul>';
  }

  /*
   * Erstellt eine Präsentation und gibt sie sofort aus.
   */
  function present() {
    // je nach slide wird etwas anderes angezeigt
    $command = explode(".", $this->slide);
    switch ($command[0]) {
      case "Gruppe":
        $t = new c_tabelle($this->adm);
        echo $t->printRanking($command[1], false);
        break;
      case "Spiele":
        $r = new Resultate($this->adm);
        echo $r->showGameView(15);
        break;
      case 'Responsibility':
      	$resp = $this->presentationConfig->getDayResponsibilityFor($this->day);
      	echo $this->adm->getTemplates()->render('presentation/responsibilitySlide', ['resp' => $resp]);
      	break;
      case 'Dates':
        echo $this->adm->getTemplates()->render('presentation/datesSlide');
        break;
    }
  }

  /*
   * Sammelt anhand der letzten eingegebenen Spiele die Slides, die angezeigt
   * werden sollen. Gespeichert werden diese Informationen in $slides[].
   */
  function getPresentationSlides() {
    // Slides initialisieren wenn nicht vorhanden oder am Ende angelangt.
    if ($_SESSION['slides'] && $_SESSION['curSlide'] < count($_SESSION['slides'])-1) {
      $this->slides = $_SESSION['slides'];
      $_SESSION['curSlide']++;
    } else {
      $this->getPresentationSlidesFromDB();
      $this->slides['Dates'] = 15;
      $_SESSION['slides'] = $this->slides;
      $_SESSION['curSlide'] = 0;
    }

    $this->curSlideNr = $_SESSION['curSlide'];
  }

  /*
   * Holt die Slies anhand der letzten eingegebenen Spiele von heute von der DB.
   */
  function getPresentationSlidesFromDB() {
    $stmt = $this->adm->prepareStatement('SELECT * FROM spiel WHERE NOT ISNULL(resultatA) AND NOT ISNULL(resultatB) AND tag = :now ORDER BY tag desc, zeit desc LIMIT 0,20;');
    $stmt->execute(array(':now' => $this->day));

    // Für jede Gruppe eine Tabelle hinzufügen
    while ($row = $stmt->fetch()) {
      // Anzahl Sekunden von der DB holen
      $gruppe = $row['gruppeID'];
      $s = $this->getTeamsFromGroup($gruppe);
      $this->slides["Gruppe." . $row['gruppeID']] = $s * 3;
    }
    $stmt->closeCursor();

    // Die aktuellen Spiele einfügen
    $this->slides["Spiele"] = 18;
    
    // Tagesverantwortliche/r falls vorhanden
    if ($dayResponsibility = $this->presentationConfig->getDayResponsibilityFor($this->day)) {
    	$this->slides['Responsibility'] = 10;
    }

  }


  /*
   * Holt die Anzahl Teams einer Gruppe von der Datenbank.
   */
  function getTeamsFromGroup($groupID) {
    $sql = "SELECT Count(ID) as count FROM teamgruppe t where gruppeID = :groupID;";

    $stmt = $this->adm->prepareStatement($sql);
    $stmt->execute(array(':groupID' => $groupID));

    $row = $stmt->fetch();
    $stmt->closeCursor();

    return $row['count'];
  }

  function getSlideCaptions() {
    $ret = array();
    foreach (array_keys($this->slides) as $key) {
      $command = explode(".", $key);
      switch ($command[0]) {
        case "Gruppe":
        	$sql = "SELECT gruppe FROM gruppe WHERE ID=:gruppeID;";
        	$stmt = $this->adm->prepareStatement($sql);
        	$stmt->execute(array('gruppeID' => $command[1]));
        	if ($row = $stmt->fetch()) {
          	$ret[] = mb_convert_encoding($row['gruppe'], 'UTF-8');
        	}
          break;
        case "Spiele":
          $ret[] = "Aktuelle Spiele";
          break;
        case 'Dates':
          $ret[] = "Meisterschaftstermine '23/24";
          break;
        case "Responsibility":
        	$ret[] = "Tagesverantwortung";
        	break;
      }
    }
    return $ret;
  }
}

?>