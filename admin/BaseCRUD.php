<?php
//Provides base CRUD operations

class BaseCRUD {

	protected $adm;

	function __construct(IVKAdmin $adm) {
		$this->adm = $adm;
	}

	var $title;

	//SQL Select, Update und Insert
	//Alle Befehle und Definitionen stehen immer in einem bestimmten Kontext.
	//(z.B. "user", "owner", "admin")
	var $sqlSel;    //SQL SELECT
	var $sqlUpd;    //SQL UPDATE
	var $sqlIns;    //SQL INSERT
	var $fldSel;    //Felder f�r SELECT
	var $fldUpd;    //Felder f�r UPDATE
	var $fldIns;    //Felder f�r INSERT
	var $descSel;   //Beschriftung f�r SELECT
	var $descUpd;   //Beschriftung f�r UPDATE
	var $descIns;   //Beschriftung f�r INSERT

	var $required;  //"muss" Felder

	var $where;     //Alf�llige WHERE-Klausel f�r die Show-Funktion
	var $order;     //ORDER BY

	var $key;       //PrimaryKey der Tabelle

	var $fieldtype;  //Array mit dem Datentyp des Feldes (check, radio, textarea, password, text, combo)
	var $fieldinfo;  //Informationen zu den entsprechenden Felddypen:
	/*
	radio:    datasource: SQL SELECT Statement, das im ersten Feld den Wert und im
	zweiten Feld den anzuzeigenden String enth�lt.
	oder
	Array mit Werten und anzuzeigenden Strings
	textarea: rows: anzahl Zeilen
	cols: anzahl Spalten
	combo:    datasource: SQL SELECT Statement, das im ersten Feld den Wert und im
	zweiten Feld den anzuzeigenden String enth�lt.
	*/


	/*******************
	 Tabelle ausgeben
	*******************/
	function show($context) {

		$sql = $this->sqlSel[$context];
		if (isset($this->where)) {
			$sql = "$sql WHERE $this->where";
		}

		if (isset($this->order)) {
			$sql = "$sql ORDER BY $this->order";
		}

		$stmt = $this->prepareStatement($sql);
		$this->executeStatement($stmt);

		//Kopf ausgeben
		$str = '';
		if ($this->title) {
			$str .= "<h1>$this->title</h1>\n";
		}
		$str .= '<table class="table table-striped table-condensed">';
		$str .= "<tr>";
		foreach($this->descSel[$context] as $desc) {
			$str .= "<th>$desc</th>";
		}
		$str .= "</tr>\n";

		//Tabelle durchlaufen
		while($row = $stmt->fetch()) {
			//$row durch eine Benutzerdefinierte Funktion laufen lassen
			$row = $this->row_finalize($row, $context);
			$str .= "<tr>";
			foreach($this->fldSel[$context] as $field) {
				$str .= "<td>$row[$field]</td>";
			}
			$str .= "</tr>\n";
		}

		$str .= "</table>\n";

		$stmt->closeCursor();

		return $str;
	}

	/***************************
	 Funktion zum �berschreiben in der Instanz. Zeilen k�nnen in der Instanz vor dem Ausgeben noch bearbeitet werden.
	***************************/

	function row_finalize($row, $context) {
		return $row;
	}

	/***********************
	 WHERE-Klausel eingeben
	***********************/
	function where($getWhere) {
		if ($getWhere!="") {
			$this->where = $getWhere;
		} else {
			unset($this->where);
		}
	}

	/***********************
	 ORDER BY-Klausel eingeben
	***********************/
	function order($getOrder) {
		if ($getOrder!="") {
			$this->order = $getOrder;
		} else {
			unset($this->order);
		}
	}

	/***********************
	 Add-Formular ausgeben
	***********************/
	function addForm($context, $target) {
		$str  = "<form action=\"$target\" method=\"POST\" name=\"BaseCRUDForm\">\n";
		$field_id=0;
		foreach($this->descIns[$context] as $desc) {
			$str .= '<div class="form-group">';
			$field = $this->fldIns[$context][$field_id];
			
			//Ist das Feld zwingend?
			if (is_array($this->required) and in_array($field, $this->required)) {
				$desc .= " <font color=\"red\">*</font>\n";
			}
				
			$str .= "<label for=\"$field\">$desc</label>";

			if (!array_key_exists($field, $this->fieldtype)) {
				$this->fieldtype[$field] = 'text';
			}

			//Feld gem�ss Feldtyp ausgeben
			switch ($this->fieldtype[$field]) {

				case "check":
					$str .= "<input class=\"form-control\" type=\"checkbox\" name=\"data[$field]\">";
					break;

				case "password":
					$str .= "<input class=\"form-control\" type=\"password\" name=\"data[$field]\">";
					break;

				case "radio":

					if (is_array($this->fieldinfo[$field]['datasource'])) {
						foreach($this->fieldinfo[$field]['datasource'] as $value => $label) {
							$str .= "<input class=\"form-control\" type=\"radio\" name=\"data[$field]\" value=\"$value\">$label<br>\n";
						}
					} else {
						$stmt = $this->prepareStatement($this->fieldinfo[$field]['datasource']);
						$this->executeStatement($stmt);
						while ($row = $stmt->fetch()) {
							$str .= "<input class=\"form-control\" type=\"radio\" name=\"data[$field]\" value=\"$row[0]\">$row[1]<br>\n";
						}
						$stmt->closeCursor();
					}
					break;

				case "combo":
					$str .= "<select class=\"form-control\" name=\"data[$field]\">\n";
					foreach($this->fieldinfo[$field]['datasource'] as $value => $label) {
						$str .= "<option value=\"$value\">$label</option>\n";
					}
					$str .= "</select>\n";
					break;

				case "textarea":
					$str .= "<textarea class=\"form-control\" name=\"data[$field]\" rows=\"".$this->fieldinfo[$field][rows]."\" cols=\"".$this->fieldinfo[$field][cols]."\"></textarea>";
					break;

				case "text":
				default:
					$str .= "<input class=\"form-control\" type=\"text\" name=\"data[$field]\">";
			}

			$field_id++;
			$str .= '</div>';
		}

		if (is_array($this->required)) {
			$str .= "<font color=\"red\">* = zwingend</font><br>";
		}

		$str .= "<a class=\"btn btn-primary\" href=\"#top\" onClick=\"javascript:BaseCRUDForm.submit();\">Speichern</a>\n";

		$str .= "</form>";

		return $str;
	} //End function addForm

	/*****************************
	 fügt Daten an die Tabelle an
	*****************************/
	function parseAddForm($context, $data) {
		// check mandatory fields
		$formIsValid = true;
		if (is_array($this->required)) {
			$i=0;
			foreach($data as $field => $value) {
				if (in_array($field, $this->required)) {
					if ($data[$field]=="") {
						$str .= "Die Eingabe von <b>" .$this->descIns[$context][$i]. "</b> ist zwingend erforderlich!<br>\n";
						$formIsValid = false;
					}
				}
				$i++;
			}
		}
		if (!$formIsValid) {
			echo "<p>$str<br>\n";
			echo "<a href=\"javascript:history.back();\">Zur&uuml;ck</a></p>\n";
			return false;
		}

		//SQL mit Daten f�llen
		$insert = $this->sqlIns[$context];
		$params = array();
		foreach($data as $field => $value) {
			$params[':' . $field] = $value;
		}

		//Datensatz anlegen
		$stmt = $this->prepareStatement($insert);
		$this->executeStatement($stmt, $params);
		return $this->adm->pdodb->lastInsertId();
	}


	/************************
	 Edit-Formular ausgeben
	************************/
	function editForm($context, $target, $primarykey) {

		$sql = $this->sqlUpd[$context] . " WHERE $this->key=:key;";
		$stmt = $this->prepareStatement($sql);
		$this->executeStatement($stmt, array('key' => $primarykey));
		$edit = $stmt->fetch();

		if ($this->title) {
			$str = "<h1>$this->title bearbeiten</h1>";
		}
		$str .= "<form action=\"$target\" method=\"POST\" name=\"BaseCRUDForm\">";
		foreach($this->descUpd[$context] as $desc) {
			$str .= '<div class="form-group">';

			list($key, $field) = each($this->fldUpd[$context]);
			
			//Ist das Feld zwingend?
			if (is_array($this->required) and in_array($field, $this->required)) {
				$desc .= " <font color=\"red\">*</font>\n";
			}
			
			$str .= "<label for=\"$field\">$desc</label>";
			
			if (!array_key_exists($field, $this->fieldtype)) {
				$this->fieldtype[$field] = 'text';
			}

			//Feld gemäss Feldtyp ausgeben
			switch ($this->fieldtype[$field]) {

				case "check":
					$str .= "<input class=\"form-control\" type=\"checkbox\" name=\"data[$field]\" id=\"$field\" ";
					if ($edit[$field]==1) {
						$str .= " checked>\n";
					} else {
						$str .= ">\n";
					}
					break;

				case "password":
					$str .= "<input class=\"form-control\" type=\"password\" name=\"data[$field]\" value=\"$edit[$field]\" id=\"$field\">";
					break;

				case "radio":

					foreach($this->fieldinfo[$field]['datasource'] as $value => $label) {
						$str .= "<input type=\"radio\" name=\"data[$field]\" value=\"$value\" id=\"$field\" ";
						if ($edit[$field]==$value) {
							$str .= " checked>";
						} else {
							$str .= ">";
						}
						$str .= "$label<br>\n";
					}

					$str .= "</select>\n";
					break;

				case "combo":
					$str .= "<select class=\"form-control\" name=\"data[$field]\">\n";
					foreach($this->fieldinfo[$field]['datasource'] as $value => $label) {
						$str .= "<option value=\"$value\"";
						if ($edit[$field]==$value) {
							$str .= " selected";
						}
						$str .= ">$label</option>\n";
					}

					$str .= "</select>\n";
					break;

				case "textarea":
					$str .= "<textarea class=\"form-control\" name=\"data[$field]\" rows=\"".$this->fieldinfo[$field][rows]."\" cols=\"".$this->fieldinfo[$field][cols]."\">$edit[$field]</textarea>";
					break;

				case "text":
				default:
					$str .= "<input class=\"form-control\" class=\"form-control\" type=\"text\" name=\"data[$field]\" value=\"$edit[$field]\" id=\"$field\">";
			}

			$str .= '</div>';
		}

		if (is_array($this->required)) {
			$str .= "<font color=\"red\">* = zwingend</font><br>\n";
		}

		//PrimaryKey mitgeben
		$str .= "<input type=\"hidden\" name=\"data[key]\" value=\"$primarykey\">\n";
		$str .= "<a class=\"btn btn-primary\" href=\"#top\" onClick=\"javascript:BaseCRUDForm.submit();\">Speichern</a>\n";


		$str .= "</form>\n";

		return $str;
	} //End function editForm

	/******************************
	 �ndert die eingegebenen Daten
	*******************************/
	function parseEditForm($context, $data) {
		$update = $this->updUpd[$context] . ' WHERE ' . $this->key . " = :key;";
		$params = array();
		foreach($data as $field => $value) {
			$params[':' . $field] = $value;
		}

		$stmt = $this->prepareStatement($update);
		$this->executeStatement($stmt, $params);

		return $this->adm->getConnection()->lastInsertId();
	}

	function getConnection() {
		return $this->adm->getConnection();
	}
	
	function prepareStatement($statement) {
		return $this->adm->prepareStatement($statement);
	}
	
	function executeStatement(PDOStatement $stmt, array $params = array()) {
		$this->adm->executeStatement($stmt, $params);
	}
	
	function getTemplates() {
		return $this->adm->getTemplates();
	}
}
//End Class Data

?>
