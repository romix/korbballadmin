<?php

/**
 * Returns games in a two dimensional array. The form of the array is int => array(column => value)
 *
 * @param String $tag
 * @param int $gruppe
 * @param int $runde
 * @param int $team
 * @return two dimensional array or games
 */
function getGames($tag, $gruppe, $runde, $team, $order, IVKAdmin $adm) {
  $tab = new c_tabelle($adm);
  
  //SQL für Spiele
  $sql = " SELECT spiel.ID, tag, zeit, halle, gruppe, runde, teamA, teamB, tA.mannschaft AS txtTeamA, tB.mannschaft AS txtTeamB, resultatA, resultatB, punkteA, punkteB
      FROM spiel
      LEFT JOIN halle ON spiel.halleID=halle.ID
      LEFT JOIN runde ON spiel.rundeID=runde.ID
      LEFT JOIN gruppe ON spiel.gruppeID=gruppe.ID
      LEFT JOIN mannschaft AS tA ON spiel.teamA=tA.ID
      LEFT JOIN mannschaft AS tB ON spiel.teamB=tB.ID ";

  $where = "";
  $sqlArgs = array();
  
  if ($tag != "") {
    $where .= "tag=:tag AND ";
    $sqlArgs['tag'] = $tag;
  }
  if ($gruppe != -1) {
    $where .= "gruppe.ID=:gruppe AND ";
    $sqlArgs[':gruppe'] = $gruppe;
  }
  if ($runde != -1) {
    $where .= "runde.ID=:runde AND ";
    $sqlArgs[':runde'] = $runde;
  }
  if ($team != -1) {
    $where .= "(spiel.teamA=:team OR spiel.teamB=:team) AND ";
    $sqlArgs[':team'] = $team;
  }
  $where .= "1=1";


  if ($order == "hall") {
  	$sql .= "WHERE " . $where . " ORDER BY tag, halle.ID, zeit";
  } else {
  	$sql .= "WHERE " . $where . " ORDER BY tag, zeit, halle.ID";
  }

  $stmt = $adm->prepareStatement($sql);
  $adm->executeStatement($stmt, $sqlArgs);

  //rows füllen
  $rows = array();
  while($row = $stmt->fetch()) {
    if (strpos($row['teamA'], ".")) {
      $arrTeamCode = $tab->arrTeamCode($row['teamA']);
      $row['txtTeamA'] = $arrTeamCode['team'] ?? '' . " (" . $arrTeamCode['txt'] . ")";
    }
    if (strpos($row['teamB'], ".")) {
      $arrTeamCode = $tab->arrTeamCode($row['teamB']);
      $row['txtTeamB'] = $arrTeamCode['team'] ?? '' . " (" . $arrTeamCode['txt'] . ")";
    }
	  if ($row['resultatA'] == null) {
    	$row['resultatA'] = '';
    }
    if ($row['resultatB'] == null) {
    	$row['resultatB'] = '';
    }
    if ($row['punkteA'] == null) {
    	$row['punkteA'] = '';
    }
    if ($row['punkteB'] == null) {
    	$row['punkteB'] = '';
    }
    $rows[] = $row;
  }

  return $rows;
}
?>