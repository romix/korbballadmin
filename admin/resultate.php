<?php

class Resultate {
	
	private $adm;

	function Resultate(IVKAdmin $adm) {
		$this->adm = $adm;
	}
  /*
  * Zeigt die letzten Spiele und die aktuellen Spiele auf einen Blick an
  */
  function showGameView($countRecentGames) {
    $str  = '<div class="col-md-6">';
    $str .= $this->getRecentGames($countRecentGames);
    $str .= "</div>";
    $str .= '<div class="col-md-6">';
    $str .= $this->getCurrentGames($countRecentGames);
    $str .= "</div>";
    
    return $str;
  }
  
  /*
  * Zeigt die letzten $count Resultate in einer Tabelle an.
  */
  function getRecentGames($count) {
    $time = time();
    $now = date('Y-m-d', $time);
    $sql = "SELECT spiel.ID, tag, zeit, halle, gruppe, runde, teamA, teamB, tA.mannschaft AS txtTeamA, tB.mannschaft AS txtTeamB, resultatA, resultatB
      FROM spiel
      LEFT JOIN halle ON spiel.halleID=halle.ID
      LEFT JOIN runde ON spiel.rundeID=runde.ID
      LEFT JOIN gruppe ON spiel.gruppeID=gruppe.ID
      LEFT JOIN mannschaft AS tA ON spiel.teamA=tA.ID
      LEFT JOIN mannschaft AS tB ON spiel.teamB=tB.ID
      WHERE NOT ISNULL(resultatA) AND NOT ISNULL(resultatB) AND tag = :tag ORDER BY tag desc, zeit desc;";
    
    $recentGamesStmt = $this->adm->prepareStatement($sql);
    $recentGamesStmt->execute(array(':tag' => $now));
    $recentGamesStmt->rowCount();
		
		// Titel
		$str = "<h3>Die letzten Resultate:</h3><br>\n";
		
		$str .= $this->getGameTable($recentGamesStmt, true, $count);

		$recentGamesStmt->closeCursor();
		
		return $str;
  }
  
  
  
  
  /*
  * gibt die aktuellen Spiele zur�ck
  */
  function getCurrentGames($count) {
    $time = time();
    $now = date('Y-m-d', $time);
    $sql = "SELECT spiel.ID, tag, zeit, halle, gruppe, runde, teamA, teamB, tA.mannschaft AS txtTeamA, tB.mannschaft AS txtTeamB, resultatA, resultatB
      FROM spiel
      LEFT JOIN halle ON spiel.halleID=halle.ID
      LEFT JOIN runde ON spiel.rundeID=runde.ID
      LEFT JOIN gruppe ON spiel.gruppeID=gruppe.ID
      LEFT JOIN mannschaft AS tA ON spiel.teamA=tA.ID
      LEFT JOIN mannschaft AS tB ON spiel.teamB=tB.ID
      WHERE tag = :tag AND ISNULL(resultatA) AND ISNULL(resultatB) ORDER BY zeit, halle";
    
    $currentGamesStmt = $this->adm->prepareStatement($sql);
    $currentGamesStmt->execute(array(':tag' => $now));
		
		// Titel
		$str = "<h3>Die aktuellen Spiele:</h3><br>\n";
		
		$str .= $this->getGameTable($currentGamesStmt, false, $count);

		$currentGamesStmt->closeCursor();
		
		return $str;
  }
  
  
  
  /*
  * Gibt eine Tabelle mit Spielen zur�ck anhand des �bergebenen SQL-Results
  */
  function getGameTable($gamesStmt, $withGameResults = true, $count) {
    $tab = new c_tabelle($this->adm);  
  
    //Tabelle
    $str = '<table class="table table-condensed table-bordered table-striped">';

    //Kopfzeile
    $str .= "<thead><tr><th>Zeit</th><th>Halle</th><th>Gruppe</th><th>Mannschaft 1</th><th>Mannschaft 2</th>";
    if ($withGameResults) {
    	$str .= "<th>Resultat</th>";
    }
    $str .= "</tr></thead>\n";
    
    //Tabelle f�llen
    $str .= '<tbody>';
    $counter = 0;
    while ($row = $gamesStmt->fetch()) {
      //Zeile
      $str .= "<tr>\n";

      //Daten
      $time = substr($row['zeit'], 0, 5);
	    $str .= "<td>$time</td>";
	    
	    $str .= '<td>' . $row['halle'] . "</td>\n";
	    
      $str .= "<td>" . $row['gruppe'] . "</td>\n";
      if (!strpos($row['teamA'], ".")) {
        $str .= "<td>" . $row['txtTeamA'] . "</td>\n";
      } else {
        $arrTeamCode = $tab->arrTeamCode($row['teamA']);
        $str .= "<td>" . $arrTeamCode['txt'] . "<br>" . $arrTeamCode['team'] . "</td>\n";
      }
      if (!strpos($row['teamB'], ".")) {
        $str .= "<td>" . $row['txtTeamB'] . "</td>\n";
      } else {
        $arrTeamCode = $tab->arrTeamCode($row['teamB']);
        $str .= "<td>" . $arrTeamCode['txt'] . "<br>" . $arrTeamCode['team'] . "</td>\n";
      }
      if ($withGameResults) {
      	$str .= "<td align=center>" . $row['resultatA'] . ' : ' . $row['resultatB'] . "</td>\n";
      }

      $str .= "</tr>\n";
      $counter++;
      if ($counter >= $count) {
      	break;
      }
    }

    //Tabelle beenden
    $str .= "</tbody></table>\n";
    
    return mb_convert_encoding($str, 'UTF-8');
  }
}

?>
