<?php

class ExtraPunkte {
	
	/**
	 * @var IVKAdmin
	 */
	private $admin;
	
	/**
	 * @var int
	 */
	private $group;
	
	/**
	 * @var c_tabelle
	 */
	private $tabelle;
	
	/**
	 * @param IVKAdmin $admin
	 * @param integer $group
	 */
	public function ExtraPunkte(IVKAdmin $admin, $group) {
		$this->admin = $admin;
		$this->group = $group;
		$this->tabelle = new c_tabelle($admin);
	}
	
	/**
	 * @return string
	 */
	public function showExtraPointTable() {
		$gruppeStmt = $this->admin->prepareStatement('SELECT g.gruppe FROM gruppe g WHERE ID = :gruppeID');
		$gruppeStmt->execute(array('gruppeID' => $this->group));
		$gruppeName = $gruppeStmt->fetch()['gruppe'];
		$str .= '<div class="row">';
		$str .= '<div class="col-md-12">';
		$str .= "<h1>Extrapunkte für Gruppe $gruppeName</h1>";
		$str .= '<p>Extrapunkte können Strafpunkte sein oder Punkte, die eine Mannschaft aus einer vorherigen Runde mitnimmt. Die Punkte werden den
				entsprechenden Teams dazugezählt oder abgezogen. In jedem Fall ist es möglich, einen Grund anzugeben.
				Dieser Grund wird dann bei den Resultaten als Bemerkung ausgegeben.</p>';
		$str .= '</div>';
		$str .= '</div>';
		
		$str .= '<div class="row">';
		$str .= '<div class="col-md-12">';
		$str .= '<div style="display: table">';
		$str .= '<div class="tr"><div class="th">Team</div><div class="th">Punkte (+/-)</div><div class="th">Grund</div><div class="th"></div></div>';
		
		$stmt = $this->admin->prepareStatement('SELECT ID, teamID, extrapunkte, grund FROM gruppe_extrapunkte WHERE gruppeID = :gruppeID;');
		$stmt->execute(array('gruppeID' => $this->group));
		while ($row = $stmt->fetch()) {
			$str .= '<form class="tr" action="?action=delete_extrapoints&id=' . $row['ID'] . '" method="post">';
			$str .= '<div class="td">';
			$teamCode = $this->tabelle->arrTeamCode($row['teamID']);
			$str .= $teamCode['txt'];
			if (array_key_exists('team', $teamCode)) {
				$str .= ' (' . $teamCode['team'] . ')';
			}
			$str .= '</div>';
			$str .= '<div class="td">' . $row['extrapunkte'] . '</div>';
			$str .= '<div class="td">' . $row['grund'] . '</div>';
			$str .= '<div class="td">';
			$str .= '<input type="hidden" name="gruppeID" value="' . $this->group . '"</input>';
			$str .= "<button type='submit' class='btn btn-default btn-xs'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span> Löschen</button>";
			$str .= '</form>';
			$str .= '</div>';
		}
		
		$str .= $this->addRow();

		$str .= '</div>';
		$str .= '</div>';
		$str .= '</div>';
		
		return $str;
	}
	
	public function addExtrapunkte($gruppeId, $teamId, $extrapunkte, $grund) {
		$stmt = $this->admin->prepareStatement('INSERT INTO gruppe_extrapunkte SET teamID=:teamID, extrapunkte=:extrapunkte, grund=:grund, gruppeID=:gruppeID;');
		$stmt->execute(array(
				'teamID' => $teamId,
				'extrapunkte' => $extrapunkte,
				'grund' => $grund,
				'gruppeID' => $gruppeId
		));
	}
	
	public function deleteExtrapunkte($ID) {
		$stmt = $this->admin->prepareStatement('DELETE FROM gruppe_extrapunkte WHERE ID = :ID');
		$stmt->execute(array('ID' => $ID));
	}
	
	private function addRow() {
		$str = '<form  class="tr" name="extrapunkte_new" action="?action=add_extrapoints" method="post">';
		$str .= '<div class="td">';
		$str .= '<input type="hidden" name="gruppeID" value="' . $this->group . '"</input>';
		$str .= '<select id="teamID" name="teamID" class="form-control">';
		$str .= '<option value="-1">Mannschaft wählen...</option>';
		$teams = $this->getTeams();
		foreach ($teams as $key => $team) {
			$str .= '<option value="' . $key . '">';
			$str .= $team['txt'];
			$str .= '</option>';
		}
		$str .= '</select>';
		$str .= '</div>';
		
		$str .= '<div class="td"><input class="form-control" type="number" name="extrapunkte" id="extrapunkte" min="-10" max="10" placeholder="Anzahl Punkte"></div>';
		$str .= '<div class="td"><input class="form-control" type="text" name="grund" id="grund" placeholder="Grund des Abzugs / Zuschlags"></div>';
		$str .= '<div class="td"><button type="submit" id="submit" name="submit" disabled class="btn btn-primary">hinzufügen</button></div>';
		$str .= '</form>';
		return $str;
	}
	
	private function getTeams() {
		$teams = array();
		$stmt = $this->admin->prepareStatement("SELECT * FROM teamgruppe WHERE gruppeID=:gruppeID");
		$stmt->execute(array('gruppeID' => $this->group));
		while ($row = $stmt->fetch()) {
			if ($row['teamCode']) {
				$teamCode = $row['teamCode'];
			} else {
				$teamCode = $row['teamID'];
			}
			$teams[$teamCode] = $this->tabelle->arrTeamCode($teamCode);
		}
		return $teams;
	}
}