<?php

namespace admin\traits;

trait ColorTrait {
  function stringToColorCode($str)
  {
    $code = dechex(crc32($str));
    $code = substr($code, 0, 6);
    return $code;
  }

  function contrastColor($code)
  {
    $R = hexdec(substr($code, 0, 2));
    $G = hexdec(substr($code, 2, 2));
    $B = hexdec(substr($code, 4, 2));
    if ($this->brightDiff($R, $G, $B, 0, 0, 0) > $this->brightDiff($R, $G, $B, 255, 255, 255)) {
      return "000000";
    } else {
      return "FFFFFF";
    }
  }

  function brightDiff($R1, $G1, $B1, $R2, $G2, $B2)
  {
    $BR1 = (299 * $R1 + 587 * $G1 + 114 * $B1) / 1000;
    $BR2 = (299 * $R2 + 587 * $G2 + 114 * $B2) / 1000;

    return abs($BR1 - $BR2);
  }
}