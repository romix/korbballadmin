<?php
define('__ROOT__', dirname(__FILE__));
require __DIR__ . '/vendor/autoload.php';
require_once __ROOT__ . '/BaseCRUD.php';
require_once __ROOT__ . '/team.php';
require_once __ROOT__ . '/gruppe.php';
require_once __ROOT__ . '/teamgruppe.php';
require_once __ROOT__ . '/halle.php';
require_once __ROOT__ . '/template.php';
require_once __ROOT__ . '/runde.php';
require_once __ROOT__ . '/spiel.php';
require_once __ROOT__ . '/explorer.php';
require_once __ROOT__ . '/tabelle.php';
require_once __ROOT__ . '/resultat.php';
require_once __ROOT__ . '/rangliste.php';
require_once __ROOT__ . '/SaveAnmeldung.php';
require_once __ROOT__ . '/anmeldungen.php';
require_once __ROOT__ . '/user.php';
require_once __ROOT__ . '/explorerFetchGames.php';
require_once __ROOT__ . '/medien.php';
require_once __ROOT__ . '/compatibility.php';
require_once __ROOT__ . '/AuthorisationException.php';
require_once __ROOT__ . '/AuthentificationException.php';
require_once __ROOT__ . '/extrapunkte.php';
require_once __ROOT__ . '/PresentationConfig.php';
require_once __ROOT__ . '/StartList.php';
require_once __ROOT__ . '/ScheduleController.php';

class IVKAdmin
{

  //Variablen
  public $tournamentName;
  public $host;
  public $db;
  public $user;
  public $pw;
  public $loginInfo = null;
  /**
   * @var PDO
   */
  public $pdodb;
  public $saison;
  public $lastSaison;
  public $anmeldungCC;
  public $anmeldungFROM;
  public $templates;

  //Konstruktor
  public function __construct()
  {

    // read configuration
    include "configuration_Default.php";
    include "configuration.php";

    //PDO-DB --> Diese connection verwenden, um injection zu vermeiden!
    $dsn = "mysql:host=$this->host;dbname=$this->db";
    $this->pdodb = new PDO($dsn, $this->user, $this->pw, array(
      PDO::MYSQL_ATTR_INIT_COMMAND => "SET sql_mode='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION', NAMES utf8;",
    ));
  }

  public function login($login, $password)
  {
    global $_SESSION;
    $this->loginInfo = null;
    if ($login) {
      $stmt = $this->prepareStatement("SELECT ID AS UserID, name, vorname, email as login, passwortHash as loginpw, rights FROM user WHERE email=:email;");
      $this->executeStatement($stmt, array(':email' => $login));
      if ($stmt->rowCount() > 0) {
        //benutzer ok
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if (password_verify($password, $row['loginpw'])) {
          //login ok
          $this->loginInfo = $row;
          $_SESSION['login'] = $this->loginInfo;
        } else {
          $this->logout();
        }
      } else {
        $this->logout();
      }
    }
  }

  public function loginUsingSession()
  {
    $this->loginInfo = get('login', $_SESSION);
  }

  public function logout()
  {
    $this->loginInfo = null;
    unset($_SESSION['login']);
    unset($_SESSION['loginpw']);
  }

  //Login Informationen
  public function getLoginInfo()
  {
    return $this->loginInfo;
  }

  public function loginRequired()
  {
    if (!$this->isLoggedIn()) {
      throw new AuthentificationException();
    }
  }

  public function isLoggedIn()
  {
    if (is_array($this->loginInfo)) {
      return $this->loginInfo['UserID'] != '';
    } else {
      return false;
    }
  }

  public function adminRequired()
  {
    if (!$this->isAdmin()) {
      throw new AuthorisationException();
    }
  }

  public function isAdmin()
  {
    return ($this->loginInfo['rights'] ?? false) == 'adm';
  }

  //Navigation auf der linken Seite
  public function navigation()
  {
    echo '<ul class="nav nav-sidebar">
				<li><a href="index.php?action=explorer" up-target=".main">Spielplan durchsuchen</a></li>';

    if ($this->isAdmin()) {
      echo '
				<li><a href="index.php?action=print_medien" up-target=".main">Medien</a></li>
			  <li><a href="index.php?action=resultat" up-target=".main">Resultate eingeben</a></li>
			  <li><a href="index.php?action=print_gruppen" up-target=".main">Gruppen</a></li>
				<li><a href="index.php?action=print_runden" up-target=".main">Runden</a></li>
				<li><a href="index.php?action=print_halle" up-target=".main">Hallen</a></li>
				<li><a href="index.php?action=print_template" up-target=".main">Spiel-Schl&uuml;ssel</a></li>
				<li><a href="index.php?action=print_move_day" up-target=".main">Spieltag verschieben</a></li>
				<li><a href="index.php?action=print_angemeldet" up-target=".main">Anmeldungen</a></li>
        <li><a href="index.php?action=print_startlist" up-target=".main">Startliste</a></li>
				<li><a href="index.php?action=print_teams" up-target=".main">Mannschaften</a></li>
				<li><a href="index.php?action=print_create_schedule" up-target=".main">Spielplan erstellen</a></li>
				<li><a href="index.php?action=print_presentation_config" up-target=".main">Präsentations Modus</a></li>';
    }

    echo '<li><a href="index.php?action=print_results" up-target=".main">Resultate</a></li>
      		  <li><a href="index.php?action=show_license" up-target=".main">Lizenz</a></li>
      </ul></div>';
  } //Ende Navigation

  public function showLogin()
  {
    if ($this->isLoggedIn()) {
      ?>
      <ul class="nav navbar-nav navbar-right">
        <li>
          <a><?php echo "Angemeldet als: <i>" . $this->loginInfo['name'] . " " . $this->loginInfo['vorname'] . "</i>"; ?></a>
        </li>
        <li><a href="index.php?action=logout">Abmelden</a></li>
      </ul>
      <?php
    } else {
      ?>
      <form class="navbar-form navbar-right" action="index.php?action=login" name="login" method="POST">
        <input type="text" class="form-control" name="formlogin" size="16" placeholder="Benutzername">
        <input type="password" class="form-control" name="formloginpw" size="16" placeholder="Passwort">
        <button type="submit" class="btn btn-primary">Login</button>
        <a class="btn" href="index.php?action=register">Registrieren</a>
      </form>
      <?php
    }
  } //Ende showLogin()

  //Ende der HTML-Datei ausgeben
  public function fuss()
  {
    include "fuss.php";
  }

  public function getConnection()
  {
    return $this->pdodb;
  }

  public function prepareStatement($statement)
  {
    return $this->getConnection()->prepare($statement);
  }

  public function executeStatement(PDOStatement $stmt, array $params = array())
  {
    $success = $stmt->execute($params);
    if (!$success) {
      $msg = $stmt->errorInfo();
      throw new Exception($msg[2]);
    }
  }

  public function getTemplates()
  {
    if (!$this->templates) {
      $this->templates = new League\Plates\Engine('./templates/');
      $this->templates->addData(['adm' => $this]);
    }
    return $this->templates;
  }
}

//n�tzliche Funktionen

//Datum vom Benutzer in MySQL-Datum umwandeln
function dat_u2m($datum)
{
  if ($datum == null) {
    return false;
  }
  $day = substr($datum, 0, strpos($datum, "."));
  $month = substr($datum, strpos($datum, ".") + 1, strrpos($datum, ".") - strpos($datum, ".") - 1);
  $year = substr($datum, strrpos($datum, ".") + 1, 4);
  if (strlen($year) == 2) {
    $year = "20" . $year;
  }
  if (checkdate($month, $day, $year)) {
    return "$year-$month-$day";
  } else {
    return false;
  }
}

//Datum vom MySQL-Datum in Benutzer-Datum umwandeln
function dat_m2u($datum)
{
  $day = substr($datum, 8, 2);
  $month = substr($datum, 5, 2);
  $year = substr($datum, 0, 4);
  return "$day.$month.$year";
}

function timeadd($time, $add)
{
  $th = substr($time, 0, 2);
  $tm = substr($time, 3, 2);
  $ts = substr($time, 6, 2);

  $ah = substr($add, 0, 2);
  $am = substr($add, 3, 2);
  $as = substr($add, 6, 2);

  return date("H:i:s", mktime($th + $ah, $tm + $am, $ts + $as, 1, 1, 1970));
}

function timestamp($time)
{
  $th = substr($time, 0, 2);
  $tm = substr($time, 3, 2);
  $ts = substr($time, 6, 2);
  return mktime($th, $tm, $ts);
}

function get($needle, $haystack, $return = null)
{
  return array_key_exists($needle, $haystack) ? $haystack[$needle] : $return;
}

?>