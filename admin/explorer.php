<?php

class c_explorer
{

  private $adm;

  function c_explorer(IVKAdmin $adm)
  {
    $this->adm = $adm;
  }

  function version()
  {
    return "2";
  }

  //Gebe String mit Selektionskriterien aus
  function selection($target, $data = "", $encoding = 'UTF-8')
  {
    $str = '';
    
    $str .= '<script>';
    $str .= 'function submitOk(id) {';
    $str .= '	if (id == "inputGruppe") {';
    $str .= '		document.getElementById("inputMannschaft").value = -1;';
    $str .= '		document.getElementById("inputRunde").value = -1;';
    $str .= '	}';
    $str .= '	if(id == "inputRunde") {';
    $str .= '		document.getElementById("inputMannschaft").value = -1;';
    $str .= '	}';
    $str .= '	document.getElementById("explorer").submit();';
    $str .= '}';
    $str .= '</script>';

    //Variablen für Standardwerte im Formular (vorher ausgewählte Kriterien)
    if (is_array($data)) {
      $tag = $data['tag'];
      $display = $data['display'];
      $gruppe = $data['gruppe'];
      $runde = $data['runde'];
      $team = $data['team'];
      $order = $data['order'];
    }

    $hallChecked = '';
    $timeChecked = '';
    if ($order == "hall") {
      $hallChecked = "checked";
    } else {
      $timeChecked = "checked";
    }

    $matrixChecked = '';
    $listChecked = '';
    if ($display == "matrix") {
      $matrixChecked = "checked";
    } else {
      $listChecked = "checked";
    }

    $pdo = $this->adm->pdodb;

    //Formular beginnen
    $str .= '<h1 class="hidden-print">Spielplan durchsuchen</h1>';
    $str .= '<h1 class="visible-print-block">' . $this->adm->tournamentName . '</h1>';
    $str .= "<form name=\"selection\" id=\"explorer\" action=\"$target\" class=\"hidden-print\" method=\"GET\">\n";

    // Tag
    $str .= '<div class="form-group">';
    $str .= '<label for="inputTag">Tag</label>';
    $str .= "<input class=\"form-control\" type=\"text\" id=\"inputTag\" name=\"data[tag]\" value=\"$tag\" size=10 placeholder=\"tt.mm.yyyy\">";
    $str .= '</div>';

    // Gruppe
    $str .= '<div class="form-group">';
    $str .= '<label for="inputGruppe">Gruppe</label>';
    $str .= "\n<select class=\"form-control\" id=\"inputGruppe\" name=\"data[gruppe]\" onchange=\"submitOk('inputGruppe')\">\n";
    $str .= "<option value=\"-1\">alle</option>\n";
    $groups = $pdo->prepare("
        SELECT gruppe.ID AS ID, liga, gruppe 
        FROM gruppe 
        LEFT JOIN liga ON gruppe.ligaID=liga.ID 
        ORDER BY liga.liga, gruppe.gruppe;
     ");
    $groups->execute(array());
    while ($row = $groups->fetch(PDO::FETCH_ASSOC)) {
      $grp = $row['gruppe'];
      if ($gruppe == $row['ID']) {
        $str .= "<option value=\"$row[ID]\" selected>$grp</option>\n";
      } else {
        $str .= "<option value=\"$row[ID]\">$grp</option>\n";
      }
    }
    $str .= "</select>\n";
    $str .= '</div>';

    // Runde
    $str .= '<div class="form-group">';
    $str .= '<label for="inputRunde">Runde</label>';
    $str .= "\n<select id=\"inputRunde\" class=\"form-control\" name=\"data[runde]\" onchange=\"submitOk('inputRunde')\">\n";
    $str .= "<option value=\"-1\">alle</option>\n";

    $rounds = $pdo->prepare("
        SELECT runde.ID AS ID, liga, gruppe, runde 
        FROM gruppe 
        LEFT JOIN liga ON gruppe.ligaID=liga.ID 
        LEFT JOIN runde ON runde.gruppeID = gruppe.ID 
        WHERE gruppe.ID = :gruppeID OR :gruppeID = -1
        ORDER BY liga.liga, gruppe.gruppe, runde.ID;
    ");
    $rounds->execute(array('gruppeID' => $gruppe));
    while ($row = $rounds->fetch(PDO::FETCH_ASSOC)) {
      $text = $row['gruppe'] . ' : ' . $row['runde'];
      if ($runde == $row['ID']) {
        $str .= "<option value=\"$row[ID]\" selected>$text</option>\n";
      } else {
        $str .= "<option value=\"$row[ID]\">$text</option>\n";
      }
    }
    $str .= "</select>\n";
    $str .= '</div>';

    // Mannschaft
    $str .= '<div class="form-group">';
    $str .= '<label for="inputMannschaft">Mannschaft</label>';
    $str .= "\n<select id=\"inputMannschaft\" class=\"form-control\" name=\"data[team]\" onchange=\"submitOk()\">\n";
    $str .= "<option value=\"-1\">alle</option>\n";
    $teams = $pdo->prepare("
        SELECT mannschaft.ID, mannschaft.mannschaft, gruppe, liga
        FROM mannschaft 
        LEFT JOIN teamgruppe ON teamgruppe.teamID=mannschaft.ID 
        LEFT JOIN gruppe ON teamgruppe.gruppeID=gruppe.ID
        LEFT JOIN liga ON mannschaft.ligaID=liga.ID
        WHERE liga.ID > 0  AND (gruppe.ID = :gruppeID OR :gruppeID = -1) AND (exists(SELECT * FROM runde WHERE runde.gruppeID = teamgruppe.gruppeID AND runde.ID = :rundeID) OR :rundeID = -1)
        ORDER BY liga, gruppe, mannschaft;
    ");
    $teams->execute(array('rundeID' => $runde, 'gruppeID' => $gruppe));
    while ($row = $teams->fetch(PDO::FETCH_ASSOC)) {
      $text = "$row[gruppe] : $row[mannschaft]";
      if ($team == $row['ID']) {
        $str .= "<option value=\"$row[ID]\" selected>$text</option>\n";
      } else {
        $str .= "<option value=\"$row[ID]\">$text</option>\n";
      }
    }
    $str .= "</select>\n";
    $str .= '</div>';

    // Sortierung
    $str .= '<label>Sortierung</label>';
    $str .= '<div class="radio-inline">';
    $str .= '<label class="radio-inline">';
    $str .= "<input type=\"radio\" name=\"data[order]\" $timeChecked value=\"time\" onchange=\"submitOk('inputGruppe')\"> Zeit";
    $str .= '</label>';
    $str .= '<label class="radio-inline">';
    $str .= "<input type=\"radio\" name=\"data[order]\" $hallChecked value=\"hall\" onchange=\"submitOk('inputGruppe')\"> Halle";
    $str .= '</label>';
    $str .= '</div>';

    // Darstellung
    $str .= '<br/><label>Darstellung</label>';
    $str .= '<div class="radio-inline">';
    $str .= '<label class="radio-inline">';
    $str .= "<input type=\"radio\" name=\"data[display]\" $listChecked value=\"list\" onchange=\"submitOk('inputGruppe')\"> Liste (detailliert)<br/>";
    $str .= '</label>';
    if ($tag != null) {
      $str .= '<label class="radio-inline">';
      $str .= "<input type=\"radio\" name=\"data[display]\" $matrixChecked value=\"matrix\" onchange=\"submitOk('inputGruppe')\"> Matrix";
      $str .= '</label>';
    }
    $str .= '</div>';

    //Formular beenden
    $str .= '<input type="hidden" name="action" value="explorer"/>' . "\n";
    $str .= '<input type="hidden" name="show" value="1"/>' . "\n";
    $str .= "</form>\n";

    return $str;
  }

  // Spielplan ausgeben
  function show($data, $encoding = 'UTF-8')
  {
    $tab = new c_tabelle($this->adm);

    if ($data['tag']) {
      $data['tag'] = dat_u2m($data['tag']);
    }

    if ($data['display'] == "matrix") {
      return $this->tagesansicht($data);
    }

    $games = getGames($data['tag'], $data['gruppe'], $data['runde'], $data['team'], $data['order'], $this->adm);

    $str = '<div class="row hidden-print">';
    $str .= '<div class="col-md-6">Gefundene Spiele: ' . count($games) . ', Version: ' . $this->version() . '</div>';
    $str .= '<div class="col-md-6" style="text-align: right">';

    // Export Links
    $str .= "<a class=\"btn btn-success\" href=\"IcalExport.php?tag=" . $data['tag'] . "&gruppe=$data[gruppe]&runde=$data[runde]&team=$data[team]\"><span class=\"glyphicon glyphicon-cloud-download\"></span> iCalendar Export</a> ";
    $str .= "<a class=\"btn btn-success\" href=\"ExcelExport.php?tag=" . $data['tag'] . "&gruppe=$data[gruppe]&runde=$data[runde]&team=$data[team]\"><span class=\"glyphicon glyphicon-cloud-download\"></span> Excel Export</a><br/>";
    $str .= '</div>';
    $str .= '</div><br>';

    //Tabelle
    $str .= "<table class=\"table test\">\n";

    //Kopfzeile
    $str .= "<tr><th>Tag</th><th>Zeit</th><th>Halle</th><th>Gruppe</th><th>Mannschaft 1</th><th>Mannschaft 2</th><th>Resultat</th></tr>\n";

    //Tabelle f�llen
    $tagVorher = null;
    for ($i = 0; $i < count($games); $i++) {
      $row = $games[$i];

      //Halle nicht umbrechen
      $row['halle'] = str_replace(" ", "&nbsp;", $row['halle']);

      //Zeile
      $str .= "<tr>\n";

      //Daten

      if ($tagVorher != $row['tag']) {
        $str .= "<td><b>" . dat_m2u($row['tag']);
        $str .= "</b></td>\n";
      } else {
        $str .= "<td></td>\n";
      }
      $tagVorher = $row['tag'];

      $str .= "<td><strong>" . substr($row['zeit'], 0, -3) . "</strong></td>\n";
      $str .= "<td>$row[halle]</td>\n";

      $str .= "<td>$row[gruppe]</td>\n";
      $str .= "<td>$row[txtTeamA]</td>\n";
      $str .= "<td>$row[txtTeamB]</td>\n";
      $str .= "<td align=center>$row[resultatA] : $row[resultatB]</td>\n";

      $str .= "</tr>\n";
    }

    //Tabelle beenden
    $str .= "</table>\n";

    //Formular beenden
    $str .= "</form>\n";

    return $str;
  }

  //gibt Tagesansicht in Matrix-Form aus
  function tagesansicht($data, $encoding = 'UTF-8')
  {
    $str = '';
    $tab = new c_tabelle($this->adm);

    //Tabelle Hallen �ffnen
    $hallen = $this->adm->pdodb->query("SELECT * FROM halle ORDER BY ID;");

    $anzHallen = $hallen->rowCount();

    //Alle verschiedenen Zeiten suchen
    $zeitStmt = $this->adm->pdodb->prepare("SELECT zeit FROM spiel WHERE tag = :tag GROUP BY zeit ORDER BY zeit;");
    $zeitStmt->execute(array(':tag' => $data['tag']));

    /*
     Vorgehen :
     Durch alle Hallen gehen (Spalten)
     Spiel zur jeweiligen Zeit und Halle suchen
     Spiel ausgeben
     */

    //Version ausgeben
    $str .= "<p><b>Tagesansicht f&uuml;r den " . dat_m2u($data['tag']);
    $str .= "<br><i>Version: " . $this->version() . "</i></b></p>\n";

    $str .= "<table class=\"table\">\n<tr>\n<td>Zeit</td>\n";

    foreach ($hallen as $rHalle) {
      $arrHalle[] = $rHalle['ID'];
      $str .= "<td>$rHalle[halle]</td>\n";
    }

    while ($rZeit = $zeitStmt->fetch()) {
      $str .= "<tr>";
      $str .= "<td>$rZeit[zeit]</td>\n";
      foreach ($arrHalle as $halle) {
        $str .= "<td>\n";

        //SQL für die Spielbeschreibung
        $stmt = $this->adm->prepareStatement("SELECT teamA, teamB, gruppe.gruppe FROM spiel LEFT JOIN gruppe ON gruppe.ID=spiel.gruppeID
          WHERE zeit=:zeit and tag=:tag and halleID=:halle;");
        $stmt->execute(array(
          ':zeit' => $rZeit['zeit'],
          ':tag' => $data['tag'],
          ':halle' => $halle
        ));

        $anz = 0;
        while ($rSpiel = $stmt->fetch()) {
          //Encoding
          foreach ($rSpiel as $key => $value) {
            $rSpiel[$key] = $value;
          }
          $anz++;
          $arrTeamCodeA = $tab->arrTeamCode($rSpiel['teamA']);
          $arrTeamCodeB = $tab->arrTeamCode($rSpiel['teamB']);
          if ($anz == 1) {
            $str .= "<a>$rSpiel[gruppe]</a>\n";
          } else {
            $aTxt = $arrTeamCodeA['txt'];
            $bTxt = $arrTeamCodeB['txt'];
            $str .= "<font color=\"red\">$rSpiel[gruppe]<br>$aTxt : $bTxt</font>\n";
          }
        }

        $str .= "</td>\n";
      } //ende foreach (halle)

      $str .= "</tr>\n";
    } //ende while (zeit)

    $str .= "</table>\n";
    return $str;


  } // Ende Funktion show


  //Datum vom Benutzer in MySQL-Datum umwandeln
  function dat_u2m($datum)
  {
    $day = substr($datum, 0, strpos($datum, "."));
    $month = substr($datum, strpos($datum, ".") + 1, strrpos($datum, ".") - strpos($datum, ".") - 1);
    $year = substr($datum, strrpos($datum, ".") + 1, 4);
    if (strlen($year) == 2) {
      $year = "20" . $year;
    }
    if (checkdate($month, $day, $year)) {
      return "$year-$month-$day";
    } else {
      return false;
    }
  }

  //Datum vom MySQL-Datum in Benutzer-Datum umwandeln
  function dat_m2u($datum)
  {
    $day = substr($datum, 8, 2);
    $month = substr($datum, 5, 2);
    $year = substr($datum, 0, 4);
    return "$day.$month.$year";
  }

} // Ende Klasse explorer

?>
