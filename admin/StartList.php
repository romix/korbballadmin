<?php

class StartList {

	private $adm;
	
	const CRAZY_SQL = 
	'select gruppeName, teamName, tag, min(zeit) ersteZeit, team from (
		(
			select teamA as team, tag, zeit, mannschaft as teamName, gruppe.gruppe as gruppeName
			from spiel
			left join mannschaft on spiel.teamA = mannschaft.ID
			left join gruppe on spiel.gruppeID = gruppe.ID
		)
		union 
		(
			select teamB as team, tag, zeit, mannschaft as teamName, gruppe.gruppe as gruppeName
			from spiel 
			left join mannschaft on spiel.teamB = mannschaft.ID
			left join gruppe on spiel.gruppeID = gruppe.ID
		)
	) t
	group by gruppeName, teamName, team, tag
	order by tag, ersteZeit, gruppeName, teamName';

	function __construct(IVKAdmin $adm) {
		$this->adm = $adm;
	}
	
	function showStartList() {
		$stmt = $this->adm->prepareStatement(StartList::CRAZY_SQL);
		$stmt->execute();
		$startPerDayList = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$data = array();
		$teams = array();
		foreach ($startPerDayList as $start) {
			$teams[$start['team']] = true;
			$data[] = $start;
		}
		
		return $this->adm->getTemplates()->render('startList', [
				'data' => $data
		]);
	}
}