<?php
include "lib.php";
//Admin-Objekt erstellen
$adm = new IVKAdmin();

$tag = $_GET['tag'];
$gruppe = $_GET['gruppe'];
$runde = $_GET['runde'];
$team = $_GET['team'];

header("Content-Type: text/csv");
header("Content-Disposition: attachment; filename=spiele.csv");

$dtstamp_date = new DateTime("now");
$dtstamp = $dtstamp_date->format("Ymd\THis\Z");

// CSV Headers
echo "Tag; Zeit; Halle; Gruppe; Mannschaft 1; Mannschaft 2; Resultat\n";

$games = getGames($tag, $gruppe, $runde, $team, 'time', $adm);

for ($i = 0; $i < count($games); $i++) {
  $game = $games[$i];
  echo dat_m2u($game['tag']) . "; ";
  echo $game['zeit'] . "; ";
  echo $game['halle'] . "; ";
  echo $game['gruppe'] . "; ";
  echo $game['txtTeamA'] . "; ";
  echo $game['txtTeamB'] . "; ";
  echo $game['resultatA'] . " : " . $game['resultatB'] . "\r\n";
}
?>
