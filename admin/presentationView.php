<?php
/**********************************
 Bibliotheken laden und  pr�fen
***********************************/
include "lib.php";
include "presentation.php";
include "resultate.php";

session_start();

// datum einstellungen
setlocale(LC_ALL, "de_DE");
putenv( "PHP_TZ=Europe/Berlin" );

//Admin-Objekt erstellen
$adm = new IVKAdmin();

// create presentation
$p = new Presentation($adm);
?>

<!DOCTYPE html>
<html>
<head>
<title>Turnverband Luzern, Ob- und Nidwalden</title>
<meta name="generator" content="Bluefish 1.0.7">
<link href="css/content.css" rel="stylesheet" media="screen">
<script src="presentationView.js" type="text/javascript"></script>
<meta name="description" content="Turnverband Luzern, Ob- und Nidwalden">
<meta name="author" content="Roman Schaller">
<style type="text/css">
<!--
html,body {
  height: 100%;
  margin: 0px;
}

a.presentation {
  border: 1px dotted #27147d;
  color: #000000;
  background-color: #e1edff;
  margin: 2px;
  padding: 1px 10px 1px 10px;
}

a.currentpresentation {
  border: 1px dotted #27147d;
  color: #000000;
  background-color: #748ca8;
  margin: 2px;
  padding: 1px 10px 1px 10px;
}

div.presentation_content {
  height: 85%;
}

td.team_header,td.group_name {
  vertical-align: middle;
  font-weight: bold;
}

#pg {
  text-align: center;
  width: 100%;
}
-->
</style>
</head>

<body bgcolor="#FFFFFF" onload="makeProgress(<?php echo $p->getSeconds() ?>);">
	<progress id="pg" value="0" max="100"></progress>
	<br />
	<br />
	<?php
	$p->showSlides();
	?>
	<br />
	<br />
	<div class="presentation_content">
		<center>
			<?php
			$p->present();
			?>
		</center>
	</div>
	<div>
		<br>
		<h1 style="text-align: center;">Willkommen an der Innerschweizer Korbball-Meisterschaft!</h1>
	</div>
</body>
</html>
