<?php

use admin\ScheduleController;

header('Content-type: text/html; charset=utf-8');

//Data aus POST lesen
$data = array();
if (array_key_exists('data', $_POST)) {
  $data = $_POST['data'];
}

//Session-Handling
session_name();
session_cache_expire(30);
session_start();

include "lib.php";

$adm = new IVKAdmin();

//Action aus GET lesen
$action = get('action', $_GET);

//login Handling
if ($action == "login") {
  $adm->login(get('formlogin', $_POST), get('formloginpw', $_POST));
} else {
  $adm->loginUsingSession();
}

//logout Handling
if ($action == "logout") {
  $adm->logout();
}

$loginInfo = $adm->getLoginInfo();
$template = $adm->getTemplates()->make('layout');
$template->data(['adm' => $adm]);

//Wenn keine Argumente übergeben wurden, zeigen wir die Startseite an.
if (count($_GET) == 0) {
  $content = "<h3>Willkommen auf dem Resultate-Dienst der Interverbands-Komission</h3>
  <h3>Bitte w&auml;hle auf der linken Seite eine Option aus.</h3>";
  $template->data(['content' => $content]);
}

//Bei erfolgreichem Login wird ein Begrüssungstext und seine Objekte angezeigt.
if ($action == "login") {
  if ($loginInfo['login']) {
    $content = '<h3>Willkommen ' . $loginInfo['name'] . ' ' . $loginInfo['vorname'] . "</h3>\n";
    $template->data(['content' => $content]);
  } else {
    $content = "<h3>Benutzername oder Passwort ist falsch</h3>\n";
    $template->data(['content' => $content]);
  }
  $action = null;
}

if ($action == "register") {
  $user = new User($adm);
  $template->data(['content' => $adm->getTemplates()->render('userregistration/register', [])]);
}

if ($action == 'register2') {
  $user = new User($adm);
  $content = $user->registerNewUser($_POST['vorname'], $_POST['name'], $_POST['email'], $_POST['password']);
  $template->data(['content' => $content]);
}

try {

  // **********  TEAMS  **************
  //neues Team speichern
  if ($action == "add_team") {
    $adm->adminRequired();
    $t = new c_team($adm);
    $result = $t->parseAddForm("new", $data);
    if ($result) {
      header("HTTP/1.1 303 See Other");
      header("Location: ?action=print_teams");
    }
  }

  //Team ändern
  if ($action == "edit_team") {
    $adm->adminRequired();
    $t = new c_team($adm);
    $t->parseEditForm("edit", $data);
    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_teams");
  }

  // Teams anzeigen
  if ($action == "print_teams") {
    $adm->loginRequired();
    $t = new c_team($adm);
    $t->order = "liga.ID, gruppe.gruppe, verein.vereinort";
    if ($adm->isAdmin()) {
      $content = '<a class="btn btn-primary" href="index.php?action=print_add_team">Mannschaft hinzufügen</a>' . $t->show("admin");
      $template->data(['content' => $content]);
    } else {
      $template->data(['content' => $t->show("pub")]);
    }
  }

  // Tema hinzuf�gen
  if ($action == "print_add_team") {
    $adm->loginRequired();
    $content = "<h3>Neue Mannschaft registrieren</h3>";
    $t = new c_team($adm);
    $t->user($loginInfo['UserID']); //Auswahlen auf User beschränken
    $content .= $t->addForm("new", "index.php?action=add_team");
    $template->data(['content' => $content]);
  }


  //Team bearbeiten anzeigen
  if ($action == "print_edit_team") {
    $adm->adminRequired();
    $t = new c_team($adm);
    $t->user($loginInfo['UserID']);
    $template->data(['content' => $t->editForm("edit", "index.php?action=edit_team", $_GET['team'])]);
  }

  //Team löschen fragen
  if ($action == "print_delete_team") {
    $adm->adminRequired();
    $t = new c_team($adm);
    $t->user($loginInfo['UserID']);
    $content = $t->askDelete($_GET['team']);
    $template->data(['content' => $content]);
  }

  // Team löschen
  if ($action == 'delete_team') {
    $adm->adminRequired();
    $t = new c_team($adm);
    $t->user($loginInfo['UserID']);
    $t->delete($_POST['teamId']);
    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_teams");
  }

  /**
   * Spielplan Erstellen
   */
  if ($action === "print_create_schedule") {

    $adm->adminRequired();

    $scheduleController = new ScheduleController($adm);
    $scheduleController->index();
    exit();
  }
  /**
   * Ein Spiel verschieben
   */
  if ($action === "move_game") {
    $adm->adminRequired();

    $scheduleController = new ScheduleController($adm);
    $scheduleController->moveGame($_POST['gameId'], $_POST['targetDate'], $_POST['targetTime'], $_POST['targetHalle']);
    exit();
  }

  /**
   * Neue Spiele erstellen
   */
  if ($action === "add_games") {
    $adm->adminRequired();

    $scheduleController = new ScheduleController($adm);

    if(isset($_POST['submit']) && $_POST['submit'] === "submit" && $_SESSION['schedule']['ready']) {
      $scheduleController->insertRoundIntoDB();
    }

    $scheduleController->addGames(
      $_POST['group_id'] ?? null,
      $_POST['round'] ?? null,
      $_POST['template_id'] ?? null,
      $_POST['date'] ?? [],
      $_POST['start'] ?? [],
      $_POST['court'] ?? [],
      $_POST['move_team'] ?? []
    );


    exit();
  }

  /**
   * Ein Spiel in die Zwischenablage verschieben
   */
  if ($action === "move_game_to_clipboard") {
    $adm->adminRequired();

    $scheduleController = new ScheduleController($adm);
    $scheduleController->moveGameToClipboard($_POST['gameId']);
    exit();
  }


  /**********************
   * Administration
   **********************/

  //Gruppe hinzuf�gen anzeigen
  if ($action == "print_add_gruppe") {
    $adm->adminRequired();
    $content = "<h3>Neue Gruppe hinzuf&uuml;gen</h3>";
    $g = new Gruppe($adm);
    $content .= $g->addForm("new", "index.php?action=add_gruppe");
    $template->data(['content' => $content]);
  }

  //Neue Gruppe speichern
  if ($action == "add_gruppe") {
    $adm->adminRequired();
    $g = new Gruppe($adm);
    $result = $g->parseAddForm('new', $data);
    if ($result) {
      header("HTTP/1.1 303 See Other");
      header("Location: ?action=print_gruppen");
    }
  }

  //Gruppe l�schen anzeigen
  if ($action == "print_delete_gruppe") {
    $adm->adminRequired();
    $g = new Gruppe($adm);
    $template->data(['content' => $g->askDelete($_GET['gruppe'])]);
  }

  //Gruppe l�schen
  if ($action == "delete_gruppe") {
    $adm->adminRequired();
    $g = new Gruppe($adm);
    $g->delete($_GET['gruppe']);
    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_gruppen");
  }

  //Gruppe bearbeiten anzeigen
  if ($action == "print_edit_gruppe") {
    $adm->adminRequired();
    $g = new Gruppe($adm);
    $content = $g->editForm("edit", "index.php?action=edit_gruppe", $_GET['gruppe']);
    $template->data(['content' => $content]);
  }

  //Gruppe �ndern
  if ($action == "edit_gruppe") {
    $adm->adminRequired();
    $g = new Gruppe($adm);
    $g->parseEditForm("edit", $data);
    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_gruppen");
  }

  // Gruppen anzeigen
  if ($action == "print_gruppen") {
    $adm->adminRequired();
    $g = new Gruppe($adm);
    $g->order("liga, gruppe");
    $content = $g->show("admin");
    $content .= "<a class='btn btn-primary' href=\"index.php?action=print_add_gruppe\">Gruppe erstellen</a>\n";
    $template->data(['content' => $content]);
  }

  //TeamGruppe bearbeiten anzeigen
  if ($action == "print_edit_teamgruppe") {
    $adm->adminRequired();
    $tg = new c_teamgruppe($adm);
    $template->data(['content' => $tg->editForm($adm, "index.php?action=edit_teamgruppe", $_GET['gruppe'])]);
  }

  //TeamGruppe bearbeiten
  if ($action == "edit_teamgruppe") {
    $adm->adminRequired();
    $tg = new c_teamgruppe($adm);
    $tg->parseEditForm($_GET['direction'], $_POST['gruppe'], $_POST['in'], $_POST['out'], $data, $_POST['delete']);
    $content = $tg->editForm($adm, "index.php?action=edit_teamgruppe", $_POST['gruppe']);
    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_edit_teamgruppe&gruppe=" . $_POST['gruppe']);
  }

  // ***** Extra Punkte *****
  if ($action == 'print_extrapoints') {
    $adm->adminRequired();
    $extra = new ExtraPunkte($adm, intval($_GET['gruppe']));
    $content = $extra->showExtraPointTable();
    $template->data(['content' => $content]);
  }

  if ($action == 'add_extrapoints') {
    $adm->adminRequired();
    $extra = new ExtraPunkte($adm, intval($_POST['gruppeID']));
    $extra->addExtrapunkte($_POST['gruppeID'], $_POST['teamID'], $_POST['extrapunkte'], $_POST['grund']);
    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_extrapoints&gruppe=" . $_POST['gruppeID']);
  }

  if ($action == 'delete_extrapoints') {
    $adm->adminRequired();
    $extra = new ExtraPunkte($adm, intval($_POST['gruppeID']));
    $extra->deleteExtrapunkte($_GET['id']);
    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_extrapoints&gruppe=" . $_POST['gruppeID']);
  }

  //****  Hallen

  //Halle hinzuf�gen anzeigen
  if ($action == "print_add_halle") {
    $adm->adminRequired();
    $content = "<h3>Neue Halle hinzuf&uuml;gen</h3>";
    $h = new c_halle($adm);
    $content .= $h->addForm("new", "index.php?action=add_halle");
    $template->data(['content' => $content]);
  }

  //Neue Halle speichern
  if ($action == "add_halle") {
    $adm->adminRequired();
    $h = new c_halle($adm);
    $result = $h->parseAddForm("new", $data);
    unset($action);
    if ($result) {
      header("HTTP/1.1 303 See Other");
      header("Location: ?action=print_halle");
    }
  }

  //Halle l�schen anzeigen
  if ($action == "print_delete_halle") {
    $adm->adminRequired();
    $h = new c_halle($adm);
    $content = $h->askDelete($_GET['halle']);
    $template->data(['content' => $content]);
  }

  //Halle l�schen
  if ($action == "delete_halle") {
    $adm->adminRequired();
    $h = new c_halle($adm);
    $h->Delete($_GET['halle']);
    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_halle");
  }

  //Halle bearbeiten anzeigen
  if ($action == "print_edit_halle") {
    $adm->adminRequired();
    $h = new c_halle($adm);
    $content = $h->editForm("edit", "index.php?action=edit_halle", $_GET['halle']);
    $template->data(['content' => $content]);
  }

  //Halle �ndern
  if ($action == "edit_halle") {
    $adm->adminRequired();
    $h = new c_halle($adm);
    $h->parseEditForm("edit", $data);
    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_halle");
  }

  // Halle anzeigen
  if ($action == "print_halle") {
    $adm->adminRequired();
    $h = new c_halle($adm);
    $content = $h->show("admin");
    $content .= '<a class="btn btn-primary" href="index.php?action=print_add_halle">Halle erstellen</a>';
    $template->data(['content' => $content]);
  }

  //****  Spiel-Vorlagen

  //Vorlage hinzuf�gen anzeigen
  if ($action == "print_add_template") {
    $adm->adminRequired();
    $content = "<h3>Neue Spiel-Vorlage hinzuf&uuml;gen</h3>";
    $t = new c_template($adm);
    $content .= $t->addForm("new", "index.php?action=add_template");
    $template->data(['content' => $content]);
  }

  //Neue Vorlage speichern
  if ($action == "add_template") {
    $adm->adminRequired();
    $t = new c_template($adm);
    $result = $t->parseAddForm("new", $data);
    unset($action);
    if ($result) {

      header("HTTP/1.1 303 See Other");
      header("Location: ?action=print_template");
    }
  }

  //Vorlage l�schen anzeigen
  if ($action == "print_delete_template") {
    $adm->adminRequired();
    $t = new c_template($adm);
    $content = $t->askDelete($_GET['template']);
    $template->data(['content' => $content]);
  }

  //Vorlage l�schen
  if ($action == "delete_template") {
    $adm->adminRequired();
    $t = new c_template($adm);
    $t->Delete($_GET['template']);
    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_template");
  }

  //Vorlage bearbeiten anzeigen
  if ($action == "print_edit_template") {
    $adm->adminRequired();
    $t = new c_template($adm);
    $content = $t->editForm("edit", "index.php?action=edit_template", $_GET['template']);
    $template->data(['content' => $content]);
  }

  //Vorlage �ndern
  if ($action == "edit_template") {
    $adm->adminRequired();
    $t = new c_template($adm);
    $t->parseEditForm("edit", $data);
    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_template");
  }

  // Vorlage anzeigen
  if ($action == "print_template") {
    $adm->adminRequired();
    $t = new c_template($adm);
    $t->order = "template";
    $content = $t->show("admin");
    $content .= "<a class='btn btn-primary' href=\"index.php?action=print_add_template\">Spiel-Vorlage erstellen</a>\n";
    $template->data(['content' => $content]);
  }

  //********  Vorlagen-Matrix


  //Matrix-�nderungen speichern
  if ($action == "edit_temp_matrix") {
    $adm->adminRequired();
    $t = new c_template($adm);
    $t->ParseEditMatrix($_POST['template'], $_POST['teamA'], $_POST['teamB'], $_POST['newTeamA'], $_POST['newTeamB']);
    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_edit_temp_matrix&template=" . $_POST['template']);
  }

  //Matrix bearbeiten anzeigen
  if ($action == "print_edit_temp_matrix") {
    $adm->adminRequired();
    $t = new c_template($adm);
    $content = $t->editMatrix($_GET['template']);
    $content .= "<h3>Kontrolle:</h3>\n";
    $kontrolle = $t->checkMatrix($_GET['template']);
    if (!$kontrolle) {
      $content .= "<font color=\"darkGreen\">Scheint alles in Ordnung zu sein.</font>\n";
    } else {
      $content .= "<font color=\"red\">$kontrolle</font>\n";
    }
    $template->data(['content' => $content]);
  }

  //*************** Runden ***********

  //Runde hinzuf�gen anzeigen
  if ($action == "print_add_runde") {
    $adm->adminRequired();
    $content = "<h3>Neue Runde hinzuf&uuml;gen</h3>";
    $r = new c_runde($adm);
    $content .= $r->addForm("new", "index.php?action=add_runde");
    $template->data(['content' => $content]);
  }

  //Neue Runde speichern
  if ($action == "add_runde") {
    $adm->adminRequired();
    $r = new c_runde($adm);
    $result = $r->parseAddForm("new", $data);
    if ($result) {
      header("HTTP/1.1 303 See Other");
      header("Location: ?action=print_runden");
    }
  }

  //Runde l�schen anzeigen
  if ($action == "print_delete_runde") {
    $adm->adminRequired();
    $r = new c_runde($adm);
    $content = $r->askDelete($_GET['runde']);
    $template->data(['content' => $content]);
  }

  //Runde l�schen
  if ($action == "delete_runde") {
    $adm->adminRequired();
    $r = new c_runde($adm);
    $content = $r->Delete($_GET['runde']);
    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_runden");
  }

  //Spiele in Runde l�schen anzeigen
  if ($action == "print_delete_spiele") {
    $adm->adminRequired();
    $r = new c_runde($adm);
    $content = $r->askDeleteSpiel($_GET['runde']);
    $template->data(['content' => $content]);
  }

  //Runde löschen
  if ($action == "delete_spiele") {
    $adm->adminRequired();
    $r = new c_runde($adm);
    $r->DeleteSpiel($_GET['runde']);
    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_runden");
  }

  //Runde bearbeiten anzeigen
  if ($action == "print_edit_runde") {
    $adm->adminRequired();
    $r = new c_runde($adm);
    $content = $r->editForm("edit", "index.php?action=edit_runde", $_GET['runde']);
    $template->data(['content' => $content]);
  }

  //Runde ändern
  if ($action == "edit_runde") {
    $adm->adminRequired();
    $r = new c_runde($adm);
    $r->parseEditForm("edit", $data);
    "Runde wurde gespeichert.<br>\n";
    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_runden");
  }

  // Runde anzeigen
  if ($action == "print_runden") {
    $adm->adminRequired();
    $r = new c_runde($adm);
    $r->order("liga, gruppe, runde.ID");
    $content = $r->show("admin");
    $content .= "<a class='btn btn-primary' href=\"index.php?action=print_add_runde\">Runde erstellen</a>\n";
    $template->data(['content' => $content]);
  }

  //************ Move day **********

  if ($action == 'print_move_day') {
    $adm->adminRequired();
    $s = new c_spiel($adm);
    $content = $s->showMoveDay();
    $template->data(['content' => $content]);
  } else if ($action == 'do_move_day') {
    $adm->adminRequired();
    $s = new c_spiel($adm);
    $didMove = $s->moveDay($data);
    if ($didMove) {
      header("HTTP/1.1 303 See Other");
      header("Location: ?action=did_move_day&day=" . $data['day'] . "&dayto=" . $data['dayto']);
    } else {
      header("HTTP/1.1 303 See Other");
      header("Location: ?action=did_not_move_day&day=" . $data['day'] . "&dayto=" . $data['dayto']);
    }
  } else if ($action == 'did_move_day') {
    $adm->adminRequired();
    $s = new c_spiel($adm);
    $content = $s->didMoveDay($_GET['day'], $_GET['dayto']);
    $template->data(['content' => $content]);
  } else if ($action == 'did_not_move_day') {
    $adm->adminRequired();
    $s = new c_spiel($adm);
    $content = $s->didNotMoveDay($_GET['day'], $_GET['dayto']);
    $template->data(['content' => $content]);
  }


  //************ Spiele ************

  //Spiel bearbeiten anzeigen
  if ($action == "print_edit_spiel") {
    $adm->adminRequired();
    $s = new c_spiel($adm);
    $content = $s->editForm("edit", "index.php?action=edit_spiel&runde=" . $_GET['runde'], $_GET['spiel']);
    $template->data(['content' => $content]);
  }

  //Spiel �ndern
  if ($action == "edit_spiel") {
    $adm->adminRequired();
    $s = new c_spiel($adm);
    $s->parseEditForm("edit", $data);
    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_spiele&runde=" . $_GET['runde']);
  }

  //Spiele mit Vorlage hinzuf�gen 2
  if ($action == "add_spiele_runde2") {
    $adm->adminRequired();
    $s = new c_spiel($adm);
    $content = $s->addRunde2($data, "index.php?action=add_spiele_runde3&runde=" . $_GET['runde']);
    $template->data(['content' => $content]);
  }

  //Spiele mit Vorlage hinzuf�gen 3
  if ($action == "add_spiele_runde3") {
    $adm->adminRequired();
    $s = new c_spiel($adm);
    $content = $s->addRunde3($data, $_POST['halle'], $_POST['team'], $_POST['datum'], $_POST['beginn']);
    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_spiele&runde=" . $_GET['runde']);
  }

  //Spiele anzeigen
  if ($action == "print_spiele") {
    $adm->adminRequired();
    $s = new c_spiel($adm);
    $s->where("rundeID=" . $_GET['runde']);
    $s->order("tag, zeit, halle");
    $content = $s->show("admin");
    $content .= $s->addRunde1($_GET['runde'], "index.php?action=add_spiele_runde2&runde=" . $_GET['runde']);
    $template->data(['content' => $content]);
  }

  //*******  Explorer ************
  if ($action == "explorer") {
    $x = new c_explorer($adm);

    //handling falls data nicht exisitert
    if (isset($_GET['data'])) {
      $data = $_GET['data'];
    } else {
      $data['tag'] = null;
      $data['display'] = 'list';
      $data['gruppe'] = -1;
      $data['runde'] = -1;
      $data['team'] = -1;
      $data['order'] = -1;
    }
    $content = $x->selection("index.php", $data);

    //Suchergebnis anzeigen
    $content .= $x->show($data);
    $template->data(['content' => $content]);
  }

  //*******  Resultate ************
  if ($action == "resultat") {
    $adm->adminRequired();
    $x = new c_resultat($adm);

    //Spiel speichern
    if (isset($_GET['save']) && $_GET['save'] == '1') {
      $x->save($data, $_POST['ID']);
      header("HTTP/1.1 303 See Other");
      header("Location: ?action=resultat&show=1&tag=" . $_POST['tag'] . "&zeit=" . $_POST['zeit']);
    } else if ($_GET['show'] == '1') {
      $content = $x->show("index.php?action=resultat&show=1&save=1", $_GET['tag'], $_GET['zeit']);
      $template->data(['content' => $content]);
    } else {
      $content = $x->selection();
      $template->data(['content' => $content]);
    }

  }


  //*******  Medien  ************
  if ($action == "print_medien") {
    $adm->adminRequired();
    $m = new Medien($adm);

    //Suchergebnis anzeigen
    if (get('show', $_GET) == 1) {
      $content = $m->showMedien($adm->pdodb, $_GET['days']);
    } else {
      $content = $m->selectionMedien("index.php?action=print_medien&show=1");
    }
    $template->data(['content' => $content]);
  }

  //******** angemeldete Mannschaften *********
  if ($action == "print_angemeldet") {
    $adm->adminRequired();
    $a = new c_anmeldungen($adm);

    $saison = get('saison', $_GET);
    if ($saison != null) {
      $a->where("saison=\"" . $saison . "\"");
    }
    $a->interpretOrder();
    $content = $a->anmeldungenGruppiert();
    $content .= $a->showOrderBy($saison);
    $content .= $a->show("admin");
    $template->data(['content' => $content]);
  }

  if ($action == 'print_delete_registration') {
    $adm->adminRequired();
    $regId = $_GET['regId'];
    $a = new c_anmeldungen($adm);
    $content = $a->askDelete($regId);
    $template->data(['content' => $content]);
  }

  if ($action == 'delete_registration') {
    $adm->adminRequired();
    $a = new c_anmeldungen($adm);
    $regId = $_GET['regId'];
    $a->delete($regId);
    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_angemeldet");
  }

  if ($action == 'print_create_team_from_registration') {
    $adm->adminRequired();
    $regId = $_GET['regId'];
    $a = new c_anmeldungen($adm);
    $content = $a->createTeamForm($regId);
    $template->data(['content' => $content]);
  }

  if ($action == 'add_team_from_registration') {
    $adm->adminRequired();
    $regId = $_POST['regId'];
    $ligaId = $_POST['liga'];
    $vereinId = $_POST['verein'];
    $a = new c_anmeldungen($adm);
    $a->createTeam($regId, $ligaId, $vereinId);
    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_angemeldet");
  }

  // ********* Startliste *********
  if ($action == 'print_startlist') {
    $adm->adminRequired();
    $list = new StartList($adm);
    $content = $list->showStartlist();
    $template->data(['content' => $content]);
  }

  //*******  Resultate ************
  if ($action == "print_results") {
    $t = new c_tabelle($adm);
    $content = $t->printResultLinks();
    $content .= $t->printTableLinks();
    $template->data(['content' => $content]);
  }

  if ($action == "print_result") {
    $t = new c_tabelle($adm);
    $content = $t->printRankingSinglePageLink($_GET['gruppe']);
    $content .= $t->printRanking($_GET['gruppe']);
    $content .= $t->printResult($_GET['gruppe']);
    $template->data(['content' => $content]);
  }

  //*******  Präsentations-Modus ************
  if ($action == 'print_presentation_config') {
    $adm->adminRequired();
    $p = new PresentationConfig($adm);
    $content = $p->printPresentationConfig();
    $template->data(['content' => $content]);
  }
  if ($action == 'edit_presentation_responsible_config') {
    $adm->adminRequired();
    $p = new PresentationConfig($adm);
    $p->updateResponsible($_POST);
    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_presentation_config");
  }
  if ($action == 'upload_presentation_responsible_pic') {
    $adm->adminRequired();
    $p = new PresentationConfig($adm);
    $p->uploadPicture($_FILES);
    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_presentation_config");
  }
  if ($action == 'delete_presentation_responsible_pic') {
    $adm->adminRequired();
    $p = new PresentationConfig($adm);
    $p->deletePicture($_GET);
    header("HTTP/1.1 303 See Other");
    header("Location: ?action=print_presentation_config");
  }


  //*******  Lizenz ************
  if ($action == 'show_license') {
    $Lictemplate = $adm->getTemplates()->make('license');
    $content = $Lictemplate->render();
    $template->data(['content' => $content]);
  }

  //********************************

} catch (AuthentificationException $e) {
  $template->data(['content' => "Für diese Operation musst du angemeldet sein."]);
} catch (AuthorisationException $e) {
  $template->data(['content' => "Für diese Operation musst du über Admin-Rechte verfügen."]);
}

echo $template->render();