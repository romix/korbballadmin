<?php header('Cache-Control: no-cache'); ?>
<?php header('Content-type: text/html; charset=utf-8'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>Tabelle</title>
  <meta http-equiv="content-type" content="text/html;charset=utf-8">
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
  <link href="css/content.css" rel="stylesheet">
</head>
<body>
<div style="text-align: left">

<?php
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$ws_link = substr($actual_link, 0, strpos($actual_link, '/admin/')) . '/ws/tableV1.php?group=' . $_GET['gruppe'];
$jsonTable = json_decode(file_get_contents($ws_link), true);

function echoHeader($groupName, $teams) {
	echo '<thead><tr>';
	echo '<th colspan="2" class="single_table_group_header">' . $groupName . '</th>';
	foreach($teams as $team) {
		echo '<td><img src="teamImage.php?text=' . $team . '"></td>';
	}
	echo '</tr></thead>';
}

function echoResultLines($rounds, $teams, $games) {
	$countRound = count($rounds);
	foreach ($teams as $Kteam => $Vteam) {
		echo '<tr>';
		echo '<td style="vertical-align: middle;" rowspan="' . $countRound . '">' . $Vteam . '</td>';
		$firstRound = true;
		foreach ($rounds as $Kround => $Vround) {
			if (!$firstRound) {
				$firstRound = false;
				echo '<tr>';
			}
			echoResultLineByRound($Vround, $games[$Kround], $teams, $Kteam);
			echo '</tr>';
		}
	}
}

function echoResultLineByRound($roundName, $roundGames, $teams, $lineTeamKey) {
	echo '<td>' . $roundName . '</td>';
	foreach ($teams as $teamKey => $teamName) {
		if ($teamKey==$lineTeamKey) {
			echo '<td bgcolor="#353535"></td>';
		} else {
			echo '<td class="text-center">' . $roundGames[$lineTeamKey][$teamKey] . '</td>';
		}
	}
}

function echoRates($rates, $teams) {
	echo '<tr>';
	echo '<td colspan="2">K&ouml;rbe</td>';
	foreach ($teams as $teamKey => $teamName) {
		echo '<td class="text-center">' . $rates[$teamKey]['others'] . ' : ' . $rates[$teamKey]['own'] . '</td>';
	}
	echo '</tr>';
	echo '<tr>';
	echo '<td colspan="2">Korbverh&auml;ltnis</td>';
	foreach ($teams as $teamKey => $teamName) {
		echo '<td class="text-center">' . ($rates[$teamKey]['own'] - $rates[$teamKey]['others']) . '</td>';
	}
	echo '</tr>';
}

function echoPoints($points, $teams) {
	echo '<tr>';
	echo '<td colspan="2">Punkte</td>';
	foreach ($teams as $teamKey => $teamName) {
		echo '<td class="text-center">' . $points[$teamKey] . '</td>';
	}
	echo '</tr>';
}

function echoRankings($ranking, $teams) {
	echo '<tr class="active">';
	echo '<td colspan="2">Rang</td>';
	$rankPerTeam = array();
	foreach ($ranking as $rank => $teamKeys) {
		foreach ($teamKeys as $teamKey) {
			$rankPerTeam[$teamKey] = $rank + 1;
		}
	}
	foreach ($teams as $teamKey => $teamName) {
		echo '<th class="text-center">' . $rankPerTeam[$teamKey] . '</th>';
	}
	echo '</tr>';
}

function echoTable($table, $ranking, $points, $rates, $path) {
	$groupName = $table['groupName'];
	$rounds = $table['rounds'];
	$teams = $table['teams'];
	$games = $table['games'];
	echo '<div class="container-fluid" style="overflow: auto;">';
	echo '<div class="row">';
	echo '<table class="table table-condensed table-bordered single_table_embedded">';
	echoHeader($groupName, $teams);
	echo '<tbody>';
	echoResultLines($rounds, $teams, $games);
	echoRates($rates, $teams);
	echoPoints($points, $teams);
	echoRankings($ranking, $teams);
	echo '</tbody>';
	echo '</table>';
	echo "</div>";
	echo "</div>";
}

function echoGroups($groups, $path) {
	echo '<ul>';
	foreach ($groups as $groupId => $groupName) {
		echo "<li><a href=\"$path?group=$groupId\">$groupName</a></li>";
	}
	echo '</ul';
}

if (isset($jsonTable)) {
	$table = $jsonTable['table'];
	$ranking = $jsonTable['ranking'];
	$points = $jsonTable['points'];
	$rates = $jsonTable['rates'];
	echoTable($table, $ranking, $points, $rates, $path);
}

?>

</div>

</body>
</html>
