<?php

class c_resultat
{

  private $adm;

  function __construct(IVKAdmin $adm)
  {
    $this->adm = $adm;
  }

  //Gebe String mit Selektionskriterien zurück
  function selection() {
    $tag = date("d.m.Y", time());
    $zeit = date("H", time()) - 1 . ":00";
    

    $str = '<div class="col-md-4">';
    $str .= '<form name="selection" action="index.php" method="GET" up-target=".main">';
    $str .= '<input hidden name="action" value="resultat"/>';
    $str .= '<input hidden name="show" value="1"/>';
    $str .= '<h3>Resultate eingeben</h3>';
    $str .= '<div class="form-group">';
    $str .= '<label for="tag">Tag</label>';
    $str .= '<input type=text name="tag" value=' . $tag . ' class="form-control">';
    $str .= '</div>';

    $str .= '<div class="form-group">';
    $str .= '<label for="zeit">Zeit</label>';
    $str .= '<input type=text name="zeit" value=' . $zeit . ' class="form-control">';
    $str .= '</div>';

    $str .= '<button class="btn btn-primary" type="submit">Anzeigen</button>';

    $str .= "</form><br/>";
    $str .= '</div>';
    $str .= '<div class="col-md-8">';
    $str .= '</div>';

    return $str;
  }

  //Zeigt die ausgewählten Spiele an
  function show($target, $tag, $zeit) {

    //Dieses Objekt wird gebraucht, wenn wir Mannschaftsnamen herausfinden wollen
    $tab = new c_tabelle($this->adm);

    //Datum in MySQL-Datum wandeln
    $mysql_tag = dat_u2m($tag);

    $mysql_zeit = $zeit . ":00";

    //sql aufbereiten
    $sql = " SELECT spiel.ID, tag, zeit, halle, gruppe, runde, teamA, teamB, tA.mannschaft AS txtTeamA, tB.mannschaft AS txtTeamB, resultatA, resultatB
      FROM spiel
      LEFT JOIN halle ON spiel.halleID=halle.ID
      LEFT JOIN runde ON spiel.rundeID=runde.ID
      LEFT JOIN gruppe ON spiel.gruppeID=gruppe.ID
      LEFT JOIN mannschaft AS tA ON spiel.teamA=tA.ID
      LEFT JOIN mannschaft AS tB ON spiel.teamB=tB.ID ";

    $sql .= "WHERE spiel.tag=:tag AND spiel.zeit>=:zeit ";

    $sql .= "ORDER BY spiel.zeit, halle.halle;";

    //sql ausführen
    $stmt = $this->adm->prepareStatement($sql);
    $stmt->execute(array("tag" => $mysql_tag, "zeit" => $mysql_zeit));

    //Tabelle ausgeben
    $str = '<div class="col-md-12">';
    $str .= "<div class='table'>\n";
    $str .= "<div class='tr'><div class='th'>Zeit</div><div class='th'>Halle</div><div class='th'>Gruppe</div><div class='th'>Mannschaft A</div><div class='th'>Mannschaft B</div><div class='th'>Resultat A</div><div class='th'>Resultat B</div><div class='th'>Speichern</div></div>\n";

    //Tabelle füllen
    while ($row = $stmt->fetch()) {
      $formName = 'result-form-' . $row['ID'];
      $str .= '<form class="tr" id="' . $formName . '" name="' . $formName . '" action="' .$target.  '" method="POST" up-target="#' . $formName  . '" up-transition="move-to-left/move-from-top">';
      $str .= "<input type='hidden' name='tag' value='".$tag."'>\n";
      $str .= "<input type='hidden' name='zeit' value='".$zeit."'>\n";
      $str .= "<input type='hidden' name='ID' value='".$row['ID']."'>\n";
      $str .= "<div class='td'>" . substr($row['zeit'], 0, 5) . "</div>\n";
      $str .= "<div class='td'>".$row['halle']."</div>\n";
      $str .= "<div class='td'>".$row['gruppe']."</div>\n";
      //Team A
      if (!strpos($row['teamA'], ".")) {
        $str .= "<div class='td'>".$row['txtTeamA']."</div>\n";
      } else {
        $arrTeamCode = $tab->arrTeamCode($row[teamA]);
        $str .= "<div class='td'>" . $arrTeamCode['txt'] . "<br>" . $arrTeamCode['team'] . "</div>\n";
      }
      //Team B
      if (!strpos($row['teamB'], ".")) {
        $str .= "<div class='td'>".$row['txtTeamB']."</div>\n";
      } else {
        $arrTeamCode = $tab->arrTeamCode($row[teamB]);
        $str .= "<div class='td'>" . $arrTeamCode[txt] . "<br>" . $arrTeamCode[team] . "</div>\n";
      }
      //Resultat
      $str .= "<div class='td'><input type='text' name='data[resultatA]' value='$row[resultatA]' size='4'></div>\n";
      $str .= "<div class='td'><input type='text' name='data[resultatB]' value='$row[resultatB]' size='4'></div>\n";
      $str .= "<div class='td'><input type='submit' value='Speichern'></div>\n";
      $str .= "</form>\n";
    }

    //Tabelle beenden
    $str .= "</div>\n";

    return $str;

  }

  //Speichert ein Spiel
  function save($data, $ID)
  {
    //SQL zusammenstellen

    $resultatA = $data['resultatA'];
    $resultatB = $data['resultatB'];
    $punkteA = null;
    $punkteB = null;
    if ($resultatA == "" and $resultatB == "") {
      $resultatA = null;
      $resultatB = null;
    } else {
      if ($data['resultatA'] > $data['resultatB']) {
        $punkteA = 2;
        $punkteB = 0;
      } elseif ($data['resultatA'] < $data['resultatB']) {
        $punkteA = 0;
        $punkteB = 2;
      } elseif ($data['resultatA'] == $data['resultatB']) {
        $punkteA = 1;
        $punkteB = 1;
      }
    }

    $sql = "UPDATE spiel SET resultatA=:resultatA, resultatB=:resultatB, punkteA=:punkteA, punkteB=:punkteB WHERE spiel.ID=:id";
    $stmt = $this->adm->prepareStatement($sql);
    $this->adm->executeStatement($stmt, array(
      'id' => $ID,
      'resultatA' => $resultatA,
      'resultatB' => $resultatB,
      'punkteA' => $punkteA,
      'punkteB' => $punkteB
    ));
  }
}

?>
