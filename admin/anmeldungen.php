<?php

// This is for CRUD operations on registrations.

class c_anmeldungen extends BaseCRUD {

	var $katMap = array(
			'D' => 'Damen',
			'H' => 'Herren',
			'M' => 'Mixed',
	);

	var $registration;

	//Constructor
	function __construct(IVKAdmin $adm) {
		parent::__construct($adm);
		$this->title = "Angemeldete Mannschaften";

		$this->sqlSel['admin'] = "SELECT ID, saison, kategorie, malter, verein, mNummer, minGebDat, maxGebDat, liga, tenueShirt, tenueHose, ersatzShirt,
				ersatzHose, vName, vAdresse, vPLZOrt, vTel, vMail, mName, mAdresse, mPLZOrt, mTel, mMail, sFirstname, sLastname, sMail, 
				kaName, kaAdresse, kaPLZOrt, kaTel, kaMail,
				jName, jAdresse, jPLZOrt, jTel, jMail, bank, konflikt, verband, datum, mannschaftID FROM `anmeldung`";
		$this->fldSel['admin'] = array("saison", "kategorie", "malter", "verein", "mNummer", 'actions', "minGebDat", "maxGebDat", "liga", "tenueShirt", "tenueHose", "ersatzShirt",
				"ersatzHose", "vName", "vAdresse", "vPLZOrt", "vTel", "vMail", "mName", "mAdresse", "mPLZOrt", "mTel", "mMail", "sFirstname", "sLastname", "sMail",
				"kaName", "kaAdresse", "kaPLZOrt", "kaTel", "kaMail",
				"jName", "jAdresse", "jPLZOrt", "jTel", "jMail", "bank", "konflikt", "verband", "datum");
		$this->descSel['admin'] = array("Saison", "Kategorie", "Alter", "Verein", "Mannschafts-Nummer", 'Aktionen', "Jüngster", "Ältester", "Liga", "Tenue Shirt", "Tenue Hose", "Ersatz Shirt",
				"Ersatz Hose",
				"Vereinsverantw. Name", "Vereinsverantw. Adresse", "Vereinsverantw. PLZ / Ort", "Vereinsverantw. Tel", "Vereinsverantw. Mail",
				"Mannschaftsverantw. Name", "Mannschaftsverantw. Adresse", "Mannschaftsverantw. PLZOrt", "Mannschaftsverantw. Tel", "Mannschaftsverantw. Mail",
				"Schiedsrichter Vorname", "Schiedsrichter Nachname", "Schiedsrichter Mail",
				"Kassier Name", "Kassier Adresse", "Kassier PLZ/Ort", "Kassier Tel", "Kassier Mail",
				"Jugendverantw. Name", "Jugendverantw. Adresse", "Jugendverantw. PLZ / Ort", "Jugendverantw. Tel", "Jugendverantw. Mail",
				"Bank", "Bemerkungen", "Verband", "Anmeldedatum");
	}

	//Links einfügen
	function row_finalize($row, $context) {
		if ($context=='admin') {
			if ($row['mannschaftID'] == 0) {
				$row['actions'] = '<a href="index.php?action=print_delete_registration&regId=' . $row['ID'] . '"><img src="images/delete.svg" width=20 heigh=20 border=0 alt="löschen"></a> ';
				$row['actions'] .= '<a href="index.php?action=print_create_team_from_registration&regId=' . $row['ID'] . '"><img src="images/add.svg" width=20 heigh=20 border=0 alt="Team erstellen"></a> ';
			} else {
				$row['actions'] .= '<a href="index.php?action=print_edit_team&team=' . $row['mannschaftID'] . '">Mannschaft anzeigen</a>';
			}
			return $row;
		} else {
			return $row;
		}
	}

	//Gibt HTML zurück, das alle saisons enthält.
	function anmeldungenGruppiert() {
		$stmt = $this->prepareStatement("SELECT saison FROM anmeldung GROUP BY saison;");
		$this->executeStatement($stmt);
		$str = "<p>\n";
		while($row = $stmt->fetch()) {
			$str .= "<a class='button' href='index.php?action=print_angemeldet&saison=" . $row['saison'] . "'>" . $row['saison'] . "</a>\n";
		}
		$str .= "</p>\n";

		return $str;
	}

	// Gibt die Sortierungs-Buttons nach Datum und nach Kategorie aus
	function showOrderBy($saison) {
		$str = "Sortieren nach: ";
		$str .= "<a class='button' href='index.php?action=print_angemeldet&saison=$saison&orderby=date'>Datum</a> ";
		$str .= "<a class='button' href='index.php?action=print_angemeldet&saison=$saison&orderby=category'>Kategorie</a>";
		return $str;
	}

	// Interpretiert die Sortierung anhand von GET-Argumenten
	function interpretOrder() {
		$order = get('orderby', $_GET);
		switch ($order) {
			case "date";
			$this->order("datum");
			break;
			default:
				$this->order("saison, kategorie, malter, liga, verein");
				break;
		}
	}

	function getRegistration($regId) {
		if (isset($this->registration) && $this->registration['ID'] == $regId) {
			// We already got the result
		} else {
			$stmt = $this->prepareStatement("SELECT * FROM `anmeldung` where ID=:ID");
			$this->executeStatement($stmt, array(":ID" => $regId));
			$this->registration = $stmt->fetch();
			$stmt->closeCursor();
		}
		return $this->registration;
	}

	function askDelete($regId) {
		$reg = $this->getRegistration($regId);
		$str  = "<p>Wollen Sie die Anmeldung von <b>$reg[verein] $reg[mNummer]</b> der Kategorie <b>$reg[kategorie] $reg[malter]</b> der Saison <b>$reg[saison]</b> wirklich l&ouml;schen?<br>\n";
		$str .= "<a class='btn btn-primary' href=\"index.php?action=delete_registration&regId=$regId\">Anmeldung löschen </a> <a class='btn btn-default' href=\"javascript:history.back();\">nicht löschen und zurück</a><br>\n";
		$str .= "</p>";
		return $str;
	}

	function delete($regId) {
		$stmt = $this->prepareStatement("DELETE FROM anmeldung WHERE ID=:ID;");
		$this->executeStatement($stmt, array(':ID' => $regId));

		return 'Anmeldung gelöscht. <a href="?action=print_angemeldet"><img src="images/undo.svg" width=20 height=20 border=0> Zurück zur Übersicht</a>';
	}

	function createTeamForm($regId) {
		$teamDescription = $this->getTeamDescription($regId);
		$teamName = $this->getTeamName($regId);
		$leagues = $this->getLeagues();
		$vereine = $this->getVereine($regId);
		$guessedVerein = $this->getGuessedVerein($regId);

		return $this->getTemplates()->render('registration/createTeamForm', [
				'regId' => $regId,
				'teamName' => $teamName,
				'teamDescription' => $teamDescription,
				'leagues' => $leagues,
				'vereine' => $vereine,
				'guessedVerein' => $guessedVerein
		]);
	}

	private function getTeamDescription($regId) {
		$team = $this->getRegistration($regId);
		$category = $this->katMap[$team['kategorie']];
		$teamDescription = $team['verein'] . ' ' . $team['mNummer'] . ' ' . 'Kategorie ' . $category . ' ' . $team['malter'];
		return $teamDescription;
	}
	
	private function getTeamName($regId) {
		$team = $this->getRegistration($regId);
		$category = $this->katMap[$team['kategorie']];
		$teamDescription = $team['verein'];
		if ($team['mNummer']) {
			$teamDescription .= ' ' . $team['mNummer'];
		}
		return $teamDescription;
	}

	private function getLeagues() {
		$stmt = $this->prepareStatement('SELECT ID, liga FROM liga order by liga;');
		$this->executeStatement($stmt);
		$leagues = array();
		while($row = $stmt->fetch()) {
			$leagues[$row[ID]] = $row[liga];
		}
		return $leagues;
	}
	
	private function getGuessedVerein($regId) {
		$reg = $this->getRegistration($regId);
		$stmt = $this->prepareStatement('SELECT ID, verein FROM verein WHERE verein like :verein');
		$this->executeStatement($stmt, array(':verein' => '%' . $reg['verein'] . '%'));
		$v = $stmt->fetch();
		$guessedVereinId = -1;
		if ($stmt->rowCount()>0) {
			$guessedVereinId = $v['ID'];
		}
		$stmt->closeCursor();
		return $guessedVereinId;
	}

	private function getVereine($regId) {
		$stmt = $this->prepareStatement('SELECT ID, verein FROM verein ORDER BY verein;');
		$this->executeStatement($stmt);
		$vereine = array();
		while ($row = $stmt->fetch()) {
			$vereine[$row[ID]] = $row[verein];
		}
		return $vereine;
	}

	function createTeam($regId, $ligaId, $vereinId) {
		$reg = $this->getRegistration($regId);
		$stmt = $this->prepareStatement("INSERT INTO mannschaft SET
				mannschaft=:mannschaft,
				vereinID=:vereinId,
				ligaID=:ligaId,
				tenue=:tenu,
				hosen=:hosen,
				ersatz_tenue=:ersatz_tenue,
				ersatz_hosen=:ersatz_hosen;");
		$this->executeStatement($stmt, array(
				':mannschaft' => $reg['verein'] . ' ' . $reg['mNummer'],
				':vereinId' => $vereinId,
				':ligaId' => $ligaId,
				':tenu' => $reg['tenueShirt'],
				':hosen' => $reg['tenueHose'],
				':ersatz_tenue' => $reg['ersatzShirt'],
				':ersatz_hosen' => $reg['ersatzHose'],
		));
		$mannschaftID = $this->getConnection()->lastInsertId();
		$stmt = $this->prepareStatement("UPDATE anmeldung SET mannschaftID=:mannschaftID WHERE ID=:ID;");
		$this->executeStatement($stmt, array(':mannschaftID' => $mannschaftID, ':ID' => $regId));

		$str = 'Mannschaft wurde erstellt.</br><a class="btn btn-primary" href="index.php?action=print_angemeldet">Zurück zu den Anmeldungen</a>';
		return $str;
	}
}

?>
