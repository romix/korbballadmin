<?php

class Medien {

	private $adm;

	function __construct(IVKAdmin $adm) {
		$this->adm = $adm;
	}

	//Zeigt die gruppen an und macht Checkboxen f�r Rangliste oder Tabelle
	function selectionMedien($target) {
		$str = '<h1>Medien</h1>';
		$str .= '<p>Bitte w&auml;hle die Spieltage aus, die Du den Medien schicken m&ouml;chtest:</p>';
		$str .= '<form name="selection" action="' . $target . '" method="GET">';
		$str .= '<input type="hidden" name="action" value="print_medien">';
		$str .= '<input type="hidden" name="show" value="1">';
		$str .= '<table class="table" bordercolor=black cellpadding=3 cellspacing=0 class=data>';
		$str .= '<tr><th>Spieltag</th><th>Gruppen</th></tr>';

		//SQL zusammenstellen
		$sql = "SELECT g.gruppe, s.tag FROM gruppe g LEFT JOIN spiel s ON g.ID = s.gruppeID WHERE NOT s.tag IS NULL GROUP BY g.gruppe, s.tag ORDER BY s.tag, g.gruppe";

		$selectionStmt = $this->adm->pdodb->query($sql);
		
		$days = array();
		while ($row = $selectionStmt->fetch()) {
			$day = $row['tag'];
			if (get($day,$days) == null) {
				$days[$day] = $row['gruppe'];
			} else {
				$days[$day] .= ', ' . $row['gruppe'];
			}
		}

		foreach ($days as $day => $groups) {
			$str .= "<tr><td><input type='checkbox' name='days[$day]'> " . dat_m2u($day) . "</td><td>$groups</td></tr>\n";
		}

		$str .= '</table>';
		$str .= '<p><button type="submit" class="btn btn-primary">Weiter</button></p>';
		$str .= '</form>';

		return $str;
	}

	//Gibt den Text für die Medien zurück
	function showMedien ($pdodb, $days) {
		$str ="<p><strong>Folgender Text kannst du per Mail an deine Medien verschicken:</strong><p>";
		
		$str .="<p><strong>Empfänger:</strong></p>";
		$str .="<ul>";
		$str .="<li>Radio Sunshine &lt;newsredaktion@sunshine.ch&gt;&#59;</li>";
		$str .="<li>Radio Pilatus &lt;redaktion@radio-pilatus.ch&gt;&#59;</li>";
		$str .="<li>Radio SRF Regionaljournal &lt;regilu@srf.ch&gt;&#59;</li>";
		$str .="<li>Marlis Ulrich &lt;marlis.ulrich87@bluewin.ch&gt;&#59;</li>";
		$str .="<li>Roman Schaller &lt;roman.schaller@gmail.com&gt;&#59;</li>";
		$str .="<li>Anzeiger vom Rottal &lt;redaktion@rottaler.ch&gt;&#59;</li>";
		$str .="<li>Surseer Woche &lt;redaktion@surseerwoche.ch&gt;&#59;</li>";
		$str .="<li>Willisauerbote &lt;patrik.birrer@willisauerbote.ch&gt;&#59;</li>";
		$str .="<li>Emil Stöckli &lt;stoeckli_langnau@bluewin.ch&gt;&#59;</li>";
		$str .="<li>Entlebucher Anzeiger &lt;redaktion@entlebucher-anzeiger.ch&gt;&#59;</li>";
		$str .="<li>Freier Schweizer &lt;verlag@freierschweizer.ch&gt;&#59;</li>";
		$str .="<li>René Leupi &lt;rene.leupi@luzernerzeitung.ch&gt;&#59;</li>";
		$str .="</ul>";
		
		$str .="<p><strong>Betreff:</strong> Resultate Korbball-Wintermeisterschaft Zentralschweiz</p>";
		
		$str .="<strong>Mitteilung:</strong><br><br>";
		
		$str .="<p>Guten Tag</p>";
		$str .="<p>Anbei finden Sie die Resultate der Korbball-Wintermeisterschaft der Zentralschweiz von diesem Wochenende:</p>";

		$inSql = "(";
		$first = true;
		$daysParam = null;
		foreach ($days as $day => $value) {
			if ($first) {
				$first=false;
			} else {
				$inSql .=', ';
			}
			$inSql .= '?';
			$daysParam[] = $day;
		}
		$inSql .=")";
		$t = new c_tabelle($this->adm);
    $sql = "SELECT g.ID, g.gruppe FROM gruppe g LEFT JOIN spiel s ON g.ID = s.gruppeID WHERE s.tag IN ".$inSql." GROUP BY g.ID, g.gruppe ORDER BY g.gruppe";
		$stmt = $pdodb->prepare($sql);
		$res = $stmt->execute($daysParam);
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			$gruppe = $row['gruppe'];
			$str .= "<p><strong>".$gruppe.": </strong>\n";
			$str .= $this->resultate($inSql, $daysParam, $row['ID'], $pdodb);
			$str .= "<strong> - Rangliste:</strong> " . $this->rangliste($row['ID']) . "</p>\n";
		}

		return $str;
	}

	//Gibt eine Rangliste zur�ck
	function rangliste($gruppeId) {
		//Brauchen wir zum Anzeigen der Mannschaftsnamen
		$tab = new c_tabelle($this->adm);

		//Gruppe und Liga suchen
		$sql = "SELECT gruppe, liga FROM gruppe LEFT JOIN liga ON liga.ID = gruppe.ligaID WHERE gruppe.ID = :gruppeId";
		$stmt = $this->adm->pdodb->prepare($sql);
		$stmt->execute(array(':gruppeId' => $gruppeId));
		$row = $stmt->fetch();

		$r = new c_rangliste($this->adm);
		$r->setGroup($gruppeId);
		$rang = $r->getRangliste();

		$first = true;
		$str = "";
		foreach ($rang as $Krang => $rangTeams) {
			foreach ($rangTeams as $Vrang) {
				if ($first) {
					$first = false;
				} else {
					$str .= ". ";
				}
				if (!strpos($Vrang, ".")) {
					//Normale Gruppe
					$sql = "SELECT mannschaft FROM mannschaft WHERE ID = :mid";
					$stmt = $this->adm->pdodb->prepare($sql);
					$stmt->execute(array(':mid' => $Vrang));
					$row = $stmt->fetch();
					$str .= $Krang + 1 . ". " . $this->beautifyTeam($row['mannschaft']) . ' ';
				} else {
					//Final-Gruppe
					$arrTeamCode = $tab->arrTeamCode($Vrang);
					$str .= $Krang + 1 . ". " . $this->beautifyTeam($arrTeamCode['team']);
				}

				$str .= ' ' . $r->anzahlGespielteSpiele($Vrang) .'/'. $r->punkte($Vrang);
			}
		}

		return $str;
	}

	//Gibt die Resultate zurck
	function resultate ($inSql, $daysParam, $gruppe, PDO $pdodb) {
		$tab = new c_tabelle($this->adm);
		//Gruppe und Liga suchen
		$sql = "SELECT gruppe, liga FROM gruppe LEFT JOIN liga ON liga.ID = gruppe.ligaID WHERE gruppe.ID=:gid";
		$stmt = $pdodb->prepare($sql);
		$stmt->execute(array(':gid' => $gruppe));
		$row = $stmt->fetch();


		$sql = "SELECT s.* FROM spiel s WHERE s.tag IN $inSql AND gruppeID=?";
		$stmt = $pdodb->prepare($sql);
		$daysParam[] = $gruppe;
		$stmt->execute($daysParam);

		$first = true;
		$str = '';
		
		while ($Spiel = $stmt->fetch(PDO::FETCH_ASSOC)) {
			if ($first) {
				$first = false;
			} else {
				$str .= ". ";
			}
			$arrTeamCode = $tab->arrTeamCode($Spiel['teamA']);
			if ($arrTeamCode['team']=="") {
				$str .= $this->beautifyTeam($arrTeamCode['txt']) . " - ";
			} else {
				$str .= $this->beautifyTeam($arrTeamCode['team']) . " - ";
			}
			$arrTeamCode = $tab->arrTeamCode($Spiel['teamB']);
			if ($arrTeamCode['team']=="") {
				$str .= $this->beautifyTeam($arrTeamCode['txt']) . " ";
			} else {
				$str .= $this->beautifyTeam($arrTeamCode['team']) . " ";
			}
			$str .= $Spiel['resultatA'] .":". $Spiel['resultatB'];
		}

		return $str;
	}

	function beautifyTeam($team) {
		$space = strpos($team, ' ');
		if ($space < 4) {
			$team = substr($team, $space);
		} else {
			if (substr($team, 0, 11) == "Sport Union") {
				$team = substr($team, 12);
			} else {
				if (substr($team, 0, 16) == "SVKT Frauensport") {
					$team = substr($team, 17);
				} else {
					if (substr($team, 0, 5) == "Fides") {
						$team = substr($team, 6);
					} else {
						if (substr($team, 0, 8) == "Korbball") {
							$team = substr($team, 9);
						}
					}
				}
			}
		}
		return $team;
	}
}

?>