<?php
header("Content-type: image/png");
header('Cache-Control: max-age=31556926');
header('Expires: ' . gmdate('D, d M Y H:i:s', time() + (60*60*24)) . ' GMT');
header("");
$string = $_GET['text'];
$string = str_replace('<br>', ' ', $string);
$string = strip_tags($string);
$bbox = imagettfbbox(10, 0 ,"font/Arial.ttf", $string);
$imgHeight = $bbox[2] + 5;
$imgWidth = 20;
$im     = imagecreatetruecolor($imgWidth, $imgHeight);
imagesavealpha($im, true);
$trans_colour = imagecolorallocatealpha($im, 0, 0, 0, 127);
imagefill($im, 0, 0, $trans_colour);
$fontcolor = imagecolorallocate($im, 0, 0, 0);
$px     = 15;
$py     = $imgHeight;
imagettftext($im, 9, 90, $px, $py, $fontcolor, "font/Arial.ttf", $string);
imagepng($im);
imagedestroy($im);

?>
