<?php

define('RANGLISTE_resultA', 'resultA');
define('RANGLISTE_resultB', 'resultB');
define('RANGLISTE_pointsA', 'pointsA');
define('RANGLISTE_pointsB', 'pointsB');
define('RANGLISTE_own', 'own');
define('RANGLISTE_others', 'others');
define('RANGLISTE_games', 'games');

define('RANGLISTE_SORTING_points', 'points');
define('RANGLISTE_SORTING_subranking', 'subranking');
define('RANGLISTE_SORTING_goalrates', 'goalrates');
define('RANGLISTE_SORTING_shotgoals', 'shotgoals');


class c_rangliste {

	private $teams; // the teams in this ranking list
	private $ranking; // (rank => array(teamA, ...))
	private $games; // [teamA] => (
	//       [teamB] => (
	//                  [RANGLISTE_pointsA] => 3,
	//                  [RANGLISTE_pointsB] => 1,
	//                  [RANGLISTE_games] => (
	//                         [0] => ([RANGLISTE_resultA] = 5, [RANGLISTE_resultB] = 4),
	//                         [1] => ([RANGLISTE_resultA] = 3, [RANGLISTE_resultB] = 3)
	//                     )
	//             )
	//       )
	private $points; // [teamID] => points
	private $goalRate; // [teamID] => (RANGLISTE_own => own goals, RANGLISTE_others => other goals)
	private $annotations = array();
	private $gruppe;
	private $gruppeArt;
	private $adm;
	private $subtableRanking = array();

	function __construct(IVKAdmin $adm) {
		$this->adm = $adm;
	}

	function setGroup($gruppe) {
		$this->gruppe = $gruppe;
		$this->teams = array();

		$this->fillGroupType($gruppe);

		if ($this->gruppeArt=="liga") {
			$this->fillTeamIDForNormalGroup($gruppe);
		} elseif ($this->gruppeArt=="final") {
			$this->fillTeamIDForFinalGroup($gruppe);
		}

		$this->fillGames($gruppe);		
		$this->calculateTotalPointsAndGoalRates();
		$this->addGroupExtraPoints();
		$this->calculateRanking();
	}

	function setGames($games) {
		$this->games = $games;
		$this->teams = array();
		foreach ($games as $team => $competitors) {
			if (!array_key_exists($team, $this->teams)) {
				$this->teams[] = $team;
			}
		}
		$this->calculateTotalPointsAndGoalRates();
		$this->calculateRanking();
	}

	private function calculateTotalPointsAndGoalRates() {
		$this->points = array();
		$this->goalRate = array();
		foreach ($this->teams as $teamID) {
			$this->points[$teamID] = 0;
			$this->goalRate[$teamID] = array(RANGLISTE_own => 0, RANGLISTE_others => 0);
		}
		foreach ($this->games as $teamID => $competitors) {
			foreach ($competitors as $competitorID => $results) {
				// points
				$this->points[$teamID] += $this->games[$teamID][$competitorID][RANGLISTE_pointsA];
				// goal rates
				foreach ($this->games[$teamID][$competitorID][RANGLISTE_games] as $game) {
					$this->goalRate[$teamID][RANGLISTE_own] += $game[RANGLISTE_resultA];
					$this->goalRate[$teamID][RANGLISTE_others] += $game[RANGLISTE_resultB];
				}
			}
		}
	}
	
	private function addGroupExtraPoints() {
		$sql = "SELECT teamID, extrapunkte, grund FROM gruppe_extrapunkte WHERE gruppeID=:gruppeID";
		$extraPointsQuery = $this->adm->pdodb->prepare($sql);
		if (!$extraPointsQuery->execute(array (':gruppeID' => $this->gruppe))) {
			echo $extraPointsQuery->errorInfo();
			throw new Exception("Could not load extra points.");
		}
		while ($extraPoints = $extraPointsQuery->fetch()) {
			$points = $extraPoints['extrapunkte'];
			$teamId = $extraPoints['teamID'];
			$grund = $extraPoints['grund'];
			$this->points[$teamId] += $points;
			$annotation = null;
			$singularPlural = abs($points) == 1 ? ' Punkt' : ' Punkte';
			if ($points > 0) {
				$annotation = $points . $singularPlural. ' Zuschlag: ' . $grund;
			} else {
				$annotation = (0 - $points) . $singularPlural. ' Abzug: ' . $grund;
			}
			if ($annotation != null) {
				$this->annotations[$teamId][] = $annotation;
			}
		}
	}
	
	private function compareByPoints($a, $b) {
		$diff = $this->punkte($a) - $this->punkte($b);
		return $diff * -1;
	}
	
	private function compareByGoalRates($a, $b) {
		$ratesA = $this->goalRate[$a];
		$ratesB = $this->goalRate[$b];
		$diff = ($ratesA[RANGLISTE_own] - $ratesA[RANGLISTE_others]) - ($ratesB[RANGLISTE_own] - $ratesB[RANGLISTE_others]);
		return $diff * -1;
	}
	
	private function compareByShotGoals($a, $b) {
		$ratesA = $this->goalRate[$a];
		$ratesB = $this->goalRate[$b];
		$diff = $ratesA[RANGLISTE_own] - $ratesB[RANGLISTE_own];
		return $diff * -1;
	}
	
	private function allTeamsSamePoints($sorting) {
		reset($sorting);
		$firstPoints = $sorting[key($sorting)][RANGLISTE_SORTING_points];
		foreach ($sorting as $team => $s) {
			if ($sorting[$team][RANGLISTE_SORTING_points] != $firstPoints) {
				return false;
			}
		}
		return true;
	}
	
	private function allTeamsSameRates() {
		$firstRate = $this->goalRate[$this->teams[0]];
		foreach ($this->teams as $team) {
			if ($this->goalRate[$team] != $firstRate) {
				return false;
			}
		}
		return true;
	}
	
	private function groupTeamsWithSamePoints($sorting) {
		$arr = array();
		foreach ($sorting as $team => $s) {
			$arr[$s[RANGLISTE_SORTING_points]][] = $team;
		}
		$toRemove = array();
		foreach ($arr as $points => $teams) {
			if (count($teams) == 1) {
				$toRemove[] = $points;
			}
		}
		foreach ($toRemove as $remove) {
			unset($arr[$remove]);
		}
		return $arr;
	}

	// Bei Punktegleichheit am Ende der Meisterschaft entscheidet die Rangierung vorab:
	// 1. Punktzahl aus den direkten Begegnungen der betroffenen Mannschaften.
	// 2. Korbdifferenz aus den direkten Begegnungen der betreffenden Mannschaften.
	// 3. Bessere Zahl der erzielten Körbe aus den direkten Begegnungen.
	// 4. Korbdifferenz aus der ganzen Meisterschaft.
	// 5. Bessere Zahl der erzielten Körbe aus der ganzen Meisterschaft. 
	private function calculateRanking() {
		// create ranking by points
		$sorting = array();
		$rankingOffset = array();
		foreach ($this->teams as $team) {
			$sorting[$team][RANGLISTE_SORTING_points] = $this->punkte($team);
		}
		
		uksort($sorting, array($this, "compareByPoints"));

		if ($this->allTeamsSamePoints($sorting)) {
			// all teams got the same amount of points. Sort by goal rates.
			uksort($sorting, array($this, "compareByGoalRates"));

			if ($this->allTeamsSameRates()) {
				// all teams got the same rate. Sort by shot goals.
				uksort($sorting, array($this, "compareByShotGoals"));
			}
		} else {
			// if some teams have the same amount of points. create a sub table
			foreach ($this->groupTeamsWithSamePoints($sorting) as $point => $samePointTeams) {
				$subTable = new c_rangliste($this->adm);
				$subGames = array();
				foreach ($this->games as $teamA => $competitors) {
					if (in_array($teamA, $samePointTeams)) {
						foreach ($competitors as $competitor => $samePointGames) {
							if (in_array($competitor, $samePointTeams)) {
								$subGames[$teamA][$competitor] = $samePointGames;
							}
						}
					}
				}
				$subTable->setGames($subGames);
				foreach ($subTable->getRangliste() as $subTableRanking => $teamIds) {
					foreach ($teamIds as $teamId) {
						$this->subtableRanking[$teamId] = $subTableRanking;
					}
				}
			}
		}

		// make final sorting by
		// 1. points
		// 2. subtable ranking
		// 3. goal rates
		// 4. shot goals
		uksort($sorting, array($this, "compareByPointsSubtableGoalratesShotgoals"));
		
		// create ranking array with the content like this ranking => teamIDs (array)
		$this->ranking = array();
		$rank = 0;
		$teamCount = 0;
		$previousTeam = null;
		foreach($sorting as $teamId => $points) {
			if ($previousTeam) {
				if ($this->compareByPointsSubtableGoalratesShotgoals($previousTeam, $teamId) != 0) {
					$rank = $teamCount;
				}
			}
			$this->ranking[$rank][] = $teamId;
			$previousTeam = $teamId;
			$teamCount++;
		}
	}
	
	private function compareByPointsSubtableGoalratesShotgoals($a, $b) {
		$diff = $this->compareByPoints($a, $b);
		if ($diff != 0) {
			return $diff;
		}
		$diff = $this->compareBySubtableRanking($a, $b);
		if ($diff != 0) {
			return $diff;
		}
		$diff = $this->compareByGoalRates($a, $b);
		if ($diff != 0) {
			return $diff;
		}
		$diff = $this->compareByShotGoals($a, $b);
		return $diff;
	}
	
	private function compareBySubtableRanking($a, $b) {
		if (sizeof($this->subtableRanking) > 0) {
			$diff = $this->subtableRanking[$a] - $this->subtableRanking[$b];
			return $diff;
		} else {
			return 0;
		}
	}

	
	/**
	 * Returns the ranking in an array. It comes in following format:
	 * 
	 * rank => array(teamA, teamB, ...)
	 * 
	 * @return array
	 */
	public function getRangliste() {
		return $this->ranking;
	}

	/**
	 * Returns all points won of a team
	 * @param string $ID the team ID
	 * @return integer
	 */
	function punkte($ID) {
		return $this->points[$ID];
	}
	
	/**
	 * Returns all points for all teams in an array.
	 * 
	 * @return array [teamID] => points
	 */
	function getAllPoints() {
		return $this->points;
	}
	
	/**
	 * Returns all annotations to this table
	 * 
	 * @return array of teamId => annotation as string
	 */
	function getAllAnnotations() {
		return $this->annotations;
	}

	/**
	 * Korbverhältnis für ein Team. Kommt in einem array zurück. index 0 sind die geschossenen Körbe, index 1 die erhaltenen.
	 * @param string $ID des Teams
	 * @return NULL[]|mixed[]
	 */
	function korbVerhaeltnis ($ID) {
		return array($this->goalRate[$ID][RANGLISTE_own], $this->goalRate[$ID][RANGLISTE_others]);
	}
	
	function getAllGoalRates() {
		return $this->goalRate;
	}

	/**
	 * Count of games played of a particular team.
	 * 
	 * @param string $ID team
	 * @return number
	 */
	function anzahlGespielteSpiele($ID) {
		$gespielt = 0;
		$games = $this->games[$ID];
		if ($games != null) {
			foreach ($games as $competitorID => $competitorGames) {
				foreach ($competitorGames['games'] as $gameID => $results) {
					if ($results['resultA'] != '' && $results['resultB'] != '') {
						$gespielt++;
					}
				}
			}
		}
		return $gespielt;
	}
	
	/**
	 * Count of games won of a particular team.
	 * 
	 * @param string $ID team
	 * @return number
	 */
	function gamesWon($ID) {
		$won = 0;
		$games = $this->games[$ID];
		if ($games != null) {
			foreach ($games as $competitorID => $competitorGames) {
				foreach ($competitorGames['games'] as $gameID => $results) {
					if ($results[RANGLISTE_resultA] > $results[RANGLISTE_resultB]) {
						$won++;
					}
				}
			}
		}
		return $won;
	}
	
	/**
	 * 
	 * Count of games lost of a particular team.
	 * 
	 * @param string $ID team
	 * @return number
	 */
	function gamesLost($ID) {
		$lost = 0;
		$games = $this->games[$ID];
		if ($games != null) {
			foreach ($games as $competitorID => $competitorGames) {
				foreach ($competitorGames['games'] as $gameID => $results) {
					if ($results[RANGLISTE_resultA] < $results[RANGLISTE_resultB]) {
						$lost++;
					}
				}
			}
		}
		return $lost;
	}
	
	/**
	 *
	 * Count of tie games of a particular team.
	 *
	 * @param string $ID team
	 * @return number
	 */
	function gamesTie($ID) {
		$tie = 0;
		$games = $this->games[$ID];
		if ($games != null) {
			foreach ($games as $competitorID => $competitorGames) {
				foreach ($competitorGames['games'] as $gameID => $results) {
					if ($results['resultA'] != '' && $results['resultB'] != '') {
						if ($results[RANGLISTE_resultA] == $results[RANGLISTE_resultB]) {
							$tie++;
						}
					}
				}
			}
		}
		return $tie;
	}
	
	function fillTeamIDForNormalGroup($groupId) {
		$sql = "SELECT mannschaft.ID, mannschaft.mannschaft
		FROM teamgruppe LEFT JOIN mannschaft ON teamgruppe.teamID=mannschaft.ID
		WHERE teamgruppe.gruppeID=:gruppeID
		ORDER BY mannschaft";
		$teamQuery = $this->adm->pdodb->prepare($sql);
		if (!$teamQuery->execute(array (':gruppeID' => $groupId))) {
			echo $teamQuery->errorInfo();
			throw new Exception("Could not load teams for group $groupId.");
		}
		
		while ($teamRow = $teamQuery->fetch()) {
			$this->teams[] = $teamRow['ID'];
		}
	}
	
	function fillTeamIDForFinalGroup($groupId) {
		$sql = "SELECT teamA FROM spiel where gruppeID=:gruppeID GROUP BY teamA ORDER BY teamA";
		$teamQuery = $this->adm->pdodb->prepare($sql);
		if (!$teamQuery->execute(array (':gruppeID' => $groupId))) {
			echo $teamQuery->errorInfo();
			throw new Exception("Could not load teams for group $groupId.");
		}
		while ($teamRow = $teamQuery->fetch()) {
			$this->teams[] = $teamRow['teamA'];
		}
	}

	function fillGroupType($groupId) {
		$groupQuery = $this->adm->pdodb->prepare("SELECT * FROM gruppe WHERE ID=:gruppeID");
		if (!$groupQuery->execute(array (':gruppeID' => $groupId))) {
			echo $groupQuery->errorInfo();
			throw new Exception("Group not found.");
		}
		$group = $groupQuery->fetch();
		$this->gruppeArt = $group["typ"];
	}
	
	private function fillGames($groupId) {
		$this->games = array();
		$sql = "SELECT teamA, teamB, resultatA, resultatB, punkteA, punkteB FROM spiel where gruppeID=:gruppeID";
		$gameQuery = $this->adm->pdodb->prepare($sql);
		if (!$gameQuery->execute(array (':gruppeID' => $groupId))) {
			echo $gameQuery->errorInfo();
			throw new Exception("Could not load games.");
		}
		while ($gameRow = $gameQuery->fetch()) {
			$teamA = $gameRow['teamA'];
			$teamB = $gameRow['teamB'];
			$resultA = $gameRow['resultatA'];
			$resultB = $gameRow['resultatB'];
			$pointsA = $gameRow['punkteA'];
			$pointsB = $gameRow['punkteB'];
			if (!array_key_exists($teamA, $this->games)) {
				$this->games[$teamA] = array();
			}
			if (!array_key_exists($teamB, $this->games[$teamA])) {
				$this->games[$teamA][$teamB] = array(RANGLISTE_pointsA => 0, RANGLISTE_pointsB => 0, RANGLISTE_games => array());
				$this->games[$teamB][$teamA] = array(RANGLISTE_pointsA => 0, RANGLISTE_pointsB => 0, RANGLISTE_games => array());
			}
			$this->games[$teamA][$teamB][RANGLISTE_pointsA] += $pointsA;
			$this->games[$teamA][$teamB][RANGLISTE_pointsB] += $pointsB;
			$this->games[$teamB][$teamA][RANGLISTE_pointsA] += $pointsB;
			$this->games[$teamB][$teamA][RANGLISTE_pointsB] += $pointsA;
		
			$gameA = array(RANGLISTE_resultA => $resultA, RANGLISTE_resultB => $resultB);
			$this->games[$teamA][$teamB][RANGLISTE_games][] = $gameA;
		
			$gameB = array(RANGLISTE_resultA => $resultB, RANGLISTE_resultB => $resultA);
			$this->games[$teamB][$teamA][RANGLISTE_games][] = $gameB;
		}
	}
}
?>
