<?php

class User extends BaseCRUD
{

    public function __construct(IVKAdmin $adm)
    {
        parent::__construct($adm);
        $this->title = "User";

        $this->sqlSel['admin'] = "SELECT * FROM user;";
        $this->fldSel['admin'] = array("ID", "anrede", "name", "vorname", "strasse", "ort", "email", "passwort", "rights");
        $this->descSel['admin'] = array("ID", "Anrede", "Name", "Vorname", "Strasse", "Ort", "E-Mail", "Passwort", "Rechte");

        $this->sqlIns['new'] = "INSERT INTO user SET anrede=:anrede, name=:name, vorname=:vorname, strasse=:strasse, ort=:ort, email=:email , passwort=:passwort;";
        $this->fldIns['new'] = array("anrede", "name", "vorname", "strasse", "ort", "email", "passwort");
        $this->descIns['new'] = array("Anrede", "Name", "Vorname", "Strasse", "Ort", "E-Mail", "Passwort");

        $this->key = "ID";
        $this->required = array("name", "vorname", "email", "passwort");
        $this->fieldtype = array(
            "anrede" => "combo",
            "passwort" => "password",
        );
        $this->fieldinfo = array(
            "anrede" => array(
                "datasource" => array("Frau" => "Frau", "Herr" => "Herr"),
            ),
        );
    }

    public function verify_email($email)
    {
        $stmt = $this->prepareStatement('SELECT ID FROM user WHERE email=:email;');
        $stmt->execute(array("email" => $email));
        if ($stmt->rowCount() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function registerNewUser($vorname, $name, $email, $password)
    {
        $isNewUser = $this->verify_email($email);
        if ($isNewUser) {
					// If this is the first user of that system. Make it admin.
					$stmt = $this->prepareStatement('SELECT count(*) AS userCount FROM user;');
					$stmt->execute();
					$userCountRow = $stmt->fetchAll(PDO::FETCH_ASSOC);
					$isAdmin = $userCountRow[0]['userCount'] == 0;
					$rights = $isAdmin ? 'adm' : null;
					$passwortHash = password_hash($password, PASSWORD_DEFAULT);
					$insertStmt = $this->prepareStatement('INSERT INTO user SET name = :name, vorname = :vorname, email = :email, passwortHash = :passwortHash, rights = :rights;');
					$insertSuccess = $insertStmt->execute(array('vorname' => $vorname, 'name' => $name, 'email' => $email, 'passwortHash' => $passwortHash, 'rights' => $rights));
					return $this->getTemplates()->render('userregistration/registrationSuccessful', [
						'name' => $name, 
						'vorname' => $vorname,
						'isAdmin' => $isAdmin]);
        } else {
					return $this->getTemplates()->render('userregistration/userAlreadyExists', ['email' => $email]);
        }
    }

}