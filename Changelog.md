# Changelog

## 23.1.2020: Anpassung um neuere mySQL und MariaDB Datenbanken zu unterstützen

## 30.1.2020: Neue Mobile-Schnittstelle zur Abfrage der aktuellen Saison

## 13.1.2020: Präsentationsmodus aktualisiert

## 9.1.2020: Mobile-Schnittstelle
Content-Type wird nun mitgeliefert.
Keinen Fehler mehr, wenn in einer Gruppe keine Runde oder keine Spiele vorhanden sind.

## 6.12.2019: Mobile-Schnittstelle
Access-Control-Header hinzugefügt, damit die Resultate auch auf anderen Seiten im Browser integriert werden können.

## 25.11.2019: Diverse Modernisierungen des Codes

## 12.6.2019: Anmeldung aktualisiert

## 7.5.2019: Sicherheits-Patch
Passwörter werden nicht mehr im Klartext abgespeichert.

## 16.4.2019: Besserer Filter in der Explorer-Ansicht

## 16.4.2019: Verbesserungen im Anmeldeprozess

## 12.1.2019: Verbesserung in Kalender-Integration
Im Kalender-Eintrag für Smartphones wird nun auch die Halle angezeigt.

## 17.11.2018: Neue Funktion Startliste
Die Startliste zeigt an, um welche Zeit jede erscheinende Manschaft ihr erstes Spiel absolviert.

## 3.12.2017: Optische Verbesserungen an Tabellendarstellung

## 3.12.2017: Optische Verbesserungen an Präsentationsmodus

## 25.11.2017: Medien
Mail-Adressen korrigiert

## 17.6.2017: Anmeldeformular angepasst

## 9.2.2017: Rangliste
Bugfix der Ranglisten-Berechnung.

## 26.11.2016: Medien
Mail-Adressen ergänzt

## 24.11.2016: Präsentations-Modus erweitert
Nun ist es möglich, die Tagesverantwortlichen Schiedsrichter im Präsentationsmodus zu konfigurieren.

## 16.10.2016: Spiel-Export und -Import entfernt

## 16.10.2016: Kompatibilität zu PHP 7

## 17.9.2016: Einfachere Spielerstellung
Der Spielschlüssel wird nun beim Generieren der Spiele angezeigt.
![Spielerstellung](doc-images/Spielerstellung.png)

## 17.9.2016: Tool-Tipps für Buttons in Runden

## 21.6.2016: Punkte hinzufügen / abziehen
Um z.B. Strafpunkte abzuziehen oder Punkte von einer anderen Gruppe mitzunehmen gibt es nun die Möglichkeit, dies bequem pro Gruppe einzutragen. Die Punkte werden in allen Tabellen und Resultaten berücksichtigt und gekennzeichnet.