# Was ist Korbball-Admin?
KorbballAdmin ist eine Web-Applikation, welche ich für die IVK (Interverbands Kommission) geschrieben habe. 
Mit dieser App können Anmeldungen, Spielpläne und Resultate verwaltet und publiziert werden.

# Was ist neu?
Alle Software-Anpassungen sind im [Changelog](Changelog.md) dokumentiert. 

# Urheber
Urherber dieser Web-App sind

- Roman Schaller, Obereyweg 1a, 6207 Nottwil (roman.schaller@gmail.com)
- Samuel Schwegler, Willisauerstrasse 11, 6122 Menznau (samuel.schwegler@gmx.ch)

# Nutzungsrecht
Die IVK hat das alleinige Nutzungsrecht der Anwendung. Sie kann dies aber auch an weitere Organisationen über individuelle Abmachungen übertragen.
Bei der Übertragung des Nutzungsrechtes muss der Urheber einbezogen werden. Das Nutzungsrecht kann nur an Non-Profit-Organisationen übertragen werden. 

# Lokale installation

## Korbball-Admin Konfiguration

KorbballAdmin hat ein Konfigurations-File. Das liegt im Ordner `admin/`. Dort liegt das File `configuration_Default.php`. Das solltest du umbenennen auf `configuration.php` und mit deinen DB-Credentials ausfühllen.

## docker compose

Mit folgenden paar Befehlen kannst ud dir zwei Docker-Container aufsetzen mit dem Apache-Server und einer MariaDB:

```
$ cd docker
$ docker compose up -d
```

Docker beendest du wieder mit: `docker compose stop`.

## Composer

Nun müssen wir noch mittels composer zusätzliche Packages installieren. Wechsle dazu in der Konsole ins Root-Verzeichnis und führe folgendes aus: 

```
$ docker exec -it korbballadmin-web-1 /bin/bash
/var/www/html# php composer.phar install
...
/var/www/html# exit

```

## DB initialisieren
Wir setzen ein DB-Migrations-Tool namens phinx ein. Für phinx müssen wir ebenfalls die Credentials angeben. Kopiere dazu die Datei `dbmigrations/conf/phinx.example.php` und setze die entsprechenden Credentials ein.

Besuche jetzt im Browser folgende URL: `localhost/dbmigrations/index.php` und klicke auf Migrate. Nun werden alle Tabellen angelegt.

Schlussendlich fügst du nun in der Tabelle user einen Benutzer für dich ein und setzt bei `rights` den Wert `adm` ein.

## Daten importieren


## Einstiegspunkte

- DB-Migration: http://localhost:8080/dbmigrations/index.php
- Admin-Oberfläche: http://localhost:8080/admin/
- Anmeldung: http://localhost:8080/anmeldung/registration.php