<?php
session_start();
include "../admin/lib.php";
$input = file_get_contents("php://input");
$data = json_decode($input);
$secret = '6Lef410UAAAAAK2eq3gJQutrc2B2_OSO49rceuTA';
//get verify response data
$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$data->gRecaptchaResponse);
$responseData = json_decode($verifyResponse);
if($responseData->success) {
	try {
		$adm = new IVKAdmin();
		$anmeldung = new SaveAnmeldung($adm);
		$anmeldungData = new AnmeldungData($data);
		$anmeldung->anmelden($anmeldungData);
		$status = 'Eine Bestätigung wird in den nächsten Minuten an folgende Empfänger verschickt: ';
		$status .= $anmeldung->getMailReceiver();
		$response['status'] = $status;
	} catch (Exception $e) {
		http_response_code(400);
		$response['status'] = $e->getMessage();
	}
} else {
	http_response_code(400);
	$response['status'] = 'Bist du ein Roboter? Wahrscheinlich hast du vergessen die Checkbox oben anzukreuzen.';
}

echo json_encode($response);