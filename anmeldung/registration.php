<?php 
header('Content-type: text/html; charset=utf-8');
session_start();
class Configuration {
	public function __construct() {
		include "../admin/configuration_Default.php";
		include "../admin/configuration.php";
	}
}

$config = new Configuration();

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Anmeldung IVK-Wintermeisterschaft <?= $config->saison ?></title>
<link rel="stylesheet" href="bootstrap/css/bootstrap.css"></link>
<!-- script src="bootstrap/js/bootstrap.min.js"></script -->
<script src="angular/angular.min.js"></script>
<script> window.seasonStartYear = <?= $config->seasonStartYear ?>; </script>
<script src="js/registration.js" charset="UTF-8"></script>
<script> function relayRecaptchaDataCallback(data) { recaptchaDataCallback(data); } </script>
<script> function relayRecaptchaDataExpiredCallback(data) { recaptchaDataExpiredCallback(data); } </script>
<script src='https://www.google.com/recaptcha/api.js?hl=de-CH'></script>
</head>
<body>
	<div class="container" ng-app="registrationApp">
		<div ng-controller="registrationCtrl">
			<form name="registration_form" ng-submit="submit()" role="form">
				<h1>Anmeldung Wintermeisterschaft <?= $config->saison ?></h1>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group" ng-class="mandatoryClass(reg.category)">
							<select class="form-control" name="category" ng-model="reg.category" ng-options="cat.id as cat.title for cat in categories">
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group" ng-class="mandatoryClass(reg.age)">
							<select class="form-control" name="age" ng-model="reg.age" ng-options="age.id as age.title for age in ages"></select>
							<div ng-if="reg.age == 'U20'">Bitte Wettkampfvorschriften U20 Hallenkorbball WM der Region 3 beachten.</div>
						</div>
					</div>
				</div>
				<div class="row" ng-show="!isUTeam">
					<div class="col-md-3" ng-class="mandatoryClass(reg.liga)">
						<select class="form-control" name="liga" ng-model="reg.liga" ng-options="liga.id as liga.title for liga in ligas"></select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3" ng-class="mandatoryClass(reg.verein)"><br/>
						<div class="form-group">
							<input id="verein" type="text" class="form-control" ng-model="reg.verein" placeholder="Verein" />
						</div>
					</div>
					<div class="col-md-3" ng-class="mandatoryClass(reg.verband)"><br/>
						<div class="form-group">
							<input id="verband" class="form-control" ng-model="reg.verband" placeholder="Verband" />
						</div>
					</div>
				</div>
				<div class="row" ng-show="!isUTeam">
					<div class="col-md-3">
						<div class="form-group" ng-class="mandatoryClass(reg.teamNbr)">
							<label class="control-label" for="teamNbr">Mannschaft Nr.</label>
							<div id="teamNbr" class="btn-group">
								<button type="button" class="btn btn-default" ng-click="selectTeamNbr('1')" ng-class="{ 'active' : reg.teamNbr == '1' }">1</button>
								<button type="button" class="btn btn-default" ng-click="selectTeamNbr('2')" ng-class="{ 'active' : reg.teamNbr == '2' }">2</button>
								<button type="button" class="btn btn-default" ng-click="selectTeamNbr('3')" ng-class="{ 'active' : reg.teamNbr == '3' }">3</button>
								<button type="button" class="btn btn-default" ng-click="selectTeamNbr('4')" ng-class="{ 'active' : reg.teamNbr == '4' }">4</button>
								<button type="button" class="btn btn-default" ng-click="selectTeamNbr('5')" ng-class="{ 'active' : reg.teamNbr == '5' }">5</button>
								<button type="button" class="btn btn-default" ng-click="selectTeamNbr('6')" ng-class="{ 'active' : reg.teamNbr == '6' }">6</button>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row" ng-show="isUTeam">
					<div class="col-md-3 form-group" ng-class="mandatoryClass(reg.birthYoung)">
						<label for="birthYoung" class="control-label">Geburtsdatum jüngste/r Spieler/in:</label> <input class="form-control" type="date"
							name="birthYoung" ng-model="reg.birthYoung" />
					</div>
					<div class="col-md-3 form-group" ng-class="mandatoryClass(reg.birthOld)">
						<label for="birthOld" class="control-label">Geburtsdatum älteste/r Spieler/in:</label> <input class="form-control" type="date"
							name="birthOld" ng-model="reg.birthOld" />
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<h3>Tenü-Farben</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3" ng-class="mandatoryClass(reg.shirt)">
						<input type="text" class="form-control" ng-model="reg.shirt" placeholder="Shirt" />
					</div>
					<div class="col-md-3" ng-class="mandatoryClass(reg.hose)">
						<input type="text" class="form-control" ng-model="reg.hose" placeholder="Hose" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<h3>Ersatz-Tenü</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3" ng-class="mandatoryClass(reg.ersatzshirt)">
						<input type="text" class="form-control" ng-model="reg.ersatzshirt" placeholder="Ersatz-Shirt" />
					</div>
					<div class="col-md-3" ng-class="mandatoryClass(reg.ersatzhose)">
						<input type="text" class="form-control" ng-model="reg.ersatzhose" placeholder="Ersatz-Hose" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<h3>Vereins-Verantwortliche(r)</h3>
					</div>
				</div>
				<div class="row">
					<my-address address="reg.vereinAddress" mandatory="true" />
				</div>
				<div class="row" ng-show="isUTeam">
					<div class="col-md-6">
						<h3>Jugend-Verantwortliche(r)</h3>
					</div>
				</div>
				<div class="row" ng-show="isUTeam">
					<my-address address="reg.jugendAddress" mandatory="true" />
				</div>
				<div class="row">
					<div class="col-md-6">
						<h3>Mannschafts-Verantwortliche(r)</h3>
					</div>
				</div>
				<div class="row">
					<my-address address="reg.teamAddress" mandatory="true" />
				</div>
				<div class="row">
					<div class="col-md-6">
						<h3>Kassier(in)</h3>
					</div>
				</div>
				<div class="row">
					<my-address address="reg.kassierAddress" mandatory="true" />
				</div>
				<div class="row">
					<div class="col-md-6">
						<h3>Schiedsrichter(in)</h3>
					</div>
				</div>
				<div class="row">
					<referee-address address="reg.refereeAddress" />
				</div>
				<div class="row">
					<div class="col-md-6">
						<h3>Bankverbindung des Vereins</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<input type="text" class="form-control" ng-model="reg.bank" placeholder="PC-Konto oder Bankverbindung" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<h4>Spieler/innen dieser Mannschaft spielen ebenfalls in der folgenden Mannschaft:</h4>
						<input type="text" class="form-control" ng-model="reg.conflict" />
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<h4>{{commentTitle}}</h4>
						<textarea class="form-control" style="min-width: 100%" ng-model="reg.bemerkungen"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="controll-group" ng-class="mandatoryCheckboxClass(reg.agree)">
							<label class="control-label" for="agreement"><input ng-model="reg.agree" type="checkbox" id="agreement" /> Der Verein verpflichtet
								sich den Einsatz und das Haftgeld pro Mannschaft innert der im Reglement erwähnten Frist zu bezahlen, was auch bei einem
								Nichterscheinen einer Mannschaft seine Gültigkeit hat.</label>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="g-recaptcha" data-callback="relayRecaptchaDataCallback" data-expired-callback="relayRecaptchaDataExpiredCallback" 
						data-sitekey="6Lef410UAAAAAFLEhJF_-oSmcs0MNd5ZxrJsdhIE"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<div class="alert alert-danger" ng-show="formError">Bitte die zwingenden Felder (roter Rahmen) ausfüllen.</div>
							<div class="alert alert-info" ng-show="submitting">Bitte warten, die Anmeldung wird verarbeitet...</div>
							<div class="alert alert-danger" ng-show="error">Anmeldung NICHT erfolgreich. {{error}}</div>
							<div class="alert alert-success" ng-show="success">Die Anmeldung wurde erfolgreich gespeichert. {{response}}</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary" ng-show="!submitting">Anmelden</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>

