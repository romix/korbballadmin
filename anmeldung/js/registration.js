var app = angular.module('registrationApp', []);

var userTriedToSend = false;

function recaptchaDataCallback(data) {
	console.log('got data ' + data);
	window.recaptchaResponse = data;
};

function recaptchaDataExpiredCallback() {
	window.recaptchaResponse = undefined;
}

window.recaptchaDataCallback = recaptchaDataCallback;

app.controller('registrationCtrl', function ($scope, $http) {

	// registration var
	$scope.reg = {};
	$scope.reg.category = '';
	$scope.reg.age = '';
	$scope.reg.liga = '';
	$scope.reg.agree = false;
	$scope.reg.vereinAddress = {};
	$scope.reg.jugendAddress = {};
	$scope.reg.teamAddress = {};
	$scope.reg.refereeAddress = {};
	$scope.reg.kassierAddress = {};
	$scope.formError = false;
	$scope.submitting = false;
	$scope.success = false;
	$scope.error = false;
	$scope.commentTitle = 'Bemerkungen:';

	// form states
	$scope.isUTeam = false;

	// state toggles
	$scope.$watch('reg.age', function () {
		$scope.isUTeam = $scope.reg.age.indexOf('U') >= 0;
		if ($scope.reg.age === 'U14' || $scope.reg.age === 'U16' ||  $scope.reg.age === 'HEU18' || $scope.reg.age === 'DAU21') {
			$scope.commentTitle = 'Wünsche / Bemerkungen falls Jugend-Mannschaften zusammengelegt werden müssen:';
		} else {
			$scope.commentTitle = 'Bemerkungen:';
		}
	});

	let oldestU14 = window.seasonStartYear - 13;
	let oldestU16 = window.seasonStartYear - 15;
	let oldestU18 = window.seasonStartYear - 17;
	let oldestU21 = window.seasonStartYear - 20;

	// select arrays
	$scope.categories = [{
		'id': '',
		'title': 'Kategorie wählen...'
	}, {
		'id': 'D',
		'title': 'Damen'
	}, {
		'id': 'H',
		'title': 'Herren'
	}];
	$scope.ages = [{
		'id': '',
		'title': 'Alter wählen...',
	}, {
		'id': 'U14',
		'title': `U14 (${oldestU14} und jünger)`,
	}, {
		'id': 'U16',
		'title': `U16 (${oldestU16} und jünger)`,
	}, {
		'id': 'DAU21',
		'title': `Damen U21 (${oldestU21 - 1} bzw. ${oldestU21} (SM) und jünger)`,
	}, {
		'id': 'HEU18',
		'title': `Herren U18 (${oldestU18} und jünger)`,
	}, {
		'id': 'Aktiv',
		'title': 'Aktiv',
	},];
	$scope.ligas = [{
		'id': '',
		'title': 'Liga wählen...'
	}, {
		'id': '1',
		'title': '1. Liga'
	}, {
		'id': '2',
		'title': '2. Liga'
	}, {
		'id': '3',
		'title': '3. Liga'
	}, {
		'id': '4',
		'title': '4. Liga'
	}];

	$scope.selectTeamNbr = function (nbr) {
		$scope.reg.teamNbr = nbr;
	};

	$scope.mandatoryClass = function (variable) {
		if (!userTriedToSend || variable && variable != '') {
			return '';
		} else {
			return 'has-error';
		}
	};

	$scope.mandatoryCheckboxClass = function (variable) {
		if (!userTriedToSend || variable) {
			return '';
		} else {
			return 'has-error';
		}
	};

	$scope.submit = function () {
		userTriedToSend = true;
		var isValid = $scope.checkModel();
		$scope.formError = !isValid;
		if (!$scope.formError) {
			console.log('will send data to server');
			$scope.doSubmit();
		}
	};

	$scope.doSubmit = function () {
		$scope.submitting = true;
		$scope.success = false;
		$scope.error = false;
		$scope.reg['gRecaptchaResponse'] = window.recaptchaResponse;
		var response = $http.post('postRegistration.php', $scope.reg);
		response.success($scope.doSuccess);
		response.error($scope.doError);
		$scope.response = '';
		$scope.error = '';
	};

	$scope.doSuccess = function (data, status) {
		$scope.submitting = false;
		$scope.success = true;
		$scope.error = false;
		$scope.response = data.status;
	};

	$scope.doError = function (data, status) {
		$scope.submitting = false;
		$scope.success = false;
		console.log(data);
		console.log(status);
		switch (status) {
			case 400:
				$scope.error = data.status;
				break;
			default:
				$scope.error = "Unbekannter Fehler aufgetreten (" + status + "). Bitte wende dich an mich (roman.schaller@gmail.com) damit ich das Problem beheben kann.";
				break;
		}
	};

	$scope.checkModel = function () {
		var isValid = $scope.checkAlwaysMandatory();
		if ($scope.isUTeam) {
			isValid = isValid && $scope.checkUTeam();
		} else {
			isValid = isValid && $scope.checkAgedTeam();
		}
		return isValid;
	};

	$scope.checkAlwaysMandatory = function () {
		var isValid = $scope.checkMandatory([$scope.reg.verein,
		$scope.reg.verband, $scope.reg.category,
		$scope.reg.age, $scope.reg.shirt, $scope.reg.hose,
		$scope.reg.ersatzshirt, $scope.reg.ersatzhose,
		$scope.reg.vereinAddress.name,
		$scope.reg.vereinAddress.address,
		$scope.reg.vereinAddress.ZIPCity, $scope.reg.vereinAddress.tel,
		$scope.reg.vereinAddress.mail, $scope.reg.teamAddress.name,
		$scope.reg.teamAddress.address, $scope.reg.teamAddress.ZIPCity,
		$scope.reg.teamAddress.tel, $scope.reg.teamAddress.mail]);
		isValid = isValid && $scope.reg.agree;
		return isValid;
	};

	$scope.checkUTeam = function () {
		var isValid = $scope.checkMandatory([$scope.reg.jugendAddress.name,
		$scope.reg.jugendAddress.address,
		$scope.reg.jugendAddress.ZIPCity, $scope.reg.jugendAddress.tel,
		$scope.reg.jugendAddress.mail, $scope.reg.birthYoung,
		$scope.reg.birthOld]);
		return isValid;
	};

	$scope.checkAgedTeam = function () {
		var isValid = $scope.checkMandatory([$scope.reg.liga, $scope.reg.teamNbr]);
		return isValid;
	};

	$scope.checkMandatory = function (mandatoryFields) {
		var isValid = true;
		mandatoryFields.map(function (fld) {
			if (fld === undefined || fld === "") {
				isValid = false;
			}
		});
		return isValid;
	};

});

app.directive('myAddress', function () {
	return {
		restrict: 'E',
		templateUrl: 'js/my-address.html',
		scope: {
			address: '=address',
			isMandatory: '=mandatory',
		},
		controller: function ($scope) {
			$scope.mandatoryClass = function (variable) {
				if (!$scope.isMandatory || !userTriedToSend || variable
					&& variable != '') {
					return '';
				} else {
					return 'has-error';
				}
			};
		}
	};
});

app.directive('refereeAddress', function () {
	return {
		restrict: 'E',
		templateUrl: 'js/referee-address.html',
		scope: {
			address: '=address',
			isMandatory: '=mandatory',
		},
		controller: function ($scope) {
			$scope.mandatoryClass = function (variable) {
				if (!$scope.isMandatory || !userTriedToSend || variable
					&& variable != '') {
					return '';
				} else {
					return 'has-error';
				}
			};
		}
	};
});
